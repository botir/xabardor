<?php

namespace frontend\v2\models;

use Yii;
use yii\base\Model;
use common\models\FeedbackMessage;

/**
 * ContactForm is the model behind the contact form.
* @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'subject', 'body'], 'required'],
            // We need to sanitize them
            [['name', 'subject', 'body'], 'filter', 'filter' => 'strip_tags'],
            // email has to be a valid email address
            [['phone'], '\common\validators\PhoneValidator', 'strict'=>false,'format'=>false, 'country'=>'RU'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name'              => Yii::t('frontend', 'contact_form_name'),
            'phone'             => Yii::t('frontend', 'contact_form_phone'),
            'subject'           => Yii::t('frontend', 'contact_form_subject'),
            'body'              => Yii::t('frontend', 'contact_form_body'),
            'verifyCode'        => Yii::t('frontend', 'contact_form_verify_code')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($is_mobile, $user=null)
    {
        if ($this->validate()) {
            $model = new FeedbackMessage();
            $model->full_name = $this->name;
            $model->phone = $this->phone;
            $model->subject = $this->subject;
            $model->message_txt = $this->body;
            $model->is_mobile = $is_mobile;
            if ($user)
                $model->user_id = $user;
            return $model->save();
        } else {
            return false;
        }
    }
}
