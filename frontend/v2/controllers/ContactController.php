<?php

namespace frontend\v2\controllers;

use yii\web\NotFoundHttpException;
use frontend\v2\components\FrontendController;
use common\models\PageStatic;
use Yii;

/**
 * Contact controller
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class ContactController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {   
        $model = $this->findModel('contact');
        if ($this->is_mobile){
            return $this->render('mobile/index');
        }else{
            return $this->render('desktop/index', ['model'=>$model]);
        }
    }

    /**
     * Finds the PageStatic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageStatic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        $model = PageStatic::getPageBySlug($slug);
        if ($model !== null && $model) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
