<?php

namespace frontend\v2\controllers;

use yii\web\NotFoundHttpException;
use common\models\PageStatic;
use frontend\v2\components\FrontendController;
use Yii;

/**
 * PolicyController
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class PolicyController extends FrontendController
{
    /**
     * @return string
     */
    public function actionTerms()
    {
        $model = $this->findModel('terms');
        if ($this->is_mobile){
            return $this->render('mobile/terms', ['model' => $model]);
        }else{
            return $this->render('desktop/terms', ['model' => $model]);
        }
    }

    /**
     * Finds the PageStatic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageStatic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = PageStatic::find()->where(['slug'=>$slug])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
