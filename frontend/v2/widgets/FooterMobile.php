<?php
namespace frontend\v2\widgets;


use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class FooterMobile
 * @package frontend\v2\widgets
 */
class FooterMobile extends \yii\base\Widget
{

    public function init()
    {   
        parent::init();
    }

  public function run()
  {

    echo '<footer class="footer"><div class="white-panel"><p class="copyright">&copy; 2018 Xabardor <br />'.Yii::t('frontend', 'footer_privacy_info').'</p> <p><span class="blue-color">'.Yii::t('frontend','footer_documentation').'</span><br /><br />'.Yii::t('frontend','footer_address').'</p></div><div class="colorful-panel"><div class="infopanel"><h5 class="infopanel__title f-title">'.Yii::t('frontend', 'footer_info_pages').'</h5><ul class="unstyled infopanel__list"><li class="infopanel__item"><a class="infopanel__link" href="'.Url::to(['/page/view', 'slug'=>'about']).'">'.Yii::t('frontend', 'page_about_us').'</a></li><li class="infopanel__item"><a class="infopanel__link" href="'.Url::to(['/contact/index']).'">'.Yii::t('frontend', 'page_contact_us').'</a></li><li class="infopanel__item"><a class="infopanel__link" href="'.Url::to(['/rss/index']).'">'.Yii::t('frontend', 'page_rss').'</a></li></ul></div><div class="social"><div class="social-box"> <h5 class="social-box__title f-title">'.Yii::t('frontend', 'footer_social').'</h5><ul class="unstyled social-links"><li class="social-links__item"><a class="social-links__link social-links__link--facebook" href="https://www.facebook.com/habardor.uz" target="__BLANK"><i class="icon-facebook-icon"></i></a></li><li class="social-links__item"><a class="social-links__link social-links__link--twitter" href="https://twitter.com/xabardor" target="__BLANK"><i class="icon-twitter-icon"></i></a></li><li class="social-links__item"><a class="social-links__link social-links__link--telegram" href="https://t.me/xabardoruz" target="__BLANK"><i class="icon-telegram"></i></a></li><li class="social-links__item"><a class="social-links__link social-links__link--instagram" href="http://instagram.com/xabardor.uz" target="__BLANK"><i class="icon-instagram-icon"></i></a></li></ul></div></div></div></footer>';
  }

}