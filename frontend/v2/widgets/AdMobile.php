<?php
namespace frontend\v2\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
/**
 * Class AdMobile
 * @package frontend\v2\widgets
 */
class AdMobile extends \yii\base\Widget
{
  public $place;
  public $link_class;

  public function run()
  {
      return $this->renderBanner($this->place, $this->link_class);
      //return $this->getAds($this->place, $this->link_class);

  }

  public function renderBanner($place, $class){
    if ($place==3){
      return "<!-- Revive Adserver Interstitial or Floating DHTML Tag - Generated with Revive Adserver v4.2.1 -->
        <script type='text/javascript'><!--//<![CDATA[
           var ox_u = 'http://ad.xabardor.uz/www/delivery/al.php?zoneid=3&layerstyle=simple&align=right&valign=top&padding=2&padding=2&shifth=0&shiftv=0&closebutton=f&nobg=t&noborder=t';
           if (document.context) ox_u += '&context=' + escape(document.context);
           document.write(\"<scr\"+\"ipt type='text/javascript' src='\" + ox_u + \"'></scr\"+\"ipt>\");
        //]]>--></script>";

    }elseif($place==4){
      return "<!-- Revive Adserver Interstitial or Floating DHTML Tag - Generated with Revive Adserver v4.2.1 -->
        <script type='text/javascript'><!--//<![CDATA[
           var ox_u = 'http://ad.xabardor.uz/www/delivery/al.php?zoneid=4&layerstyle=simple&align=right&valign=top&padding=2&padding=2&shifth=0&shiftv=0&closebutton=f&nobg=t&noborder=t';
           if (document.context) ox_u += '&context=' + escape(document.context);
           document.write(\"<scr\"+\"ipt type='text/javascript' src='\" + ox_u + \"'></scr\"+\"ipt>\");
        //]]>--></script>";
    }

  }

  public function getAds($place, $class)
  {

    if ($place==3){
      $file='images/img-topads.jpg';
    }elseif($place == 4){
      $file='images/img-topads.jpg';
    }
    $view = $this->getView();
    $assets = \frontend\v2\assets\MobileAsset::register($view);
    $baseUrl = $assets->baseUrl;
    
    $this->renderImage($baseUrl.'/'.$file, $class);
      
  }

  protected function renderImage($file, $class_link)
  {
    echo '<a href="#" target="__BLANK" class="'.$class_link.'"><img class="img-fluid" src="'.$file.'" /></a>';
  }

}
