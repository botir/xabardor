<?php
namespace frontend\v2\widgets;


use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\v2\modules\news\models\Category;
/**
 * Class HeaderDesktop
 * @package frontend\v2\widgets
 */
class HeaderDesktop extends \yii\base\Widget
{
  private $categories;

  public function init()
  {   
    $this->categories =  $this->getCategoryMenu(); 
    parent::init();
  }

    public function run()
    {
       $view = $this->getView();
      $assets = \frontend\v2\assets\DesktopAsset::register($view);
      $baseUrl = $assets->baseUrl;
      echo '<div class="first-header"><div class="first-header_top">';
      echo '<div class="menu-btn"><img src="'.$baseUrl.'/images/svg/menu.svg" alt="menu button" class="menu-btn_img"></div>';
      echo '<div class="header-logo"><a href="'.Url::to(["/news/default/index"]).'"><img src="'.$baseUrl.'/images/header/logo.png" alt="Xabardor UZ"></a></div>';
      echo '<div class="header-social_icons">
          <a class="social-link" href="https://t.me/xabardoruz" target="_blank">
            <img src="'.$baseUrl.'/images/svg/telegram.svg">
          </a>
          <a class="social-link" href="https://www.facebook.com/habardor.uz" target="_blank">
            <img src="'.$baseUrl.'/images/svg/facebook-logo.svg">
          </a>
          <a class="social-link" href="http://instagram.com/xabardoruz" target="_blank">
            <img src="'.$baseUrl.'/images/svg/instagram.svg">
          </a>         
          <a class="social-link" href="https://twitter.com/xabardor" target="_blank">
            <img src="'.$baseUrl.'/images/svg/twitter.svg">
          </a>
        </div></div>';
      echo '<div class="first-header_valuta"></div>';
      echo '</div>';
      echo '<div class="second-header"><div class="second-header_wrapper"><div class="small-logo"><a href="'.Url::to(["/news/default/index"]).'"><img src="'.$baseUrl.'/images/header/small-logo.png" alt="Xabardor UZ"></a></div>';
      echo '<nav class="header-nav"><div class="header-nav_item"><a href="'.Url::to(['/news/article/lenta']).'">'.Yii::t("frontend","page_lenta").'</a></div>';
      if ($this->categories){
        foreach ($this->categories as $category) {
          
          echo '<div class="header-nav_item"><a href="'.Url::to(['/news/category/view', 'slug' => $category['slug']]).'">'.$category['title'].'</a></div>';
        }
      }
      echo '</nav>';

      $this->renderForm($baseUrl);
      echo '</div></div>';
    }

    protected function renderForm($baseUrl)
    {
        echo Html::beginForm(Url::to(['/news/article/search']), 'get', ['class' => 'search-icon']);
        echo '<img class="search_btn" src="'.$baseUrl.'/images/svg/search-icon.svg" alt="search icon">';
        echo Html::textInput('q', '',
          ['placeholder' => Yii::t('frontend','search_input'),'required'=>'true', 'class'=>'search-input', 'id'=>'search-input'
        ]);
        echo Html::endForm();
    }

    protected function getCategoryMenu(){
      $list = Category::listMenu(10);
      return $list;
    }

}