<?php
namespace frontend\v2\widgets;


use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class FooterDesktop
 * @package frontend\v2\widgets
 */
class FooterDesktop extends \yii\base\Widget
{

    public function init()
    {   
        parent::init();
    }

  public function run()
  {
    $view = $this->getView();
    $assets = \frontend\v2\assets\DesktopAsset::register($view);
    $baseUrl = $assets->baseUrl;
    echo '<footer><div class="footer-wrapper">';
    echo '<ul class="footer-nav"><li><a href="'.Url::to(['/page/view', 'slug'=>'reklama']).'">'.Yii::t('frontend', 'page_ads').'</a></li><li><a href="'.Url::to(['/contact/index']).'">'.Yii::t('frontend', 'page_contact_us').'</a></li><li><a href="'.Url::to(['/page/view', 'slug'=>'about']).'">'.Yii::t('frontend', 'page_about_us').'</a></li></ul>';
    echo '<div class="footer-info"><p>&copy; 2018 Xabardor <br>'.Yii::t('frontend', 'footer_privacy_info').'</p><p class="blue-text">'.Yii::t('frontend','footer_documentation').'</p><p>'.Yii::t('frontend', 'footer_address2').'</p></div>';
    
    echo '<div class="footer-social"><div class="footer-social_icons"><p class="footer-social_title">'.Yii::t('frontend', 'footer_social').':</p><div class="icons"><a class="social-link" href="https://t.me/xabardoruz" target="_blank">
              <img class="visible" src="'.$baseUrl.'/images/svg/telegram.svg" />
              <img src="'.$baseUrl.'/images/svg/footer-mob/tg-b.svg" class="hidden" />
            </a><a class="social-link" href="https://www.facebook.com/habardor.uz" target="_blank">
              <img class="visible" src="'.$baseUrl.'/images/svg/facebook-logo.svg" />
              <img src="'.$baseUrl.'/images/svg/footer-mob/fb-b.svg" class="hidden"/ >
            </a><a class="social-link" href="http://instagram.com/xabardoruz" target="_blank">
              <img class="visible" src="'.$baseUrl.'/images/svg/instagram.svg" />
              <img src="'.$baseUrl.'/images/svg/footer-mob/insta-b.svg" class="hidden" />
            </a><a class="social-link" href="https://twitter.com/xabardor" target="_blank">
              <img class="visible" src="'.$baseUrl.'/images/svg/twitter.svg" />
              <img src="'.$baseUrl.'/images/svg/footer-mob/twt-b.svg" class="hidden" />
            </a></div></div><div class="footer-social_app"><p class="footer-social_title">Мобильные приложения:</p><div class="logos"><a class="logos-link" href="https://play.google.com/store/apps/details?id=uz.xabardor" target="_blank">
              <img src="'.$baseUrl.'/images/footer/play-google.png" alt="Google Play icon">
            </a><a class="logos-link" href="https://apps.apple.com/us/app/xabardor/id1477791938?l=ru&ls=1" target="_blank">
              <img src="'.$baseUrl.'/images/footer/app-store.png">
            </a></div></div></div></div></footer>';
            return ;
  }

}