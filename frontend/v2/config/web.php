<?php
$config = [
    'homeUrl' => Yii::getAlias('@frontendUrl'),
    'controllerNamespace' => 'frontend\v2\controllers',
    'defaultRoute' => 'news/default/index',
    'bootstrap' => ['maintenance'],
    'language'=>'uz',
    'modules' => [
        'user' => [
            'class' => frontend\v2\modules\user\Module::class,
            'shouldBeActivated' => false,
            'enableLoginByPass' => false,
        ],
        'news' => [
            'class' => 'frontend\v2\modules\news\Module',
        ],
    ],
    'components' => [
        'authClientCollection' => [
            'class' => yii\authclient\Collection::class,
            'clients' => [
                'facebook' => [
                    'class' => yii\authclient\clients\Facebook::class,
                    'clientId' => '1731869460166386',
                    'clientSecret' => '7fd51c65a1d9710d305e75aa876ebfbf',
                    'scope' => 'email,public_profile',
                    'attributeNames' => [
                        'name',
                        'email',
                        'first_name',
                        'last_name',
                    ]
                ],
                'google' => [
                    'class' => frontend\v2\modules\user\components\GoogleAuth::class,
                    'clientId' => '340731210626-sda386tcruggv55sndhl4monavs2rjiu.apps.googleusercontent.com',
                    'clientSecret' => 'T2uPxHDXNLUFZdDKskWhkran',
                    //'returnUrl' => 'http://localhost:8080/admin/auth/auth?authclient=google',
                ]
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error'
        ],
        'maintenance' => [
            'class' => common\components\maintenance\Maintenance::class,
            'enabled' => function ($app) {
                if (env('APP_MAINTENANCE') === '1') {
                    return true;
                }
                return $app->keyStorage->get('frontend.maintenance') === 'enabled';
            }
        ],
        'request' => [
            'cookieValidationKey' => env('FRONTEND_COOKIE_VALIDATION_KEY')
        ],
        'user' => [
            'class' => yii\web\User::class,
            'identityClass' => common\models\User::class,
            'loginUrl' => ['/user/sign-in/login'],
            'enableAutoLogin' => true,
            'as afterLogin' => common\behaviors\LoginTimestampBehavior::class,
            'identityCookie' => [
                'name'=>'PlR8f3wS9XsqSC939H',
                //'domain' => '.nxabardor.local',
                'domain' => '.xabardor.uz',
            ]
        ],
        'session' => [
            'name' => 'PzYvx58lUfzW4eb6KvZ91kIl', // unique for frontend
            //'savePath' => sys_get_temp_dir(), // a temporary folder on frontend
            'cookieParams'=>[
                'domain' => '.xabardor.uz',
                'lifetime' => 3600 * 4
            ],
            'timeout' => 3600*4, //session expire
        ],
        'view'         => [
            'class' => 'frontend\v2\components\View',
        ],
        // 'assetManager' => [
        //     'bundles' => [
        //         'yii\web\JqueryAsset' => [
        //             'js'=>[]
        //         ],
        //     ],
        // ]
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => '@frontend/v2/web/static/main',
                    'js' => [
                        'jquery.min.js',
                    ]
                ],
            ],
        ],
    ]
];

return $config;
