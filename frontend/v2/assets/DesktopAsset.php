<?php
namespace frontend\v2\assets;

use common\assets\Html5shiv;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class DesktopAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/v2/web/static/desktop';

    /**
     * @var array
     */
    public $css = [
        'css/style.min.css',
        'css/media.min.css',       
    ];

    /**
     * @var array
     */
    public $js = [
        'js/libs/sticky-sidebar.min.js',
        'js/libs/ias/callbacks.js',
        'js/libs/ias/jquery-ias.js',
        'js/libs/ias/extension/history.js',
        'js/libs/ias/extension/noneleft.js',
        'js/libs/ias/extension/paging.js',
        'js/libs/ias/extension/spinner.js',
        'js/libs/ias/extension/trigger.js',
        'js/libs/audiojs/audiojs/audio.js',
        'js/libs/jplayer/jquery.jplayer.min.js',
        'js/libs/jplayer/jplayer.playlist.js',
        'js/libs/sticky-sidebar.min.js',
        'js/operation.js',

    ];

    /**
     * @var array
     */
    public $depends = [
        Html5shiv::class,
        'yii\web\JqueryAsset',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
}
