<?php
namespace frontend\v2\assets;

use common\assets\Html5shiv;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Frontend application asset
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class MobileAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/v2/web/static/mobile';

    /**
     * @var array
     */
    public $css = [
        'css/main.min.css',
    ];

    /**
     * @var array
     */
    public $js = [
        //'js/jquery.min.js',
        // 'js/scripts.min.js',
        // 'js/libs/ias/callbacks.js',
        // 'js/libs/ias/jquery-ias.js',
        // 'js/libs/ias/extension/history.js',
        // 'js/libs/ias/extension/noneleft.js',
        // 'js/libs/ias/extension/paging.js',
        // 'js/libs/ias/extension/spinner.js',
        // 'js/libs/ias/extension/trigger.js',
        // 'js/libs/load-image/load-image.all.min.js',
        // 'js/libs/fileupload/vendor/jquery.ui.widget.js',
        // 'js/libs/fileupload/jquery.iframe-transport.js',
        // 'js/libs/fileupload/jquery.fileupload.js',
        // 'js/libs/fileupload/jquery.fileupload-process.js',
        // 'js/libs/fileupload/jquery.fileupload-image.js',
        // 'js/libs/fileupload/jquery.fileupload-validate.js',

        // 'js/uploader.js',
        // 'js/operation.js',
        'js/app.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        Html5shiv::class,
        'yii\web\JqueryAsset',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
}
