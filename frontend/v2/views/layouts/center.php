<?php
use yii\helpers\Url;
$assets = \frontend\v2\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$this->beginContent('@frontend/v2/views/layouts/_clear.php');
?>
    <div class="wrapper">
      <header class="header wrapper__header subscr">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-3 col-1">
              <a class="back subscr__back" href="<?=Url::to(['/news/default/index'])?>">
                <img src="<?=$baseUrl?>/images/left-arrow.svg" />
                <span>Bash sahifaga qaytish</span>
              </a>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-6 col-8">
              <div class="logo text-center subscr__logo">
                <h2 class="logo__text subscr__text">
                  <a href="#">
                    xabardor
                    <span>+ Obuna</span>
                  </a>
                </h2>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div class="main-content">
        <div class="container">
          <div class="white-block mt-0">
            <div class="row justify-content-center">
              <div class="col-lg-8 col-md-10 col-sm-12 col-12">
                <?php echo $content ?>
              </div>
            </div>
          </div>
        </div>
      </div>
	</div>
	<?=\frontend\v2\widgets\FooterDesktop::widget()?>
<?php $this->endContent() ?>