<?php
/* @var $content string */
$this->beginContent('@frontend/v2/views/layouts/_clear_mobile.php')
?>
    <div class="wrapper">
		<?=\frontend\v2\widgets\HeaderMobile::widget()?>
    <?php if ($this->ads):?>
    	<div class="ads-container">
      		<div class="container">
      			<?=\frontend\v2\widgets\AdMobile::widget(['place'=>3, 'link_class'=>'top-ads'])?>
      		</div>      
    	</div>
    <?php endif;?>
    	<?php echo $content ?>
	</div>
	<?=\frontend\v2\widgets\FooterMobile::widget()?>
<?php $this->endContent() ?>