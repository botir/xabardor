<?php
/* @var $content string */

$this->beginContent('@frontend/v2/views/layouts/_clear.php')
?>
    <header>
      <?=\frontend\v2\widgets\HeaderDesktop::widget()?>
        <div class="ad-banner">
        <?php if ($this->ads):?>
      		<?=\frontend\v1\widgets\AdDesktop::widget(['place'=>1, 'link_class'=>'top-ads'])?>
        <?php endif; ?>
        </div>
    </header>
    <main>
    	<?php echo $content ?>
    </main>
	<?=\frontend\v2\widgets\FooterDesktop::widget()?>
<?php $this->endContent() ?>