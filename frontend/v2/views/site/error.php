<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$assets = \frontend\v2\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$this->ads = false;
?>

<div class="blocks-wrapper">
  <div class="main-block opened-news_block">
    <div class="opened-news_content">
      <div class="container">
        <span class="source-img"><?php echo Html::encode($this->title) ?></span>
         <div class="opened-news_heading">
            <p><?php echo nl2br(Html::encode($message)) ?></p>
            <p><a class="blue-color" href="/">Bosh sahifaga</a> qaytish</p>
         </div>
      </div>
    </div>
  </div>
</div>


