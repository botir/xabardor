<?php
use yii\helpers\Url;
$this->title = $model['title'];
$assets = \frontend\v2\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$this->ads = false;
$photo_day = [
  'url'=> Url::to(['/news/app/index', 'f'=>'photoday']), 
  'icon'=>$baseUrl.'/images/svg/rightSide/camera.svg'
];
$selected_articles = ['url'=> Url::to(['/news/app/index', 'f'=>'selected'])];
?>

<div class="blocks-wrapper">
  <div class="main-block">
    <div class="about about-site">
      <div class="about_wrapper">
        <span class="rubric-name"><?=$model['title']?></span>

        <div class="container">
          <?=$model['content']?>
        </div>

        
      </div>
    </div>
  </div>
  <div class="rightSide-block">
    <div class="rightSide-news_top" id="photo_day" data='<?=json_encode($photo_day)?>'></div>
    
        <div class="rightSide-actual" id="actual_news_right" data='<?=json_encode($selected_articles)?>'></div>
        <div class="rightSide-app">
          <h3 class="rightSide-app_title"><?=Yii::t('frontend', 'Mobile_apps')?></h3>
            <div class="rightSide-app_wrapper">
              <a class="app-link" href="https://play.google.com/store/apps/details?id=uz.xabardor" target="_blank">
                <img src="<?=$baseUrl?>/images/rightside/playMarket.png" alt="Google Play" />
              </a>
              <a class="app-link" href="https://apps.apple.com/us/app/xabardor/id1477791938?l=ru&ls=1" target="_blank">
                  <img src="<?=$baseUrl?>/images/rightside/appStore.png" alt="App Store">
              </a>
            </div>
        </div>
  </div>
</div>


