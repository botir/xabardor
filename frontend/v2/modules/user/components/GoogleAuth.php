<?php

namespace frontend\v2\modules\user\components;

use yii\authclient\clients\Google;


class GoogleAuth extends SocialOAuth2
{
   /**
     * {@inheritdoc}
     */
    public $authUrl = 'https://accounts.google.com/o/oauth2/auth';
    /**
     * {@inheritdoc}
     */
    public $tokenUrl = 'https://accounts.google.com/o/oauth2/token';
    /**
     * {@inheritdoc}
     */
    public $apiBaseUrl = 'https://www.googleapis.com/plus/v2';


    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if ($this->scope === null) {
            $this->scope = implode(' ', [
                'profile',
                'email',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function initUserAttributes()
    {
        return $this->api('people/me', 'GET');
    }

    /**
     * {@inheritdoc}
     */
    protected function defaultName()
    {
        return 'google';
    }

    /**
     * {@inheritdoc}
     */
    protected function defaultTitle()
    {
        return 'Google';
    }

}
