<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $model common\base\MultiModel */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('frontend', 'User Settings');
$profile__avatar = $profile->getAvatar();
?>
<div class="main-content">
  <div class="container">
    <div class="profile white-block mt-0">
      <div class="follow">
        <div class="profile__avatar">
          <i class="icon-camera-icon"></i>
          <input type="file" id="file-upload" name="_fileinput_w1" style="width: 1000px;
                height: 100%;
                position: absolute;
                display: block;
                opacity: 0;
                cursor: pointer;
                z-index: 9;" />
          <input type="hidden" id="newsarticle-photo" class="empty-value" name="avatar_user">
        <?php if ($profile__avatar):?>
          <span class="profile__img follow__img" id="profile-image" style="background: url(<?=$profile__avatar?>) no-repeat center center; background-size: cover;">
          </span>
        <?php else:?>
          <span class="profile__img follow__img d-none" id="profile-image" style="background: url(1) no-repeat center center; background-size: cover;">
          </span>
            <span class="dis-table" id="profile-none-image">
                <span class="table-cell">
                  <span class="profile__logo">Xabardor</span>
                </span>
            </span>
        <?php endif; ?>
          <div class="upload-kit-input">
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
          </div>
        </div>
        <div class="follow__body">
          <div class="follow__content">
            <h3 class="follow__title"><?=$profile->getFullName()?></h3>
            <a href="#" title="" class="follow__link">Profilni o’zgartirish</a>
            <p class="follow__text red-color">
              <strong>Xabardor ID: <?=$user->tab_number?></strong>
            </p>               
          </div>
          <div class="follow__content--end">
            <p class="fc-email">Email: <a href="mailto:<?=$user->email?>"><?=$user->email?></a></p>
            <?php if ($period):?>
              <p class="blue-color fc-semi">Obuna vaqti: <span class="start-date"><?=$period['start_date']?></span> - <span class="end-date"><?=$period['end_date']?></span></p>
            <?php endif; ?>
          </div> 
        </div>                      
      </div>
      <div class="popular">
        <ul class="popular__tabs profile__tabs">
          <?php if ($period):?>
            <li class="popular__tab-link" data-tab="tab-3" id="get-history"><?=Yii::t('frontend', 'user_history_subscribe')?></li>
            <li class="popular__tab-link" data-tab="tab-4"><?=Yii::t('frontend', 'user_change_password')?></li>
            <li class="popular__tab-link" data-tab="tab-5"><?=Yii::t('frontend', 'user_change_profile')?></li>
          <?php else:?>
            <li class="popular__tab-link current" data-tab="tab-1"><?=Yii::t('frontend', 'service_list')?></li>
            <li class="popular__tab-link" data-tab="tab-4"><?=Yii::t('frontend', 'user_change_password')?></li>
            <li class="popular__tab-link" data-tab="tab-5"><?=Yii::t('frontend', 'user_change_profile')?></li>
          <?php endif; ?>
        </ul>
        <?php if ($plans):?> 
        <div id="tab-1" class="popular__tab-content current">
          <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-12">
              <div class="row">
                <?php foreach($plans as $plan):?>
                <div class="col-lg-6 col-md-12 col-12">
                  <div class="subscribe__item">
                    <div class="sbscr-item__header">
                      <h3 class="sbscr-item__title"><?=$plan['title']?></h3>
                      <p class="sbscr-item__price"><b><?=$plan['price']?></b> so’m</p>
                    </div>
                    <div class="sbscr-item__body">
                      <p class="text-center"><strong>Xabardorga obuna bo’lish bu:</strong></p>
                      <div class="yes-box">
                        <ul class="unstyled yesno__list">
                          <?php $plan_options = Json::decode($plan['more_option']); foreach($plan_options as $plan_option){
                            echo '<li class="yesno__item">'.$plan_option['title'].'</li>';
                          }
                          ?>
                        </ul>
                      </div>                                 
                    </div>
                    <div class="sbscr-item__footer">
                      <a href="<?=Url::to(['/subscribe/view', 'name'=>$plan['uniq_name']])?>" class="sbscr-item__link">Batafsil ma’lumot</a>
                    </div>
                  </div>
                </div>
              <?php endforeach;?>
              </div>
            </div>                
          </div>
        </div>
      <?php endif; ?>
        <div id="tab-3" class="popular__tab-content">
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col"><?=Yii::t('frontend', 'user_history_service_title')?></th>
                  <th scope="col"><?=Yii::t('frontend', 'user_history_service_price')?></th>
                  <th scope="col"><?=Yii::t('frontend', 'user_history_service_date')?></th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>
        </div>
        <div id="tab-4" class="popular__tab-content">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 col-sm-8 col-12">
              <?php $form = ActiveForm::begin([
                  'id' => 'password-form',
                  'enableClientValidation'=> true,
                  'enableAjaxValidation'=> false,
                  'validateOnSubmit' => true,
                  'validateOnChange' => true,
                  'validateOnType' => true,
                  'options'=>[
                      'class'=>'signform content-form'
                  ]
                  ]);
                ?>
                <?php echo $form->field($passwordmodel, 'old_password',[
                  'options' => ['class' => 'signform__form-group'],
                  'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-lock-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
                ->passwordInput([
                    'placeholder'=> Yii::t('frontend', 'old_password'),
                    'maxlength' => true,
                    'class' => 'signform__form-control',
                ]); 
                ?>

                <?php echo $form->field($passwordmodel, 'new_password',[
                  'options' => ['class' => 'signform__form-group'],
                  'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-lock-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
                ->passwordInput([
                    'placeholder'=> Yii::t('frontend', 'new_password'),
                    'maxlength' => true,
                    'class' => 'signform__form-control',
                ]); 
                ?>

                <?php echo $form->field($passwordmodel, 'new_password_confirm',[
                  'options' => ['class' => 'signform__form-group'],
                  'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-lock-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
                ->passwordInput([
                    'placeholder'=> Yii::t('frontend', 'new_password_confirm'),
                    'maxlength' => true,
                    'class' => 'signform__form-control',
                ]); 
                ?>
                
                <?php echo Html::submitButton('<span>'.Yii::t('frontend', 'user_account_save').'</span>', ['class' => 'btn btn-success signform__btn w-100', 'name' => 'login-button']) ?> 

                <?php ActiveForm::end(); ?> 
            </div>
          </div>              
        </div>
        <div id="tab-5" class="popular__tab-content">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 col-sm-8 col-12">
              <?php $form = ActiveForm::begin([
                  'id' => 'account-form',
                  'enableClientValidation'=> true,
                  'enableAjaxValidation'=> false,
                  'validateOnSubmit' => true,
                  'validateOnChange' => true,
                  'validateOnType' => true,
                  'options'=>[
                      'class'=>'signform content-form'
                  ]
                  ]);
                ?>
                <?php echo $form->field($profile, 'firstname',[
                  'options' => ['class' => 'signform__form-group'],
                  'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-user-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
                ->textInput([
                    'placeholder'=> Yii::t('frontend', 'user_sign_with_email'),
                    'maxlength' => true,
                    'class' => 'signform__form-control',
                ]); 
                ?>

                <?php echo $form->field($profile, 'lastname',[
                  'options' => ['class' => 'signform__form-group'],
                  'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-user-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
                ->textInput([
                    'placeholder'=> Yii::t('frontend', 'user_sign_with_email'),
                    'maxlength' => true,
                    'class' => 'signform__form-control',
                ]); 
                ?> 
                <?php echo Html::submitButton('<span>'.Yii::t('frontend', 'user_account_save').'</span>', ['class' => 'btn btn-success signform__btn w-100', 'name' => 'login-button']) ?> 

                <?php ActiveForm::end(); ?>
            </div>
          </div>
          
        </div>
      </div>
    </div>        
  </div>
</div>

<style type="text/css">
.upload-kit .upload-kit-input .progress,
.upload-kit .upload-kit-item .progress,
.upload-kit .upload-kit-input .add,
.upload-kit .upload-kit-item .add,
.upload-kit .upload-kit-input .remove,
.upload-kit .upload-kit-item .remove {
  display: block;
  position: absolute;
}
.upload-kit .upload-kit-input .error-popover,
.upload-kit .upload-kit-item .error-popover {
  color: #9b0014;
  position: absolute;
  bottom: 0;
  right: 0;
  z-index: 999;
  display: none;
  cursor: default;
}
.upload-kit .upload-kit-input .progress {
  position: absolute;
  top: 50%;
  margin-top: -10px;
  margin-left: 5%;
  margin-right: 5%;
  width: 90%;
  display: none;
}
.upload-kit .upload-kit-input.in-progress .progress {
  display: block;
}
.upload-kit .upload-kit-input.in-progress .add {
  display: none;
}
@-webkit-keyframes progress-bar-stripes {
  from {
    background-position: 1rem 0;
  }
  to {
    background-position: 0 0;
  }
}

@keyframes progress-bar-stripes {
  from {
    background-position: 1rem 0;
  }
  to {
    background-position: 0 0;
  }
}

.progress {
  display: -ms-flexbox;
  display: flex;
  height: 1rem;
  overflow: hidden;
  font-size: 0.75rem;
  background-color: #e9ecef;
  border-radius: 0.25rem;
}

.progress-bar {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-direction: column;
  flex-direction: column;
  -ms-flex-pack: center;
  justify-content: center;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  background-color: #007bff;
  transition: width 0.6s ease;
}

@media screen and (prefers-reduced-motion: reduce) {
  .progress-bar {
    transition: none;
  }
}

.progress-bar-striped {
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-size: 1rem 1rem;
}

.progress-bar-animated {
  -webkit-animation: progress-bar-stripes 1s linear infinite;
  animation: progress-bar-stripes 1s linear infinite;
}
</style>