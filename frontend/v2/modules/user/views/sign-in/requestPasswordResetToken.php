<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\PasswordResetRequestForm */

$this->title =  Yii::t('frontend', 'user_forgot_password_page_title');
$this->bclass = 'forgot-page';
$this->ads = false;
?>

<div class="main-content full-content">
  <div class="container">
    <div class="white-block mt-0 full-height">
      <div class="vertical-center">
        <div class="row justify-content-center">
          <div class="col-md-4 col-sm-6 col-12">
            <h1 class="modal__title text-center"><?=Yii::t('frontend', 'user_forgot_password_title');?></h1>
            <p class="text-center"><?=Yii::t('frontend', 'user_forgot_password_enter_email');?></p>
            <?php $form = ActiveForm::begin([
                    'id' => 'request-password-reset-form',
                    'options'=>['class'=>'signform lostpass-form']]); ?>
                <?php echo $form->field($model, 'email',[
                    'options' => ['class' => 'signform__form-group'],
                    'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-mail-icon"></i></div></div>{input}</div>{error}'])
                ->textInput([
                    'placeholder'=>Yii::t('frontend', 'user_forgot_password_input_email'),
                    'maxlength' => true,
                    'class' => 'signform__form-control',
                ]); 
                ?>
              <?php echo Html::submitButton('<span>'.Yii::t('frontend', 'user_forgot_password_submit').'</span>', ['class' => 'btn btn-success signform__btn w-100 mb-0']) ?>
            <?php ActiveForm::end(); ?>          
          </div>
        </div>  
      </div>                 
    </div>        
  </div>
</div>