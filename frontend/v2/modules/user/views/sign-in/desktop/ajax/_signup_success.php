<div class="modal__content">
<h1 class="modal__title"><?=Yii::t('frontend', 'user_sign_up_success_title')?></h1>
<p><?=Yii::t('frontend', 'user_sign_up_success_sub_title')?></p>
</div>