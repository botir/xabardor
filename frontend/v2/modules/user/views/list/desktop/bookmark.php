<?php 
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
$trend_tags = ['url'=> Url::to(['/news/app/index', 'f'=>'tags'])];
$this->title = Yii::t('frontend','user_bookmarks_list');
?>
<div class="main-content search-content">
	<div class="container">
		<div class="white-block mt-0">
			<div class="page-title">
            	<i class="icon-ribbon-filled-icon page-title__icon"></i>
            	<h3 class="page-title__title"><?=Yii::t('frontend','user_bookmarks_list')?></h3>
          	</div>
		</div>
		<div class="main-section bycategory">
			<div id="search-tab" class="popular__tab-content current">
				<?php if ($list){ foreach($list as $data):?>
				<div class="white-block">
	              <div class="news-list tabnews">
	                <div class="news-list__item">
	                  <div class="news-list__content">
	                    <h4 class="news-list__title news-title">
	                      <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
	                    </h4>
	                    <p class="news-list__text news-content"><?=$data['description']?></p>
	                    <div class="dashed-divider"></div>
	                    <ul class="unstyled taglist">
	                      <li class="taglist__item">
	                        <a href="<?=Url::to(['/user/bookmark/delete', 'tab_number'=>$data['tab_number']])?>" class="taglist__link remove-bookmark"><span><?=Yii::t('frontend','bookmark_remove')?></span></a>
	                      </li>
	                    </ul>
	                  </div>
	                  <a class="news-list__link" href="<?=$this->getArticleUrl($data['slug'])?>">
	                    <img src="<?=$data['thumb_name'].'_medium.'.$data['image_ext']?>" />
	                  </a>
	                </div>
	              </div>
	            </div>
	        <?php endforeach;} else{ echo '<div class="white-block"><h2>Xech narsa saqlanmagan</h2></div>';}?>
	        <?php if ($next_page):?>
		        <div class="load-panel__button" id="load-more-btn">
		          <a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
		        </div>
	      <?php endif;?>
			</div>
		</div>
		<div class="right-sidebar">
			<div class="white-block mt-0" id="trend_tags" data='<?=json_encode($trend_tags)?>'></div>
			<div class="ads-block">
            	<?=\frontend\v2\widgets\AdDesktop::widget(['place'=>2, 'link_class'=>'right-ads'])?>
            </div>
		</div>
	</div>
</div>