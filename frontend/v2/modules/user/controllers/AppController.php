<?php

namespace frontend\v2\modules\user\controllers;

use Yii;
use yii\rest\Controller;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use common\models\UserBookmark;


class AppController extends Controller
{

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['contentNegotiator'] = [
      'class' => 'yii\filters\ContentNegotiator',
      'formats' => [
          'application/json' => Response::FORMAT_JSON,
          'charset' => 'UTF-8',
      ]
    ];
    return $behaviors;
  }

  public function actionBookmark($tab_number)
  {
    $request = Yii::$app->request;
    print_r($request);
    exit;

  }

}
