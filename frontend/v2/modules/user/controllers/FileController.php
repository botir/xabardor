<?php

namespace frontend\v2\modules\user\controllers;

use League\Flysystem\FilesystemInterface;
use trntv\filekit\events\UploadEvent;
use trntv\filekit\actions\BaseAction;
use League\Flysystem\File as FlysystemFile;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;
use Imagine\Image\Box;
use yii\web\Controller;



class FileController extends Controller
{
    public function actionAvatar()
    {
        $user = Yii::$app->user->identity;
        $profile = $user->userProfile;

        return $this->render('index', ['user'=>$user, 'profile'=>$profile]);
    }
}
