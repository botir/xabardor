<?php

namespace frontend\v2\modules\user\controllers;

use common\commands\SendEmailCommand;
use common\models\User;
use common\models\UserSocial;
use common\models\UserToken;
use frontend\v2\components\FrontendController;
use frontend\v2\modules\user\models\LoginForm;
use frontend\v2\modules\user\models\PasswordResetRequestForm;
use frontend\v2\modules\user\models\ResetPasswordForm;
use frontend\v2\modules\user\models\SignupForm;
use Yii;
use yii\authclient\AuthAction;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class SignInController
 * @package frontend\v2\modules\user\controllers
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class SignInController extends FrontendController
{

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'oauth' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'successOAuthCallback']
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'signup', 'login', 'login-by-pass', 'request-password-reset', 'reset-password', 'oauth', 'activation'
                        ],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => [
                            'signup', 'login', 'request-password-reset', 'reset-password', 'oauth', 'activation'
                        ],
                        'allow' => false,
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/user/default/index']);
                        }
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::class,
            //     'actions' => [
            //         'logout' => ['post']
            //     ]
            // ]
        ];
    }

    /**
     * @return array|string|Response
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load(Yii::$app->request->post()) && $model->login()){
                
                return ['status'=>'success', 'content'=>'OK'];
            }
            Yii::$app->assetManager->bundles = [
                'yii\web\JqueryAsset' => false,
            ];
            $content = $this->renderAjax('desktop/ajax/_login', ['model' => $model]);
            return ['status'=>'failure', 'content'=>$content];
        }
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        if ($this->is_mobile){
            return $this->render('mobile/login', [
                'model' => $model
            ]);    
        }
        return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * @param $token
     * @return array|string|Response
     * @throws ForbiddenHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionLoginByPass($token)
    {
        if (!$this->module->enableLoginByPass) {
            throw new NotFoundHttpException();
        }

        $user = UserToken::use($token, UserToken::TYPE_LOGIN_PASS);

        if ($user === null) {
            throw new ForbiddenHttpException();
        }

        Yii::$app->user->login($user);
        return $this->goHome();
    }

    /**
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * @return string|Response
     */
    public function actionSignup()
    {
        $model = new SignupForm();

         if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load(Yii::$app->request->post())){
                $user = $model->signup();
                if ($user) {
                    $content = $this->renderAjax('desktop/ajax/_signup_success');
                    return ['status'=>'success', 'content'=>$content];
                }
            }
            Yii::$app->assetManager->bundles = [
                'yii\web\JqueryAsset' => false,
            ];
            $content = $this->renderAjax('desktop/ajax/_signup', ['model' => $model]);
            return ['status'=>'failure', 'content'=>$content];
        }



        if ($model->load(Yii::$app->request->post())) {
            $user = $model->signup();
            if ($user) {
                return $this->render('desktop/signup_success', [
                    'model' => $model
                ]);
            }
        }
        if ($this->is_mobile){
            return $this->render('mobile/signup', [
                'model' => $model
            ]);
        }
        return $this->render('signup', [
            'model' => $model
        ]);
    }

    /**
     * @param $token
     * @return Response
     * @throws BadRequestHttpException
     */
    public function actionActivation($token)
    {
        $token = UserToken::find()
            ->byType(UserToken::TYPE_ACTIVATION)
            ->byToken($token)
            ->notExpired()
            ->one();

        if (!$token) {
            throw new BadRequestHttpException;
        }

        $user = $token->user;
        $user->updateAttributes([
            'status' => User::STATUS_ACTIVE
        ]);
        $token->delete();
        Yii::$app->getUser()->login($user);
        Yii::$app->getSession()->setFlash('alert', [
            'body' => Yii::t('frontend', 'Your account has been successfully activated.'),
            'options' => ['class' => 'alert-success']
        ]);

        return $this->goHome();
    }

    /**
     * @return string|Response
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                return $this->render('desktop/resetr_password_success', [
                    'model' => $model
                ]);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     * @return string|Response
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('alert', [
                'body' => Yii::t('frontend', 'New password was saved.'),
                'options' => ['class' => 'alert-success']
            ]);
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * @param $client \yii\authclient\BaseClient
     * @return bool
     * @throws Exception
     */
    public function successOAuthCallback($client)
    {
        // use BaseClient::normalizeUserAttributeMap to provide consistency for user attribute`s names
        $attributes = $client->getUserAttributes();
        $client_name = $client->getName();
        $oauth_client = UserSocial::find()->where([
            'oauth_client' => $client->getName(),
            'oauth_client_user_id' => ArrayHelper::getValue($attributes, 'id')
        ])->one();

        if (!$oauth_client) {
            $oauth_client = new UserSocial();
            $oauth_client->oauth_client = $client->getName();
            $oauth_client->oauth_client_user_id = ArrayHelper::getValue($attributes, 'id');

            $email = ArrayHelper::getValue($attributes, 'email');
            if($email === null){
                $email = ArrayHelper::getValue($attributes, ['emails', 0, 'value']);
            }
            $user = User::findByLogin($email);
            if ($user){
                $oauth_client->user_id = $user->id;
            }else{
                $user = new User();
                $user->scenario = 'oauth_create';
                $user->username = $email;
                $user->email = $email;
                $user->status = User::STATUS_ACTIVE;
                $password = Yii::$app->security->generateRandomString(8);
                $user->setPassword($password);
                if ($user->save()) {
                    $oauth_client->user_id = $user->id;
                    $profileData = [];
                    if ($client_name === 'facebook') {
                        $profileData['firstname'] = ArrayHelper::getValue($attributes, 'first_name');
                        $profileData['lastname'] = ArrayHelper::getValue($attributes, 'last_name');
                    }else{
                        $info_google  = $attributes['name'];

                        $profileData['firstname'] = ArrayHelper::getValue($info_google, 'givenName');
                        $profileData['lastname'] = ArrayHelper::getValue($info_google, 'familyName');
                    }
                    $user->afterSignup($profileData);
                }else{
                    throw new Exception('User error');
                }
            }
            $oauth_client->save();
        }else{
            $user = User::findIdentity($oauth_client->user_id);
        }

        if (Yii::$app->user->login($user, 3600 * 24 * 30)) {
            return true;
        }

        throw new Exception('OAuth error');
    }
}
