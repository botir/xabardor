<?php

namespace frontend\v2\modules\user\controllers;

use Yii;
use yii\rest\ActiveController;
use frontend\v2\modules\news\models\Article;


class BookmarkController extends ActiveController
{

  public $modelClass = 'common\models\UserBookmark';

  public function actions()
  {
    return array_merge(
      parent::actions(),
        [
            'update' => null,
            'create' => null,
            'delete' => null,
            'view' => null,
            'index' => null,
        ]
    );
  }

  public function actionView($id)
  {
    if (Yii::$app->user->isGuest)
      return ['title'=>Yii::t('frontend','bookmark_save')];

    $article = $this->findNews($id);
    $user = Yii::$app->user->identity;
    $model = $this->findModel($user->id, $article->id);
    if ($model){
      return ['title'=>Yii::t('frontend','bookmark_remove')];
    }else{
      return ['title'=>Yii::t('frontend','bookmark_save')];
    }
  }

  public function actionCreate()
  {
    if (isset($_POST['nm'])){
      $tab_number = intval($_POST['nm']);
    }
    $article = $this->findNews($tab_number);
    $user = Yii::$app->user->identity;
    $model = $this->findModel($user->id, $article->id);
    if ($model){
      $model->delete();
      return ['title'=>Yii::t('frontend','bookmark_save')];
    }

    $model = new $this->modelClass;
    $model->news_id = $article->id;
    $model->user_id = $user->id;
    $model->save();
    return ['title'=> Yii::t('frontend','bookmark_remove')];
  }

  public function actionDelete($tab_number)
  {
    $article = $this->findNews($tab_number);
    $user = Yii::$app->user->identity;
    $model = $this->findModel($user->id, $article->id);
    if ($model){
      $model->delete();
      
    }
    return ['success'=>true];
  }


  protected function findModel($user_id, $id)
  {
    $model = $this->modelClass::findOne(['news_id'=>$id, 'user_id'=>$user_id]);
    return $model;
  }

  protected function findNews($tab_number)
  {
    if (($model = Article::findOne(['tab_number'=>$tab_number])) !== null) {
      return $model;
    } else {
      throw new \yii\web\HttpException(404, 'Not Found');
    }
  }


}
