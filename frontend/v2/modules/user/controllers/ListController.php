<?php

namespace frontend\v2\modules\user\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use frontend\v2\components\FrontendController;
use common\models\User;
use common\models\UserProfile;
use common\models\UserBookmark;

class ListController extends FrontendController
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionBookmark()
    {
        $user = Yii::$app->user->identity;
        $list = UserBookmark::listArticles($user->id, $this->next,  self::ARTICLE_LIMIT);

        $last_item = $this->getLoadMore($list);
        if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
            $next_page =  Url::to(['/user/list/bookmark', 'next' => $last_item['next']]);
        }else{
            $next_page = null;
        }
        if ($this->is_mobile){
            return $this->render('mobile/bookmark', [
                'user'=>$user, 
                'list'=>$list,
                'next_page' => $next_page,
            ]);

        }else{
            return $this->render('desktop/bookmark', [
                'user'=>$user, 
                'list'=>$list,
                'next_page' => $next_page,
            ]);
        }
    }
}
