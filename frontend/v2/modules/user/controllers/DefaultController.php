<?php

namespace frontend\v2\modules\user\controllers;

use common\base\MultiModel;
use common\models\User;
use common\models\UserProfile;
use common\models\PaymentUser;
use common\models\PaymentService;
use frontend\v2\modules\user\models\AccountForm;
use frontend\v2\modules\user\models\PasswordForm;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\filters\AccessControl;
use frontend\v2\components\FrontendController;

class DefaultController extends FrontendController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::class,
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(215, 215);
                    $file->put($img->encode());
                    $user = Yii::$app->user->identity;
                    //$path_perfex = $file->getAdapter()->getPathPrefix();
                    $path_perfex = \Yii::getAlias('@storageUrl').'/source';
                    //print_r($ad); exit;
                    User::changeAvatar($user->id, $path_perfex, $file->getPath());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $profile = $user->userProfile;
        $profile->locale = 'uz-Uz';
        $passwordModel  = new PasswordForm();
        $passwordModel->user = $user;
        if ($profile->load(Yii::$app->request->post()) && $profile->save()) {
            return $this->refresh();
        }
        if ($passwordModel->load(Yii::$app->request->post()) && $passwordModel->validate()&&$passwordModel->save()) {
            return $this->refresh();
        }
        $period = PaymentUser::getUserPeriod(date('Y-m-d'),  $user->id);
        if (!$period){
            $plans = PaymentService::listPlans();
        }else{
            $plans = null;
        }
        if ($this->is_mobile){
            return $this->render('mobile/index', [
                'user'=>$user, 
                'period'=>$period, 
                'plans'=>$plans, 
                'profile'=>$profile, 
                'passwordmodel'=>$passwordModel
            ]);
        }
        return $this->render('index', [
            'user'=>$user, 
            'period'=>$period, 
            'plans'=>$plans, 
            'profile'=>$profile, 
            'passwordmodel'=>$passwordModel
        ]);
    }
}
