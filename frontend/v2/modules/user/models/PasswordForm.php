<?php

namespace frontend\v2\modules\user\models;

use common\models\UserToken;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;

/**
 * PasswordForm reset form
 */
class PasswordForm extends Model
{
    /**
     * @var
     */
    public $old_password;
    public $new_password;
    public $new_password_confirm;
    public $user;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user'], 'required'],
            [['old_password', 'new_password', 'new_password_confirm'], 'required'],
            [['new_password', 'new_password_confirm'], 'string', 'min' => 6],
            ['new_password_confirm', 'compare', 'compareAttribute' => 'new_password'],
            ['old_password', 'validatePassword']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'old_password' => Yii::t('frontend', 'old_password'),
            'new_password' => Yii::t('frontend', 'new_password'),
            'new_password_confirm' => Yii::t('frontend', 'new_password_confirm')
        ];
    }

    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->old_password)) {
                $this->addError('old_password', Yii::t('frontend', 'incorrect_password.'));
            }
        }
    }

    public function getUser()
    {
        return $this->user;
    }

    public function save()
    {   
        if ($this->new_password) {
            $this->user->setPassword($this->new_password);
        }
        return $this->user->save();
    }
}
