<?php
use yii\helpers\Url;
use yii\web\View;
$this->title = Yii::t('frontend', 'audio_page_title');
$assets = \frontend\v2\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$photo_day = [
	'url'=> Url::to(['/news/app/index', 'f'=>'photoday']), 
	'icon'=>$baseUrl.'/images/svg/rightSide/camera.svg'
];
$selected_articles = ['url'=> Url::to(['/news/app/index', 'f'=>'selected'])];
// $out = array_values($audios);
// $news = json_encode($out);
?>
<div class="blocks-wrapper">
	<div class="main-block">
		<div class="podcast">
			 <h3 class="podcast-title"><?=Yii::t('frontend', 'audio_page_title')?></h3>
			<div class="podcast_wrapper">
				<div style="display: none;" id="player-cont">
					<div class="podcast-audio_controls play-pause">
	                  <div class="playBtn-wrap btn-wrap">
	                    <img class="control-btn play-btn" src="<?=$baseUrl?>/images/svg/rightSide/play-button.svg"
	                      alt="play button">
	                  </div>
	                  <div class="pauseBtn-wrap btn-wrap">
	                    <img class="control-btn pause-btn" src="<?=$baseUrl?>//images/svg/rightSide/pause-bars.svg"
	                      alt="pause button">
	                  </div>
	                </div>
				</div>
				<?php if ($audios):?>
					<?php foreach($audios as $data):?>

						<div class="podcast_item">
			              <div class="podcast_item_audio">
			                

			                <div class="podcast-track waveform">
			                  <audio src="<?=$data['sources']['mp3']?>"></audio>
			                </div>
			              </div>

			              <div class="podcast_item_info">

			                <div class="podcast-title_link">
			                  <?=$data['title']?>
			                </div>
			                <div class="podcast-views_count">
			                  <div class="podcast-date">
			                    <span class="date"><?=$this->getPublishDay($data['published'])?></span>
			                  </div>
			                </div>
			              </div>
			            </div>



					<?php endforeach;?>
				<?php endif;?>


			</div>
		</div>
	</div>
	<div class="rightSide-block">
		<div class="rightSide-news_top" id="photo_day" data='<?=json_encode($photo_day)?>'></div>
		<div class="rightSide-ad">
        	<?=\frontend\v2\widgets\AdDesktop::widget(['place'=>2, 'link_class'=>'right-ads'])?>
        </div>
        <div class="rightSide-actual" id="actual_news_right" data='<?=json_encode($selected_articles)?>'></div>
        <div class="rightSide-app">
        	<h3 class="rightSide-app_title"><?=Yii::t('frontend', 'Mobile_apps')?></h3>
          	<div class="rightSide-app_wrapper">
            	<a class="app-link" href="https://play.google.com/store/apps/details?id=uz.xabardor" target="_blank">
             		<img src="<?=$baseUrl?>/images/rightside/playMarket.png" alt="Google Play" />
            	</a>
            	<a class="app-link" href="https://apps.apple.com/us/app/xabardor/id1477791938?l=ru&ls=1" target="_blank">
              		<img src="<?=$baseUrl?>/images/rightside/appStore.png" alt="App Store">
            	</a>
          	</div>
        </div>
	</div>
</div>
<?php
$this->registerJs('
	audiojs.events.ready(function() {
		var as = audiojs.createAll();
	});	
', View::POS_END, 'audio_news');
?>