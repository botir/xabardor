<?php 
$this->title = Yii::t('frontend', 'meta_lenta_title');
?>
<div class="main-content">
	<div class="white-block mt-0">
		<div class="unstyled news-list lenta" id="list-more">
			<?php if ($list_news):?>
				<?php foreach($list_news as $data): $tags = $this->getTags($data['more_option']);?>
					
			        <div class="news-list__item item-al">
			        	<?php if ( $data !== reset( $list_news )) echo '<div class="dashed-divider"></div>';?>
			        	<ul class="unstyled infolist">
			                <li class="infolist__item">
			                  <i class="icon-clock-icon"></i>
			                  <span class="no-timeago"> <?=$this->timeAgo($data['published_at'])?></span>
			                </li>
			                
			                <li class="infolist__item">
			                  <i class="icon-eye"></i>
			                  <?=$data['views_count']?>
			                </li>
			              </ul>
			        	<?php if ($data['list_image']):?>
				        	<a class="news-list__link" href="<?=$this->getArticleUrl($data['slug'])?>">
				              <img class="post-img" src="<?=$this->getListImage($data)?>"/>
				            </a>
			            <?php endif;?>
			            <div class="news-list__content">
			            	<h6 class="news-list__title news-title">
			                	<a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
			              	</h6>
			              	<?php if ($data['show_desc']){
			              		echo '<p class="news-content">'.$data['description'].'</p>';
			              	}?>
			              	
			              	<?php 
		                    	if ($tags){ echo '<ul class="unstyled taglist">';
		                        	foreach ($tags as $tag) {
		                            echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
		                        }
		                        echo '</ul>';
		                    }?>

			            </div>
			        </div>		
				<?php endforeach;?>		
			<?php endif;?>	
		</div>
	</div>
	<?php if ($next_page):?>
		<div class="ads-block">
		    <?=\frontend\v2\widgets\AdMobile::widget(['place'=>4, 'link_class'=>'center-ads'])?>
		</div>
		<div class="load-panel__button" id="load-more-btn">
	      <a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
	    </div>
	<?php endif;?>
</div>