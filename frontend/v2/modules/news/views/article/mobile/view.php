<?php
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\View;
$assets = \frontend\v2\assets\MobileAsset::register($this);
$baseUrl = $assets->baseUrl;
$comment_url = Url::to(['/news/disqus']);
$default_avatar = $baseUrl.'/images/ic_profile.svg';
$article_options =  $this->getOptionArticle($article['more_option']);
if ($profile){
	$profile__avatar = $profile->getAvatar();	
}else{
	$profile__avatar = $default_avatar;
}
$this->title = $article['title'];
?>
<div class="main-content">
	<div class="content white-block mt-0">
		<div class="article">
			<div class="article__header">
				<h1 class="article__title">
					<?=$article['title']?>
				</h1>
					<ul class="unstyled infolist">
		              <li class="infolist__item">
		                <i class="icon-clock-icon"></i>
		                <span><?=$this->getPublishedDay($article['published_at'])?></span>
		              </li>
		              <li class="infolist__item">
		                <i class="icon-eye"></i>
		                <span><?=$article['views_count']?></span>
		              </li>
		            </ul>
		            <ul class="unstyled infolist">
		              <?php if($array_data['is_allowcoment']):?>
		              <li class="infolist__item">
		                <a class="infolist__link" href="#">
		                  <i class="icon-bubble-icon"></i>
		                  <span><?=$article['comment_count']?> ta kommentariya</span>
		                </a>
		              </li>
		        		<?php endif; ?>
		        		<?php if (!Yii::$app->user->isGuest&&Yii::$app->user->can('manager')):?>
			         		<li class="infolist__item">
			         			<a href="<?=Yii::getAlias('@backendUrl').'/'.'news/article/update?id='.$article['id']?>" class="btn btn-success signform__btn w-100">To‘g‘irlash</a>
			         		</li>
                        <?php endif; ?>
		        	</ul>
		        <div class="dashed-divider"></div>
		        <div class="horizontal-social">
		        	<div class="like-box">
		        		<button type="button" class="like-box__btn" data-url=<?=Url::to(['/news/article/like', 'tab_number'=>$article['tab_number']])?>>
		        			<i class="icon-like like-box__img"></i>
		        		</button>
		        		<span class="like-box__txt"><?=$array_data['like_count']?></span>
		        	</div>
		        	<ul class="unstyled social horizontal">
		        		<li class="social__item"><a class="social__link fb s_facebook" href="#"><i class="icon-facebook-icon"></i></a></li>
		        		<li class="social__item"><a class="social__link twitter s_twitter" href="#"><i class="icon-twitter-icon"></i></a></li>
		        		<li class="social__item"><a class="social__link ok s_odnoklassniki" href="#"><i class="icon-ok-icon"></i></a></li>
		        	</ul>
		        </div>
			</div>
			<div class="article__body">
				<article class="article__post">
					<?php if($article_options['show_image']):?>
					<p>
                		<img src="<?=$article['image_base_url'].$article['image_path']?>" />
              		</p>
              		<?php if($article_options['caption_image']):?>
              			<div class="dashed-divider"></div>
              			<p class="gray-color"><?=$article_options['caption_image']?></p>
              		<?php endif;?>
              		<?php endif;?>
					<?=$article['content']?>
				</article>
				<div class="dashed-divider"></div>
				<div class="tags trend"><div class="tags__title">Teglar</div>
 				<?php 
                 	if ($tags){ echo '<ul class="unstyled taglist">';
                        foreach ($tags as $tag) {
                            echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
                        }
                        echo '</ul>';
                    }
                ?>
 				</div>
 				<div class="ads-block">
			    <?=\frontend\v2\widgets\AdMobile::widget(['place'=>4, 'link_class'=>'center-ads'])?>
			</div>
 				<div class="dashed-divider"></div>
			</div>

			
          	
          	<?php if($array_data['is_allowcoment']):?>
          		<div class="article__footer clearfix">
		            <div class="follow">
		              <a href="<?=$this->getAuthorUrl($article['nickname'])?>" class="follow__img" style="background: url(<?=$article['avatar_base_url'].$article['avatar_path']?>) no-repeat center center; background-size: contain;"></a>
		              <div class="follow__body">
		                <div class="follow__header clearfix">
		                  <p class="follow__position float-left"><?=Yii::t('frontend','author_kolomnist')?></p>
		                </div>
		                <div class="follow__content">
		                  <h3 class="follow__title"><a href="<?=$this->getAuthorUrl($article['nickname'])?>"><?=$article['full_name']?></a></h3>
		                  <p class="follow__text"><a href="<?=$this->getAuthorUrl($article['nickname'])?>"><?=$article['user_status']?></a></p>
		                </div>
		              </div>
		            </div>
	          	</div>
          		
          		<div class="dashed-divider"></div>
          		<div class="comment" id="isso-thread" av="<?=$default_avatar?>" data-isso-id="<?=$article['tab_number']?>" data-user="<?=$profile?1:0?>">
          			<?php if(!$profile):?>
		            	<div class="comment__notification success"><?=Yii::t('frontend', 'auth_for_comment')?><ul>
				        	<li class="toplinks__item">
				            	<a href="<?=Url::to(['/user/sign-in/login'])?>" class="toplinks__link" id="sign-link">
				                    <span><?=Yii::t('frontend', 'user_sign_login')?></span>
				                </a>
				            </li>
				        </ul></div>
		            <?php endif;?>
		        </div>
          	<?php endif;?>
          	<div class="dashed-divider"></div>
          	<?php if ($related):?>
	          	<div class="related-news">
	          		<div class="hot-news">
	          			<?php foreach($related as $data): $tags_r = $this->getTags($data['more_option']);?>
	          				<div class="hot-news__item">
				                <a href="<?=$this->getArticleUrl($data['slug'])?>" class="hot-news__link">
				                  <img class="hot-news__img post-img" src="<?=$this->getListImage($data)?>" />
				                </a>
				                <div class="hot-news__body">
				                  <h6 class="hot-news__title news-title">
				                    <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
				                  </h6>
				                  <p class="hot-news__content news-content"><?=$data['description']?></p>
				                  <div class="dashed-divider"></div>
				                   <?php 
					                    if ($tags_r){ echo '<ul class="unstyled taglist">';
					                        foreach ($tags_r as $tag) {
					                            echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
					                        }
					                        echo '</ul>';
					                    }
				                	?>
				                </div>
				              </div>
	          			<?php endforeach;?>	
	          		</div>
	          	</div>
          <?php endif;?>
		</div>
	</div>
</div>
<?php if($array_data['is_allowcoment']):?>
<script	data-isso="<?=$comment_url?>" data-isso-avatar-image="<?=$profile__avatar?>" src="<?=$baseUrl?>/js/embed.dev.js"></script>
<?php endif; ?>
<?php 
$js_options = [
	'title' => $article['title'],
	'text' => $article['description'],
    'image' => $this->getListImage($article),
    'url' => $this->getShortUrl($article['tab_number'])
];
$options = Json::encode($js_options);
$this->registerJs('$(".social__link").ShareLink(' . $options . ');', View::POS_READY,'sharingnews');
?>