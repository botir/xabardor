<div class="main-content bycategory">
	<div class="white-block mt-0">
		<div class="follow">
			<a href="#" class="follow__img" style="background: url(<?=$author['avatar_base_url'].$author['avatar_path']?>) no-repeat center center; background-size: contain;"></a>
			<div class="follow__body">
				<div class="follow__content">
					<h3 class="follow__title"><a href="#"><?=$author['full_name']?></a></h3>
					<p class="follow__text"><a href="#"><?=$author['description']?></a></p>
					<ul class="unstyled follow__list">
		                <li class="follow__item">
		                   	<?=Yii::t('frontend','author_count_article')?>: <span><?=$author_article_count?></span>
		                </li>
		            </ul>
				</div>
			</div>
		</div>
	</div>
	<div class="white-block news-block">
		<div class="popular">
			<?php if ($tags):?>
				<ul class="popular__tabs">
					<li class="popular__tab-link current" data-tab="tab-all"><a href="<?=$this->getAuthorUrl($author['nickname'])?>" ><?=Yii::t('frontend','author_tag_all')?></a></li>
	              	<?php foreach($tags as $tag):?>
	                	<li class="popular__tab-link" data-tab="tab-<?=$tag['slug']?>"><a href="<?=$this->getAuthorUrl($author['nickname'], $tag['slug'])?>"><?=$tag['title']?></a></li>
	            	<?php endforeach;?>
				</ul>
			<?php endif;?>
		</div>
	</div>
	<div id="list-more" class="popular__tab-content current">
		<?php if ($articles):?>
			<?php foreach($articles as $data): $tags = $this->getTags($data['more_option']);?>
				<div class="white-block item-al">
			        	<ul class="unstyled infolist">
			                <li class="infolist__item">
			                  <i class="icon-clock-icon"></i>
			                  <span class="no-timeago"> <?=$this->timeAgo($data['published_at'])?></span>
			                </li>
			                
			                <li class="infolist__item">
			                  <i class="icon-eye"></i>
			                  <?=$data['views_count']?>
			                </li>
		              	</ul>
					<div class="news-list">
						<div class="news-list__item">
							<?php if ($data['list_image']):?>
						  		<a class="news-list__link" href="<?=$this->getArticleUrl($data['slug'])?>">
						    		<img class="post-img" src="<?=$this->getListImage($data)?>"/>
						  		</a>
					  		<?php endif; ?>	
						  	<div class="news-list__content">
						    	<h6 class="news-list__title news-title">
						      		<a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
						    	</h6>
						    	<?php if ($data['show_desc']):?>
						    		<p class="news-list__text news-content"><?=$data['description']?></p>
								<?php endif; ?>						    		
						    	
							    <?php 
					                if ($tags){ echo '<ul class="unstyled taglist">';
					                	foreach ($tags as $tag) {
					                        echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
					                    }
				                    echo '</ul>';}?>
						  	</div>
						</div>
					</div>
		        </div>
			<?php endforeach;?>
			<?php if ($next_page):?>
	    		<div class="load-panel__button" id="load-more-btn">
	      			<a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
	    		</div>
  			<?php endif;?>
		<?php endif;?>

	</div>
</div>