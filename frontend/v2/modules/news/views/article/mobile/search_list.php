<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$trend_tags = ['url'=> Url::to(['/news/app/index', 'f'=>'tags'])];
$this->title = Yii::t('frontend','meta_search_title');
?>

<div class="main-content search-content">
	<div class="content">
		<?php 
			echo Html::beginForm(Url::to(['/news/article/search']), 'get', ['class' => 'search-form']);
			echo '<div class="form-group">';
				echo Html::textInput('q', $search_text?$search_text:'',
		          ['placeholder' => Yii::t('frontend','Find'),'required'=>'true', 'class'=>'search-form__input', 'id'=>'search-input'
		        ]);
		    echo '</div>';
		    echo Html::endForm();
		?>
		<div class="bycategory">
			<div id="list-more" class="popular__tab-content current">				
					<?php if ($articles):?>
						<?php foreach($articles as $data): $tags = $this->getTags($data['more_option']);?>
			            	<div class="white-block item-al">
			            		<ul class="unstyled infolist">
					                <li class="infolist__item">
					                  <i class="icon-clock-icon"></i>
					                  <span class="no-timeago"> <?=$this->timeAgo($data['published_at'])?></span>
					                </li>
					                
					                <li class="infolist__item">
					                  <i class="icon-eye"></i>
					                  <?=$data['views_count']?>
					                </li>
				              	</ul>
				              	<div class="news-list">
				                	<div class="news-list__item">
				                		<?php if ($data['list_image']):?>
						                 	<a class="news-list__link" href="<?=$this->getArticleUrl($data['slug'])?>">
						                    	<img class="post-img" src="<?=$this->getListImage($data)?>"/>
						                  	</a>
						                <?php endif; ?>	
					                  	<div class="news-list__content">
					                    	<h6 class="news-list__title news-title">
					                      		<a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
					                    	</h6>
					                    	<?php if ($data['show_desc']):?>
					                    		<p class="news-list__text news-content"><?=$data['description']?></p>
					                    		<div class="dashed-divider"></div>
					                    	<?php endif; ?>	
					                    <?php if ($tags){ echo '<ul class="unstyled taglist">';
						                        foreach ($tags as $tag) {
						                            echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
						                        }
						                echo '</ul>';}?>
					                  </div>
				                </div>
				              </div>
				            </div>
						<?php endforeach;?>	
						<?php if ($next_page):?>
					        <div class="load-panel__button" id="load-more-btn">
					          <a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
					        </div>
			      		<?php endif;?>							
					<?php else: ?>
						<div class="subscribe">
			            <div class="row">
			              <div class="col-md-12">
			                <p>
			                    <?=Yii::t('frontend', 'not_found_search')?>
			                </p>
			              </div>
			            </div>
			          </div>
					<?php endif; ?>				
			</div>
		</div>

	</div>
</div>