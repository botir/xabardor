<?php
use yii\helpers\Url;
$this->title = Yii::t('frontend','article is {tag}',['tag'=>$tag['title']]);
$assets = \frontend\v2\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$photo_day = [
	'url'=> Url::to(['/news/app/index', 'f'=>'photoday']), 
	'icon'=>$baseUrl.'/images/svg/rightSide/camera.svg'
];
$audio_news_js = [
  'url'=> Url::to(['/news/app/index','f'=>'audio']),
  'main_url'=> Url::to(['/news/audio/index']),
  'title'=> Yii::t('frontend','audio_page_title'),
  'title_load'=> Yii::t('frontend','All_lenta'),
  'base_url'=> $baseUrl
];
$selected_articles = ['url'=> Url::to(['/news/app/index', 'f'=>'selected'])];
?>
<div class="blocks-wrapper">
	<div class="main-block all-news_block">
		<div class="container" id="lenta-news">
			<div class="rubric-content">
				<h3 class="rubric-name"><?=$tag['title']?></h3>
			</div>
			<?php if ($articles):?>
				<div class="daily-news listn-item">
					<div class="daily-news_wrapper">
						<?php if (isset($articles[0]) && $articles != $articles):?>
							<h4 class="daily-news_title"><?=$this->getPublishDay($articles[0]['published_at'])?></h4>
						<?php endif;?>
				<?php $i=0; $old_day=''; foreach($articles as $data):
					$pub_date = $this->getPublishDate($data['published_at']);
				?>
				<?php if ($i!=0 && $to_day!=$pub_date && $old_day!=$pub_date):?>
					 </div></div>
                  	<div class="daily-news listn-item">
                     	<h4 class="daily-news_title"><?=$this->getPublishDay($data['published_at'])?></h4>
                     	<div class="daily-news_wrapper">
                <?php $old_day=$pub_date; endif;?> 
				<div class="daily-news_item">
                	<span class="news-time"><?=$this->getPublishTime($data['published_at'])?></span>

	                <div class="news-heading">
	                	<a class="heading-text_link" href="<?=$this->getArticleUrl($data['slug'])?>">
	                    	<?=$data['title']?>
	                  	</a>
	                  	<a class="news-rubric_link" href="<?=$this->getCategoryUrl($data['category_slug'])?>"><?=$data['category_title']?></a>
	                </div>
              	</div>
				<?php $i++; endforeach;?>		
			</div></div><?php endif;?>
		</div>
		<?php if ($next_page):?>
			<a class="load-more_btn next" href="<?=$next_page?>" id="pagination-list"><?=Yii::t('frontend','load_more_btn')?></a>
		<?php endif;?>
	</div>
	<div class="rightSide-block">
		<div class="rightSide-news_top" id="photo_day" data='<?=json_encode($photo_day)?>'></div>
		<div class="rightSide-ad">
        	<?=\frontend\v2\widgets\AdDesktop::widget(['place'=>2, 'link_class'=>'right-ads'])?>
        </div>
        <div class="rightSide-podcast" id="audio_news_right" data='<?=json_encode($audio_news_js)?>'></div>
        <div class="rightSide-actual" id="actual_news_right" data='<?=json_encode($selected_articles)?>'></div>
        <div class="rightSide-app">
        	<h3 class="rightSide-app_title"><?=Yii::t('frontend', 'Mobile_apps')?></h3>
          	<div class="rightSide-app_wrapper">
            	<a class="app-link" href="https://play.google.com/store/apps/details?id=uz.xabardor" target="_blank">
             		<img src="<?=$baseUrl?>/images/rightside/playMarket.png" alt="Google Play" />
            	</a>
            	<a class="app-link" href="https://apps.apple.com/us/app/xabardor/id1477791938?l=ru&ls=1" target="_blank">
              		<img src="<?=$baseUrl?>/images/rightside/appStore.png" alt="App Store">
            	</a>
          	</div>
        </div>
	</div>
</div>