<?php
use yii\helpers\Url;
$trend_tags = ['url'=> Url::to(['/news/app/index', 'f'=>'tags'])];
?>

<div class="main-content">
	<div class="container">
		<div class="main-section bycategory">
			<div class="white-block mt-0 clearfix">  
	            <div class="follow">
	              	<div class="follow__img" style="background: url(<?=$author['avatar_base_url'].$author['avatar_path']?>) no-repeat center center; background-size: cover;"></div>
	              <div class="follow__body">
	                <div class="follow__header clearfix">
	                	<p class="follow__position float-left"><?=Yii::t('frontend','author_kolomnist')?></p>                  
	                </div>
	                <div class="follow__content">
	                	<h3 class="follow__title"><?=$author['full_name']?></h3>
	                  	<p class="follow__text"><?=$author['description']?></p>
	                	<ul class="unstyled follow__list">
		                    <li class="follow__item">
		                      <?=Yii::t('frontend','author_count_article')?>: <span><?=$author_article_count?></span>
		                    </li>
	                  	</ul>
	                </div>
	              </div>
	            </div>
	        </div>
	        <?php if ($tags):?>
	        <div class="white-block news-block">
	            <div class="popular">
	              <ul class="popular__tabs">
	              		<li class="popular__tab-link current" data-tab="tab-all"><a href="<?=$this->getAuthorUrl($author['nickname'])?>" ><?=Yii::t('frontend','author_tag_all')?></a></li>
	              	<?php foreach($tags as $tag):?>
	                	<li class="popular__tab-link current" data-tab="tab-<?=$tag['slug']?>"><a href="<?=$this->getAuthorUrl($author['nickname'], $tag['slug'])?>"><?=$tag['title']?></a></li>
	            	<?php endforeach;?>
	              </ul>    
	            </div>
	        </div>
	    <?php endif;?>

	    <div id="list-more" class="popular__tab-content current">
	    	<?php if ($articles):?>
	    		<?php foreach($articles as $data): $tags = $this->getTags($data['more_option']);?>
	                	<div class="white-block item-al">
	                		<ul class="unstyled infolist">
				                <li class="infolist__item">
				                  <i class="icon-clock-icon"></i>
				                  <span class="no-timeago"> <?=$this->timeAgo($data['published_at'])?></span>
				                </li>
				                <li class="infolist__item">
				                  <i class="icon-eye"></i>
				                  <?=$data['views_count']?>
				                </li>
				              </ul>
			              <div class="news-list tabnews">
			                <div class="news-list__item">
			                  <a class="news-list__link mobile-only" href="<?=$this->getArticleUrl($data['slug'])?>">
			                    <img src="<?=$this->getListImage($data)?>" />
			                  </a>
			                  <div class="news-list__content">
			                    <h4 class="news-list__title news-title">
			                      <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
			                    </h4>
			                    <p class="news-list__text news-content"><?=$data['description']?></p>
			                    <div class="dashed-divider"></div>
			                    <?php 
				                     if ($tags){ echo '<ul class="unstyled taglist">';
				                        foreach ($tags as $tag) {
				                            echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
				                        }
				                        echo '</ul>';
				                    }
				                ?>
			                  </div>
			                  <a class="news-list__link" href="<?=$this->getArticleUrl($data['slug'])?>">
			                    <img src="<?=$this->getListImage($data)?>" />
			                  </a>
			                </div>
			              </div>
			            </div>
	            	<?php endforeach;?>
	            	<?php if ($next_page):?>
			        <div class="load-panel__button" id="load-more-btn">
			          <a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
			        </div>
			      <?php endif;?>
	    	<?php endif;?>
	    </div>

		</div>
		<div class="right-sidebar">
			<div class="white-block mt-0" id="trend_tags" data='<?=json_encode($trend_tags)?>'>
			</div>
			<div class="ads-block">
            	<?=\frontend\v2\widgets\AdDesktop::widget(['place'=>2, 'link_class'=>'right-ads'])?>
            </div>
		</div>

	</div>
</div>