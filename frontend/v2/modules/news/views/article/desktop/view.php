<?php
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\View;
$assets = \frontend\v2\assets\DesktopAsset::register($this);
$article_options =  $this->getOptionArticle($article['more_option']);
$baseUrl = $assets->baseUrl;
$comment_url = Url::to(['/news/disqus']);
$default_avatar = $baseUrl.'/images/ic_profile.svg';
if ($profile){
	$profile__avatar = $profile->getAvatar();	
}else{
	$profile__avatar = $default_avatar;
}
$this->title = $article['title'];
$top_articles = ['url'=> Url::to(['/news/app/index', 'f'=>'top'])];
$photo_day = [
	'url'=> Url::to(['/news/app/index', 'f'=>'photoday']), 
	'icon'=>$baseUrl.'/images/svg/rightSide/camera.svg'
];
$audio_news_js = [
  'url'=> Url::to(['/news/app/index','f'=>'audio']),
  'main_url'=> Url::to(['/news/audio/index']),
  'title'=> Yii::t('frontend','audio_page_title'),
  'title_load'=> Yii::t('frontend','All_lenta'),
  'base_url'=> $baseUrl
];
$selected_articles = ['url'=> Url::to(['/news/app/index', 'f'=>'selected'])];
?>
<div class="blocks-wrapper">
	<div class="main-block opened-news_block">
		<div class="opened-news_content">
			<div class="opened-news_img">
              	<img src="<?=$article['image_base_url'].$article['image_path']?>" />
            </div>
            <div class="container">
            <?php 
          		if($article_options['caption_image']):?>
      			<span class="source-img"><?=$article_options['caption_image']?></span>
      		<?php endif; ?>
	      			<div class="news-item_info">
		                <div class="news-item_time">
		                  <span class="time"><?=$this->getPublishedDay($article['published_at'])?></span>
		                </div>
	                	<div class="news-item_view">
	                  		<img class="view-icon" src="<?=$baseUrl?>/images/svg/main/eye.svg" alt="view icon">
	                  		<span class="view-count"><?=$article['views_count']?></span>
	                	</div>
	              	</div>
	              	<?php echo '<a href="'.$this->getCategoryUrl($article['category_slug']).'" class="opened-news_rubric">'.$article['category_title'].'</a>';
	                ?>
	              	<div class="opened-news_heading">
	              		<h4 class="opened-news_title"><?=$article['title']?></h4>

	              		<div class="opened-news_info">
		                	<span class="opened-news_date"><?=$this->getPublishedDay($article['published_at'])?></span>

		                  	<div class="opened-news_views">
		                    	<img src="<?=$baseUrl?>/images/svg/main/grey_eye.svg" class="views-icon">
		                    	<span class="views-count"><?=$article['views_count']?></span>
		                  	</div>
		                </div>
		                <div class="opened-news_text">
		                	<?=$article['content']?>
		                </div>
	              	</div>
      		</div>
      		<div class="opened-news_comments"></div>
      		<div class="ad-banner"></div>
      		<div class="similar-news">
      			<?php if ($related):?>
      				<h3 class="similar-news_title"><?=Yii::t('frontend', 'Related_news')?></h3>
      				<div class="similar-news_wrapper">
      					<?php foreach($related as $data):?>
      						<div class="news-small_item">
				                <div class="small-news_img">
				                  <img src="<?=$this->getListImage($data)?>" />
				                </div>

				                <div class="small-news_content">
				                  <h3 class="small-news_heading">
				                    <a class="small-news_heading__link" href="<?=$this->getArticleUrl($data['slug'])?>">
				                      <?=$data['title']?>
				                    </a>
				                  </h3>
				                  <p class="small-news_text">
				                   <?=$data['description']?>
				                  </p>
				                </div>
				              </div>
      					<?php endforeach;?>
      				</div>
      			<?php endif;?>
      		</div>
      		<div class="popular-news" id="top_articles" data='<?=json_encode($top_articles)?>'></div>
		</div>

	</div>
	<div class="rightSide-block">
		<div class="rightSide-news_top" id="photo_day" data='<?=json_encode($photo_day)?>'></div>
		<div class="rightSide-ad">
        	<?=\frontend\v2\widgets\AdDesktop::widget(['place'=>2, 'link_class'=>'right-ads'])?>
        </div>
        <div class="rightSide-podcast" id="audio_news_right" data='<?=json_encode($audio_news_js)?>'></div>
        <div class="rightSide-actual" id="actual_news_right" data='<?=json_encode($selected_articles)?>'></div>
        <div class="rightSide-app">
        	<h3 class="rightSide-app_title"><?=Yii::t('frontend', 'Mobile_apps')?></h3>
          	<div class="rightSide-app_wrapper">
            	<a class="app-link" href="https://play.google.com/store/apps/details?id=uz.xabardor" target="_blank">
             		<img src="<?=$baseUrl?>/images/rightside/playMarket.png" alt="Google Play" />
            	</a>
            	<a class="app-link" href="https://apps.apple.com/us/app/xabardor/id1477791938?l=ru&ls=1" target="_blank">
              		<img src="<?=$baseUrl?>/images/rightside/appStore.png" alt="App Store">
            	</a>
          	</div>
        </div>
	</div>
</div>