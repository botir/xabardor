<?php
use yii\helpers\Url;
$assets = \frontend\v2\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$top_articles = ['url'=> Url::to(['/news/app/index', 'f'=>'top'])];
$selected_articles = ['url'=> Url::to(['/news/app/index', 'f'=>'selected'])];
$top_tags = ['url'=> Url::to(['/news/app/index', 'f'=>'tags'])];
$photo_day = [
	'url'=> Url::to(['/news/app/index', 'f'=>'photoday']), 
	'icon'=>$baseUrl.'/images/svg/rightSide/camera.svg'
];
$audio_news_js = [
  'url'=> Url::to(['/news/app/index','f'=>'audio']),
  'main_url'=> Url::to(['/news/audio/index']),
  'title'=> Yii::t('frontend','audio_page_title'),
  'title_load'=> Yii::t('frontend','All_lenta'),
  'base_url'=> $baseUrl
];
$this->title = Yii::t('frontend','default_title');

?>

<div class="blocks-wrapper">
	<div class="main-block">
		<div class="news-main_content">
			<div class="leftSide-news">
				<h3 class="leftSide-news_title"><?=Yii::t('frontend', 'news_list')?></h3>
				<div class="leftSide-news_wrapper">
					<?php foreach($list_news['articles'] as $data):?>
						<div class="leftSide-news_item">
			            	<div class="text-wrap">
			                	<div class="news-item_info">
				                    <div class="news-item_time">
				                      <span class="time"><?=$this->getPublishedDay($data['published_at'])?></span>
				                    </div>
				                    <div class="news-item_view">
				                      <img class="view-icon" src="<?=$baseUrl?>/images/svg/main/eye.svg" alt="view icon">
				                      <span class="view-count"><?=$data['views_count']?></span>
				                    </div>
			                  	</div>
			                  
				                <div class="news-item_heading">
				                    <a href="<?=$this->getArticleUrl($data['slug'])?>">
				                      <?=$data['title']?>
				                    </a>
				                </div>
			                </div>
			            </div>
					<?php endforeach;?>						
				</div>
				<a href="<?=Url::to(['/news/article/lenta'])?>" class="more-news_link">
		          <span class="more-link_text"><?=Yii::t('frontend','load_more_btn')?></span>
		          <span class="more-link_icon">
		            <img src="<?=$baseUrl?>/images/svg/rightSide/play-button.svg" alt="play-button">
		          </span>
		        </a>
			</div>
			<div class="central-news">
      			<?php foreach($main_news as $data):;
                    if ( $data === reset( $main_news )):?>
                    	<div class="central-news_big">
			              <img class="big-news_cover" src="<?=$this->getListImage($data)?>" />

			              <div class="big-news_content">
			                <?php 
			                	echo '<a href="'.$this->getCategoryUrl($data['category_slug']).'" class="rubric">'.$data['category_title'].'</a>';
                          	?>
			                <h3 class="big-news_heading">
			                  <a class="big-news_heading__link" href="<?=$this->getArticleUrl($data['slug'])?>">
			                    <?=$data['title']?>
			                  </a>
			                </h3>
			                <p class="big-news_text"><?=$data['description']?></p>
			              </div>
			            </div><div class="central-news_small">
                    <?php else: ?>
                    	<div class="news-small_item">
			                <div class="small-news_img">
			                  <img src="<?=$this->getListImage($data)?>" />
			                </div>
			              
			                <div class="small-news_content">
			                <?php echo '<a href="'.$this->getCategoryUrl($data['category_slug']).'" class="small-news_rubric">'.$data['category_title'].'</a>';
                          	?>
				                <h3 class="small-news_heading">
				                	<a class="small-news_heading__link" href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
				                </h3>
			                  	<p class="small-news_text"><?=$data['description']?></p>
			                </div>
			           	</div>
                	<?php endif;?>
                <?php endforeach;?>
                </div>
      		</div>
      		<div class="more-news">
      			<?php if ($selected):?>
      				<?php foreach($selected as $data):?>
      					<div class="news-small_item">
			              	<div class="small-news_img">
			                	<img src="<?=$this->getListImage($data)?>" />
			              	</div>
			              	<div class="small-news_content">
			              		<?php 
	                            
	                               echo '<a href="'.$this->getCategoryUrl($data['category_slug']).'" class="small-news_rubric">'.$data['category_title'].'</a>';
	                            	
	                            
                          		?>	
			                	<h3 class="small-news_heading">
			                  	<a class="small-news_heading__link" href="<?=$this->getArticleUrl($data['slug'])?>">
			                    	<?=$data['title']?>
			                  	</a>
			                	</h3>
			                	<p class="small-news_text"><?=$data['description']?></p>
			              	</div>
			            </div>
      				<?php endforeach;?>
      			<?php endif;?>
      		</div>
      		<div class="video-block"></div>
      		<?php if ($category_news): ?>
      			<?php foreach($category_news as $tags): $i=1;?>
      				<div class="rubric-content"><h3 class="rubric-name"><?=$tags['tag']?></h3><div class="rubric-news_wrapper">
      					<?php foreach($tags['news'] as $data):?>
      						<?php if ($i==1):?>
      							<div class="rubric-news_main">
				                	<div class="rubric-news_main__img">
				                  		<img src="<?=$this->getListImage($data)?>" />
				                	</div>
				                	<div class="rubric-news_main__info">
				                  		<a href="<?=$this->getArticleUrl($data['slug'])?>" class="main-news_title">
				                    	<?=$data['title']?>
				                  	</a>
				                  	<p class="main-news_text">
				                    	<?=$data['description']?>
				                  	</p>
				                	</div>
				              	</div><div class="rubric-news_other">
      						<?php else:?>
      							<div class="news-small_item">
				                	<div class="small-news_img">
				                		<img src="<?=$this->getListImage($data)?>" />
				                  	</div>
				                  	<div class="small-news_content">
				                    	<h3 class="small-news_heading">
				                      	<a class="small-news_heading__link" href="<?=$this->getArticleUrl($data['slug'])?>">
				                        	<?=$data['title']?>
				                      	</a>
				                    	</h3>
				                    	<p class="small-news_text"><?=$data['description']?></p>
				                  	</div>
				                </div>
      						<?php endif;?>
      					<?php $i++;endforeach;?> </div>
      				</div></div>
      			<?php endforeach;?>
      		<?php endif; ?>
		</div>
	</div>
	<div class="rightSide-block">
		<div class="rightSide-news_top" id="photo_day" data='<?=json_encode($photo_day)?>'></div>
		<div class="rightSide-ad">
        	<?=\frontend\v2\widgets\AdDesktop::widget(['place'=>2, 'link_class'=>'right-ads'])?>
        </div>
        <div class="rightSide-podcast" id="audio_news_right" data='<?=json_encode($audio_news_js)?>'></div>
        <div class="rightSide-actual" id="actual_news_right" data='<?=json_encode($selected_articles)?>'></div>
        <div class="rightSide-app">
        	<h3 class="rightSide-app_title"><?=Yii::t('frontend', 'Mobile_apps')?></h3>
          	<div class="rightSide-app_wrapper">
            	<a class="app-link" href="https://play.google.com/store/apps/details?id=uz.xabardor" target="_blank">
             		<img src="<?=$baseUrl?>/images/rightside/playMarket.png" alt="Google Play" />
            	</a>
            	<a class="app-link" href="https://apps.apple.com/us/app/xabardor/id1477791938?l=ru&ls=1" target="_blank">
              		<img src="<?=$baseUrl?>/images/rightside/appStore.png" alt="App Store">
            	</a>
          	</div>
        </div>
	</div>
</div>