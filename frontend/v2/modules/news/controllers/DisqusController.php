<?php

namespace frontend\v2\modules\news\controllers;

use Yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use frontend\v2\modules\news\models\Article;
use frontend\v2\modules\news\models\Comment;

/**
 * Disqus controller for the `news` module
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */

class DisqusController extends Controller
{
  const LIMIT_LIST = 20;

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['contentNegotiator'] = [
      'class' => 'yii\filters\ContentNegotiator',
      'formats' => [
          'application/json' => Response::FORMAT_JSON,
          'charset' => 'UTF-8',
      ]
    ];
    $behaviors['access'] = [
      'class' => AccessControl::class,
      'rules' => [
                    [
                      'actions' => ['new'],
                      'allow' => true,
                      'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'index', 'count'
                        ],
                        'allow' => true,
                        //'roles' => ['*']
                    ],
                ]
    ];
    return $behaviors;
  }

  public function actionCount(){
    return 0;
  }

  public function actionIndex(){
    $uri=1;
    $after=null;
    if (isset($_REQUEST['uri']))
        $uri=$_REQUEST['uri'];
    if (isset($_REQUEST['after'])){
      $after=date('Y-m-d H:i:s',$_REQUEST['after']);
    }
    else
        $after = '2000-01-01 23:59:59';
    if (isset($_REQUEST['sort']))
        $sort=$_REQUEST['sort'];
    else
      $sort = 'firts';

    $list = Comment::getListComments($uri, $after, self::LIMIT_LIST);
    $count_comments = Comment::getCountComments($uri);
    $count_hiddens = Comment::getHiddenComments($uri, $after);
    $formated = $this->getFormatComments($list);




    $result = ['total_replies'=>$count_comments, 'replies'=>$formated, 'id'=>null, 'hidden_replies'=>$count_hiddens-count($formated)];

    return $result;
  }

  public function actionGet($id){
    return [];
  }

  public function actionNew(){
    if (Yii::$app->user->isGuest){
      \Yii::$app->response->statusCode = 400;
      return ['parametrs'=>'required'];
    }
    $access = false;
    $current_user = Yii::$app->user->identity;
    $data = file_get_contents('php://input');
    if ($data){
      $request = json_decode($data, true);
      
      if (isset($_REQUEST['uri']))
        $obj_id = intval($_REQUEST['uri']);
      else{
        \Yii::$app->response->statusCode = 400;
        return ['parametrs'=>'required'];
      }

      $article = $this->findNews($obj_id);

      $model = new Comment();
      $model->content = $request['text'];
      $model->parent_id = $request['parent'];
      $model->user_id = $current_user->id;
      $model->article_id = $article->id;
      $model->status = 2;
      if ($model->save()) {
        \Yii::$app->response->statusCode = 201;
        $profile = $current_user->userProfile;
        return ['success'=>true, 'title'=>Yii::t('frontend', 'comment_to_moderation')];
      }else{
       return $model->getErrors();

      }
    }
    return ['success'=>false, 'title'=>'error'];
  }


  protected function findNews($tab_number)
  {
    if (($model = Article::findOne(['tab_number'=>$tab_number])) !== null) {
      return $model;
    } else {
      throw new \yii\web\HttpException(404, 'Not Found');
    }
  }

  protected function getFormatComments($list){
    $result = [];
    if ($list){
      foreach ($list as $data) {
        $result[] = [
          'website' => NULL,
          'author' => $data['full_name'],
          'author_avatar' => $data['avatar'],
          'parent' => $data['parent_id'],
          'created' =>  strtotime($data['created']),
          'text' =>  $data['content'],
          'dislikes' => 0,
          'modified' => NULL,
          'mode' => 1,
          'hash' =>  '1122',
          'acc' =>  false,
          'id' => $data['id'],
          'likes' => 0,
          'total_replies' => 0,
          'hidden_replies' => 0,
          'replies' => [],
        ];
      }
    }
    return $result;
  }
}
