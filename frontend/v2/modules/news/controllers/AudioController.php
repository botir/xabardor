<?php

namespace frontend\v2\modules\news\controllers;

use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use Yii;
use frontend\v2\components\FrontendController;
use frontend\v2\modules\news\models\Audio;
/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class AudioController extends FrontendController
{
	
    public function actionIndex()
    {
        $audios = $this->getAllList();
        $to_day = date('d.m.Y');
        if ($this->is_mobile){
            return $this->render('mobile/list', [
                'audios' => $audios,
                'to_day' => $to_day,
            ]);
        }
    	return $this->render('desktop/list', [
            'audios' => $audios,
            'to_day' => $to_day,
            'next_page' => null,
        ]);
        
    }
   

    protected function getAllList()
    {  
        $auios = Audio::getLastNews(20);
        if ($auios) {
            return $this->getFormatAudio($auios);
        }
        return [];
    }

    protected function getFormatAudio($audios){
        $result = [];
        foreach ($audios as $item) {
            $array_data = Json::decode($item['content']);

            $result[]=[
                'id'=>$item['id'],
                'title'=>$array_data['uz'],
                'sources'=>[
                  'mp3'=>$item['base_url'].$item['audio_path']
                ],
                'poster'=>$item['photo_base_url'].'/thumbnails/_medium'.$item['image_name'].'_medium.'.$item['image_ext'],
                'published'=> $item['published_at'],
                'duration'=> $item['duration'],
              ];
        }
        return $result;
    }
}
