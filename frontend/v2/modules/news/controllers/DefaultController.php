<?php

namespace frontend\v2\modules\news\controllers;

use frontend\v2\components\FrontendController;
use frontend\v2\modules\news\models\Article;
use frontend\v2\modules\news\models\Category;

/**
 * Default controller for the `news` module
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class DefaultController extends FrontendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    	$main_news = Article::getMainNews(2, 3);
    	$selected = Article::getMainNews(3, 3);
    	
        $list_news = $this->getLastNews($this->next, 7);
        $this->setRegisterTag();
    	if ($this->is_mobile){
	        return $this->render('mobile/index', [
	        	'main_news'=>$main_news, 
	        	'selected'=>$selected,
	        	'list_news'=>$list_news
	        ]);
	    }else{
            $categories = Category::listMain();
            $category_news = [];
            if ($categories){
                foreach ($categories as $category) {
                    $article = Article::listCategoryArticles($category['id'], $this->next, 4);
                    if ($article){                       
                        $category_news[]=[
                            'tag' => $category['title'],
                            'news' => $article
                        ];
                    }
                }
            }
           

	    	return $this->render('desktop/index', [
	        	'main_news'=>$main_news, 
	        	'selected'=>$selected,
	        	'list_news'=>$list_news,
                'category_news'=>$category_news
        	]);
	    }
    }
}
