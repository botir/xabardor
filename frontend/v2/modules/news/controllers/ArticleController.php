<?php

namespace frontend\v2\modules\news\controllers;

use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use Yii;
use common\models\UserAuthor;
use common\models\PaymentUser;
use common\models\PaymentService;
use frontend\v2\components\FrontendController;
use frontend\v2\modules\news\models\Article;
use frontend\v2\modules\news\models\Tag;
use frontend\v2\modules\news\models\Search;
/**
 * Article controller for the `news` module
* @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class ArticleController extends FrontendController
{

	/**
     * Renders the view for the module
     * @return string
     */
    public function actionView($slug)
    {
        $article = $this->findArticleSlug($slug);
        Article::updateViews($article['id']);
        $array_data = Json::decode($article['more_option']);
        
        if (isset($array_data['tags']) && count($array_data['tags']) > 0){
            $tags = $array_data['tags'];
        }else{
            $tags  = [];
        }
        $related = Article::getRelatedArticles($article['id'], $tags);
        
        if (Yii::$app->user->isGuest){
            $profile = null;    
        }else{
            $profile = Yii::$app->user->identity->userProfile;    
        }
        $image = $article['image_base_url'].'/thumbnails/_medium'.$article['image_name'].'_medium.'.$article['image_ext'];
        $this->setRegisterTag($article['title'], $article['description'], $image);
        if ($this->is_mobile){
            return $this->render('mobile/view', [
                'article' => $article,
                'tags' => $tags,
                'array_data' => $array_data,
                'related' => $related,
                'profile' => $profile,
            ]);
        }
    	return $this->render('desktop/view', [
            'article' => $article,
            'tags' => $tags,
            'array_data' => $array_data,
            'related' => $related,
            'profile' => $profile,
        ]);
        
    }

    /**
     * Renders the view for the module
     * @return string
     */
    public function actionSview($tab_number)
    {
        $article = $this->findArticleTabNumber($tab_number);
        Article::updateViews($article['id']);
        $array_data = Json::decode($article['more_option']);
        
        if (isset($array_data['tags']) && count($array_data['tags']) > 0){
            $tags = $array_data['tags'];
        }else{
            $tags  = [];
        }
        $related = Article::getRelatedArticles($article['id'], $tags);
        
        if (Yii::$app->user->isGuest){
            $profile = null;    
        }else{
            $profile = Yii::$app->user->identity->userProfile;    
        }
        $image = $article['image_base_url'].'/thumbnails/_medium'.$article['image_name'].'_medium.'.$article['image_ext'];
        $this->setRegisterTag($article['title'], $article['description'], $image);
        if ($this->is_mobile){
            return $this->render('mobile/view', [
                'article' => $article,
                'tags' => $tags,
                'array_data' => $array_data,
                'related' => $related,
                'profile' => $profile,
            ]);
        }
        return $this->render('desktop/view', [
            'article' => $article,
            'tags' => $tags,
            'array_data' => $array_data,
            'related' => $related,
            'profile' => $profile,
        ]);
        
    }

    /**
     * Renders the lenta view for the module
     * @return string
    */
    public function actionLenta()
    {
        $list_news = Article::listArticles(1, $this->next, self::ARTICLE_LIMIT);
        $last_item = $this->getLoadMore($list_news);
        
        if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
            $next_page =  Url::to(['/news/article/lenta', 'next' => $last_item['next']]);
        }else{
            $next_page = null;
        }
        $to_day = date('d.m.Y');
        $this->setRegisterTag(Yii::t('frontend', 'meta_lenta_title'), Yii::t('frontend', 'meta_lenta_description'));
        if ($this->is_mobile){
            return $this->render('mobile/lenta', [
                'list_news'=>$list_news,
                'next_page' => $next_page,
            ]);
        }else{
            $main_news = Article::getMainNews(2, 1);
            return $this->render('desktop/lenta', [
                'list_news' => $list_news,
                'next_page' => $next_page,
                'main_news' => $main_news,
                'to_day'=>$to_day
            ]);
        }
    }

    /**
     * Renders the kolumnist view for the module
     * @return string
    */
    public function actionKolumnist()
    {   
        $list = Article::listArticles(20, $this->next, self::ARTICLE_LIMIT);
        $last_item = $this->getLoadMore($list);

        if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
            $next_page =  Url::to(['/news/article/kolumnist', 'next' => $last_item['next']]);
        }else{
            $next_page = null;
        }
        $this->setRegisterTag(Yii::t('frontend', 'meta_kolumnist_title'), Yii::t('frontend', 'meta_kolumnist_description'));
        if ($this->is_mobile){
            return $this->render('mobile/kolumnist', [
                'list_news' => $list,
                'next_page' => $next_page
            ]);
        }else{
            $tag_list = Tag::listTop(10);
            return $this->render('desktop/kolumnist', [
                'list_news' => $list,
                'next_page' => $next_page,
                'tag_list' => $tag_list
            ]);
        }
    }


    /**
     * Renders the view for the module
     * @return string
     */
    public function actionTag($slug)
    {
        $tag = $this->findTag($slug);
        $list = Article::listTagArticles($tag['id'], $this->next, self::ARTICLE_LIMIT);
        $last_item = $this->getLoadMore($list);
        $to_day = date('d.m.Y');
        if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
            $next_page =  Url::to(['/news/article/tag', 'slug'=>$slug,'next' => $last_item['next']]);
        }else{
            $next_page = null;
        }
        $this->setRegisterTag(Yii::t('frontend','article is {tag}',['tag'=>$tag['title']]));
    	if ($this->is_mobile){
        	return $this->render('mobile/tag_view', [
                'articles' => $list,
                'next_page' => $next_page,
                'tag' => $tag
            ]);
        }else{
        	return $this->render('desktop/tag_view', [
                'articles' => $list,
                'next_page' => $next_page,
                'tag' => $tag,
                'to_day'=>$to_day
            ]);
        }
    }

    /**
     * Renders the view for the module
     * @return string
     */
    public function actionAuthor($nickname, $hashtag=null)
    {
        $author = $this->findAuthor($nickname);
        $author_article_count = Article::getCountArticles($author['id']);
        $tags = Article::listkolumnistTags($author['id']);
        if ($hashtag){
            $tag = $this->findTag($hashtag);
            $list = Article::listTagArticles($tag['id'], $this->next, self::ARTICLE_LIMIT, $author['id']);
            $last_item = $this->getLoadMore($list);
            if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
                $next_page =  Url::to(['/news/article/author', 'nickname'=>$nickname,'hashtag'=>$hashtag,'next' => $last_item['next']]);
            }else{
                $next_page = null;
            }

        }else{
            $list = Article::listArticles(1, $this->next, self::ARTICLE_LIMIT, $author['id']);
            $last_item = $this->getLoadMore($list);
            if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
                $next_page =  Url::to(['/news/article/author', 'nickname'=>$nickname,'next' => $last_item['next']]);
            }else{
                $next_page = null;
            }    
        }
        


        if ($this->is_mobile){
            return $this->render('mobile/author', [
                'author' => $author,
                'author_article_count' => $author_article_count,
                'tags' => $tags,
                'articles' => $list,
                'next_page' => $next_page,
            ]);
        }else{
            //$main_news = Article::getMainNews(2, 1);
            return $this->render('desktop/author', [
                'author' => $author,
                'author_article_count' => $author_article_count,
                'tags' => $tags,
                'articles' => $list,
                'next_page' => $next_page

            ]);
        }
    }

    public function actionSearch(){
        $title = \Yii::t('frontend', 'Search Result');
        $model = new Search();
        $model->load(\Yii::$app->request->get());
        $search_text = "";

        if (isset($_GET['q'])) $model->q = $_GET['q'];
        if ($model->q&& $model->q != ""){
            $search_text = strip_tags(stripslashes ( $model->q ));
            $model->q = $search_text;
            $list = $model->searchNews($this->next, self::ARTICLE_LIMIT);
            $last_item = $this->getLoadMore($list);
            if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
                $next_page =  Url::to(['/news/article/search', 'q'=>$search_text, 'next' => $last_item['next']]);
            }else{
                $next_page = null;
            }    
        }else{
            $list = null;
            $next_page = null;
            $search_text = null;
        }
        $to_day = date('d.m.Y');
        if ($this->is_mobile)
            return $this->render('mobile/search_list',[
                'search_text' => $search_text,
                'articles' => $list,
                'next_page' => $next_page,
            ]);
          else{
            return $this->render('desktop/search_list',[
                'search_text' => $search_text,
                'articles' => $list,
                'next_page' => $next_page,
                 'to_day'=>$to_day
            ]);
        }
    }

    public function actionLike($tab_number)
    {  
        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            Article::updateLikesCount($tab_number);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['status'=>'success'];
    }

    protected function findTag($slug)
    {  
        $tag = Tag::getOne($slug);
        if ($tag !== null && $tag) {
            return $tag;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findAuthor($nickname)
    {  
        $author = UserAuthor::nickOneAuthor($nickname);
        if ($author !== null && $author) {
            return $author;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findArticleSlug($slug)
    {  
        $article = Article::getOneArticle($slug);
        if ($article !== null && $article) {
            return $article;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findArticleTabNumber($tab_number)
    {  
        $article = Article::getOneArticlewithTab($tab_number);
        if ($article !== null && $article) {
            return $article;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
