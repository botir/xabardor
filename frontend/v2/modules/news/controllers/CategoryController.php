<?php

namespace frontend\v2\modules\news\controllers;

use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use Yii;
use frontend\v2\components\FrontendController;
use frontend\v2\modules\news\models\Article;
use frontend\v2\modules\news\models\Category;

/**
 * Category controller for the `news` module
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class CategoryController extends FrontendController
{

	/**
     * Renders the view for the module
     * @return string
     */
    public function actionView($slug)
    {

        $category = $this->findCategorySlug($slug);        
                
        $list_news = Article::listCategoryArticles($category['id'], $this->next, self::ARTICLE_LIMIT);
        $last_item = $this->getLoadMore($list_news);
        if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
            $next_page =  Url::to(['/news/category/view', 'slug'=>$slug,'next' => $last_item['next']]);
        }else{
            $next_page = null;
        }
        $to_day = date('d.m.Y');
        $this->setRegisterTag(Yii::t('frontend','article is {tag}',['tag'=>$category['title']]));
        if ($this->is_mobile){
            return $this->render('mobile/tag_view', [
                'articles' => $list_news,
                'next_page' => $next_page,
                'category' => $category,
                'to_day'=>$to_day
            ]);
        }
    	return $this->render('desktop/list', [
                'articles' => $list_news,
                'next_page' => $next_page,
                'category' => $category,
                'to_day'=>$to_day,
        ]);
        
    }

   

    protected function findCategorySlug($slug)
    {
        $tag = Category::getOne($slug);
        if ($tag !== null && $tag) {
            return $tag;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
