<?php

namespace frontend\v2\modules\news\models;

use Yii;
use yii\db\Query;
use common\models\NewsComment;

/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
*/
class Comment extends NewsComment
{


	public static function getListComments($article_id, $after, $limit=10)
	{
	    $query = (new Query())
	      ->select('news_comment.id, news_comment.parent_id, news_comment."created_at"::timestamp as created, news_comment.content, "lastname" ||\' \' || "firstname" as full_name, CASE WHEN avatar_path is not null THEN "avatar_base_url" || "avatar_path" ELSE null end as avatar')
	      ->from('news_comment')
	      ->innerJoin('"user"','news_comment.user_id="user".id')
	      ->innerJoin('user_profile','"user".id="user_profile".user_id')
	      ->where('news_comment.status=2 and news_comment.created_at>:after')
	      ->orderBy(['news_comment.created_at'=>SORT_ASC])
	      ->limit($limit)
	      ->params([':after'=>$after]);
	    return $query->all();
	}

	public static function getHiddenComments($article_id, $after)
	{
	    $query = (new Query())
	      ->select('count(1) as cnt')
	      ->from('news_comment')
	      ->where('news_comment.status=2 and news_comment.created_at>:after')
	      ->params([':after'=>$after]);
	    $result = $query->one();
	    return $result['cnt'];
	}

	public static function getCountComments($article_id)
	{
	    $query = (new Query())
	      ->select('count(1) as cnt')
	      ->from('news_comment')
	      ->where('news_comment.status=2');
	    $result = $query->one();
	    return $result['cnt'];
	}


}
