<?php

namespace frontend\v2\modules\news\models;

use Yii;
use yii\db\Query;

/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
*/
class Tag extends \common\models\NewsTag
{

  public static function listTop($limit=10)
  {
    $query = (new Query())
        ->select('title, slug')
        ->from('news_tag')
        ->where('status=2')
        ->orderBy(['c_weight'=>SORT_DESC])
        ->limit($limit);
    return $query->all();
  }

  public static function getOne($slug)
  {
    $query = (new Query())
        ->select('title, slug, id')
        ->from('news_tag')
        ->where('status=2 and slug=:slug')
        ->orderBy(['c_weight'=>SORT_DESC])
        ->params([':slug' =>$slug]);
    return $query->one();
  }

  public static function listMenu($limit=10)
  {
    $query = (new Query())
        ->select('title, slug')
        ->from('news_tag')
        ->where('status=2 and is_menu is true')
        ->orderBy(['c_weight'=>SORT_DESC])
        ->limit($limit);
    return $query->all();
  }
  public static function listMain($limit=2)
  {
    
    $select_str = '("content"->>\'uz\') AS "tag_title"';
    
    $query = (new Query())
        ->select('id, slug,'.$select_str)
        ->from('news_tag')
        ->where('status=2 and is_main is true')
        ->orderBy(['c_weight'=>SORT_DESC])
        ->limit($limit);
    return $query->all();
  }

}
