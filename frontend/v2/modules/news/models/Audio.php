<?php

namespace frontend\v2\modules\news\models;

use Yii;
use yii\db\Query;
use yii\data\Pagination;
use yii\helpers\Url;

/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
*/
class Audio extends \common\models\NewsAudio
{


	public static function getLastNews($limit)
	{
	    $query = (new Query())
	    	->select('id, lang_id, code, content, audio_path, base_url, photo_path, photo_base_url, duration, published_at, LEFT(photo_path, POSITION(\'.\' IN photo_path)-1) as image_name, right(photo_path, POSITION(\'.\' IN REVERSE(photo_path)) - 1) as image_ext')
	    	->from('news_audio')
	    	->where('status=2 and published_at<now()')
	    	->orderBy(['published_at'=>SORT_DESC])
	      	->limit($limit);
	    $models = $query->all();

	    return $models;
	  }

}