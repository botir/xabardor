<?php

namespace frontend\v2\modules\news\models;

use Yii;
use yii\db\Query;

/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
*/
class Category extends \common\models\NewsCategory
{


  public static function listMenu($limit=10)
  {
    $query = (new Query())
        ->select('title, slug')
        ->from('news_category')
        ->where('status=2 and is_menu is true')
        ->orderBy(['sort'=>SORT_ASC])
        ->limit($limit);
    return $query->all();
  }


  public static function getOne($slug)
  {
      $query = (new Query())
        ->select('title, slug, id')
        ->from('news_category')
        ->where('status=2 and slug=:slug')
        ->params([':slug' =>$slug]);
      return $query->one();
  }

  public static function listMain($limit=2)
  { 

    $query = (new Query())
        ->select('id, slug, title')
        ->from('news_category')
        ->where('status=2 and is_main is true')
        ->limit($limit);
    return $query->all();
  }
 

}
