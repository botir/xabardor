const MONTH_NAMES = [
  'Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'Iyun',
  'Iyul', 'Avgust', 'Sentabr', 'Oktabr', 'Noyabr', 'Dekabr'
];
function humanizeTime(){
  var elements = document.getElementsByClassName('timeago');
  for (var i in elements) {
      var $this = elements[i];
      if (typeof $this === 'object') {
          $this.innerHTML = timeAgo($this.getAttribute('title'));
      }
  }  
}
function renderTimeago(){
  var elements = document.getElementsByClassName('timeago');
    for (var i in elements) {
        var $this = elements[i];
        if (typeof $this === 'object') {
            $this.innerHTML = timeAgo($this.getAttribute('title'));
        }
  }
}
function getFormattedDate(date, prefomattedDate = false, hideYear = false) {
  const day = date.getDate();
  const month = MONTH_NAMES[date.getMonth()];
  const year = date.getFullYear();
  const hours = date.getHours();
  let minutes = date.getMinutes();

  if (minutes < 10) {
    // Adding leading zero to minutes
    minutes = `0${ minutes }`;
  }

  if (prefomattedDate) {
    // Today at 10:20
    // Yesterday at 10:20
    return `${ prefomattedDate }  ${ hours }:${ minutes }`;
  }

  if (hideYear) {
    // 10. January at 10:20
    return `${ day }-${ month } ${ hours }:${ minutes }`;
  }

  // 10. January 2017. at 10:20
  return `${ day }. ${ month } ${ year }.  ${ hours }:${ minutes }`;
}
function timeAgo(dateParam) {
  if (!dateParam) {
    return null;
  }

  const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
  const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
  const today = new Date();
  const yesterday = new Date(today - DAY_IN_MS);
  const seconds = Math.round((today - date) / 1000);
  const minutes = Math.round(seconds / 60);
  const isToday = today.toDateString() === date.toDateString();
  const isYesterday = yesterday.toDateString() === date.toDateString();
  const isThisYear = today.getFullYear() === date.getFullYear();


  if (seconds < 5) {
    return 'hozirgina';
  } else if (seconds < 60) {
    return `${ seconds } sekund oldin`;
  } else if (seconds < 90) {
    return '1 minut oldin';
  } else if (minutes < 60) {
    return `${ minutes } minut olding`;
  } else if (isToday) {
    return getFormattedDate(date, 'Bugun'); // Today at 10:20
  } else if (isYesterday) {
    return getFormattedDate(date, 'Kecha'); // Yesterday at 10:20
  } else if (isThisYear) {
    return getFormattedDate(date, false, true); // 10. January at 10:20
  }

  return getFormattedDate(date); // 10. January 2017. at 10:20
}
$(document).ready(function () {
  renderTimeago();
  if( $('#list-more').length ){
    var list_ias = jQuery.ias(
        {
          'container':'#list-more',
          'item':'.item-al',
          'pagination':'#load-more-btn',
          'next':'#load-more-btn .next',
        });
     list_ias.extension(new IASTriggerExtension({
        text:  $('#load-more-btn .next').text(),
        html: '<button type="button" class="loadMore">{text}</button>'
      }));
    list_ias.extension(new IASSpinnerExtension()); 
    list_ias.extension(new IASPagingExtension());
    list_ias.extension(new IASHistoryExtension());
  }
  if ($('#file-upload').length>0){
    $('#file-upload').yiiUploadKit({"url":"/user/default/avatar-upload?fileparam=_fileinput_w1","multiple":false,"sortable":false,"maxNumberOfFiles":1,"maxFileSize":5000000,"minFileSize":null,"acceptFileTypes":null,"files":null,"previewImage":true,"showPreviewFilename":false,"pathAttribute":"path","baseUrlAttribute":"base_url","pathAttributeName":"path","baseUrlAttributeName":"base_url","messages":{"maxNumberOfFiles":"Достигнуто максимальное кол-во файлов","acceptFileTypes":"Тип файла не разрешен","maxFileSize":"Файл слишком большой","minFileSize":"Файл меньше минимального размера"},"name":"avatar_user"});
    //$('#file-upload').yiiUploadKit({$options});
  }
  if( $('#get-history').length ){

     $(document).on('click', '#get-history', function (event) {
        var content = $('#'+$(this).data('tab')).find('tbody');
        if (content.find('tr').length){
          return 1;
        }
        
        $.ajax({
            url:'/user/payment/history',
            type:'GET',
            error: function (xhr, status, error) {
              return false;
            },
            success: function(data){
              if (data){
                for(var k in data) {
                  content.append('<tr><td>'+data[k].service_title+'</td><td class="danger">-'+data[k].price+'</td><td>'+data[k].payment_date+'</td></tr>');
                }
              }
            }
        });
        return false;
      });
  }
  $(document).on('click', '.remove-bookmark', function (event) {
    $main_c = $(this).closest('.hot-news__item');
    var data = {};
    $.ajax({
        url:$(this).attr('href'),
        type:'DELETE',
        data: data,
        error: function (xhr, status, error) {
          return false;
        },
        success: function(data){
         $main_c.remove();
        }
    });
    return false;
  });
  $(document).on('click', '.like-box__btn', function (event) {
    //alert('OK');
    var param = $('meta[name=csrf-param]').attr("content");
    var token = $('meta[name=csrf-token]').attr("content");
    var data = {};
    data[param] = token;
    data['sm'] = 1;
    $.ajax({
        url:$(this).attr('data-url'),
        type:'POST',
        data: data,
        error: function (xhr, status, error) {
          return false;
        },
        success: function(data){
          $('.like-box__txt').text((parseInt($('.like-box__txt').text())+1).toString());
        }
    });
    return false;
  });
  function get_class_list(elem){
    if(elem.classList){
        return elem.classList;
    }else{
        return $(elem).attr('class').match(/\S+/gi);
    }
  }
  $.fn.ShareLink = function(options){
    var defaults = {
        title: '',
        text: '',
        image: '',
        url: window.location.href,
        class_prefix: 's_'
    };

    var options = $.extend({}, defaults, options);

    var class_prefix_length = options.class_prefix.length;

    var templates = {
        twitter: 'https://twitter.com/intent/tweet?url={url}&text={text}',
        facebook: 'https://www.facebook.com/sharer.php?s=100&p[title]={title}&u={url}&t={title}&p[summary]={text}&p[url]={url}',
        odnoklassniki: 'https://connect.ok.ru/offer?url={url}&title={title}&imageUrl={image}',
        telegram: 'https://telegram.me/share/url?url={url} {title}',
    }

    function link(network){
        var url = templates[network];
        url = url.replace(/{url}/g, encodeURIComponent(options.url));
        url = url.replace(/{title}/g, encodeURIComponent(options.title));
        url = url.replace(/{text}/g, encodeURIComponent(options.text));
        url = url.replace(/{image}/g, encodeURIComponent(options.image));
        return url;
    }

    return this.each(function(i, elem){
        var classlist = get_class_list(elem);
        for(var i = 0; i < classlist.length; i++){
            var cls = classlist[i];
            if(cls.substr(0, class_prefix_length) == options.class_prefix && templates[cls.substr(class_prefix_length)]){
                var final_link = link(cls.substr(class_prefix_length));
                $(elem).attr('href', final_link).click(function(){
                    if($(this).attr('href').indexOf('http://') === -1 && $(this).attr('href').indexOf('https://') === -1){
                        return window.open($(this).attr('href')) && false;
                    }
                    var screen_width = screen.width;
                    var screen_height = screen.height;
                    var popup_width = options.width ? options.width : (screen_width - (screen_width*0.2));
                    var popup_height = options.height ? options.height : (screen_height - (screen_height*0.2));
                    var left = (screen_width/2)-(popup_width/2);
                    var top = (screen_height/2)-(popup_height/2);
                    var parameters = 'toolbar=0,status=0,width=' + popup_width + ',height=' + popup_height + ',top=' + top + ',left=' + left;
                    return window.open($(this).attr('href'), '', parameters) && false;
                });
            }
        }
    });
  }
});