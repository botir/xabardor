<?php

use Sitemaped\Sitemap;

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],
        ['pattern' => 'policy/xabardor-terms-of-service', 'route' => 'policy/terms'],

        // Articles
        ['pattern' => '', 'route' => 'news/default/index'],
        ['pattern' => 'rss', 'route' => 'news/rss/index'],
        ['pattern' => 'articles', 'route' => 'news/article/lenta'],
        ['pattern' => 'search', 'route' => 'news/article/search'],
        ['pattern' => 'kolumnist', 'route' => 'news/article/kolumnist'],
        ['pattern' => 'contact', 'route' => 'contact/index'],
        ['pattern' => '<slug>', 'route' => 'news/article/view'],
        ['pattern' => 'x/<tab_number>', 'route' => 'news/article/sview'],
        ['pattern' => 'articles/<slug>', 'route' => 'news/article/tag'],
        ['pattern' => 'articles/like/<tab_number>', 'route' => 'news/article/like'],
        ['pattern' => 'authors/<nickname>', 'route' => 'news/article/author'],
        ['pattern' => 'subscribe/<name>', 'route' => 'subscribe/view'],
        ['pattern' => 'bookmarks/delete/<tab_number>', 'route' => 'user/bookmark/delete'],
        
        // Sitemap
        ['pattern' => 'sitemap.xml', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML]],
        ['pattern' => 'sitemap.txt', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_TXT]],
        ['pattern' => 'sitemap.xml.gz', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML, 'gzip' => true]],

         ['class' => 'yii\rest\UrlRule', 'controller' => [
            'user/bookmark'
        ]],
    ]
];
