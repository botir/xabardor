<?php
/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */

$cache = [
    'class' => 'yii\caching\FileCache',
    'cachePath' => '@frontend/v1/runtime/cache'
];

if (YII_ENV_DEV) {
    $cache = [
        'class' => 'yii\caching\DummyCache'
    ];
}

return $cache;
