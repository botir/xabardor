<?php

namespace frontend\v1\modules\user\models;

use cheatsheet\Time;
use common\commands\SendEmailCommand;
use common\models\User;
use common\models\UserProfile;
use common\models\UserToken;
use frontend\v1\modules\user\Module;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Signup form
 */
class SignupForm extends Model
{

    /**
     * @var
     */
    public $firstname;
    /**
     * @var
     */
    public $lastname;
    /**
     * @var
     */
    public $username;
    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => '\common\models\User',
                'message' => Yii::t('frontend', 'This email address has already been taken.')
            ],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['firstname', 'filter', 'filter' => 'trim'],
            ['firstname', 'required'],

            ['lastname', 'filter', 'filter' => 'trim'],
            ['lastname', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username'  => Yii::t('frontend', 'Username'),
            'email'     => Yii::t('frontend', 'user_sign_up_email'),
            'password'  => Yii::t('frontend', 'user_sign_up_password'),
            'firstname' => Yii::t('frontend', 'user_sign_up_firstname'),
            'lastname'  => Yii::t('frontend', 'user_sign_up_lastname'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws Exception
     */
    public function signup()
    {
        if ($this->validate()) {
            $shouldBeActivated = $this->shouldBeActivated();
            $user = new User();
            $user->username = $this->email;
            $user->email = $this->email;
            $user->status = User::STATUS_NOT_ACTIVE;
            $user->setPassword($this->password);
            if (!$user->save()) {
                throw new Exception("User couldn't be  saved");
            };
            //$user->afterSignup();
            $user->afterSignup(['firstname'=>$this->firstname, 'lastname'=> $this->lastname]);
            $token = UserToken::create(
                    $user->id,
                    UserToken::TYPE_ACTIVATION,
                    Time::SECONDS_IN_A_DAY
                );
            try {
                
                Yii::$app->commandBus->handle(new SendEmailCommand([
                    'subject' => Yii::t('frontend', 'Activation email'),
                    'view' => 'activation',
                    'to' => $this->email,
                    'params' => [
                        'url' => Url::to(['/user/sign-in/activation', 'token' => $token->token], true)
                    ]
                ]));
                } catch (\Exception $e) {
                    // echo '<pre>';
                    // print_r($e);
                    // echo '</pre>';
                    // exit;
                }
            
            return $user;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function shouldBeActivated()
    {
       
        return true;
        /** @var Module $userModule */
        // $userModule = Yii::$app->getModule('user');
        // if (!$userModule) {
        //     return false;
        // } elseif ($userModule->shouldBeActivated) {
        //     return true;
        // } else {
        //     return false;
        // }
    }
}
