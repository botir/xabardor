<?php

namespace frontend\v1\modules\user\controllers;

use Yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use common\models\PaymentOrder;

class PaymentController extends Controller
{

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['contentNegotiator'] = [
      'class' => 'yii\filters\ContentNegotiator',
      'formats' => [
          'application/json' => Response::FORMAT_JSON,
          'charset' => 'UTF-8',
      ]
    ];
    $behaviors['access'] = [
      'class' => AccessControl::class,
      'rules' => [
                    [
                      'actions' => ['history'],
                      'allow' => true,
                      'roles' => ['@'],
                    ]
                ]
    ];
    return $behaviors;
  }

  public function actionHistory()
  {
    $user = Yii::$app->user->identity;
    $history = PaymentOrder::getUserHistory($user->id);
    return $history;
  }

}
