<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\PasswordResetRequestForm */

$this->title =  Yii::t('frontend', 'user_sign_up_success_title');
$this->bclass = 'forgot-page';
$this->ads = false;
?>

<div class="main-content full-content">
  <div class="container">
    <div class="white-block mt-0 full-height">
      <div class="vertical-center">
        <div class="row justify-content-center">
          <div class="col-md-4 col-sm-6 col-12">
            <h1 class="modal__title text-center"><?=Yii::t('frontend', 'user_sign_up_success_title');?></h1>
            <p class="text-center"><?=Yii::t('frontend', 'user_sign_up_success_description');?></p>
          </div>
        </div>  
      </div>                 
    </div>        
  </div>
</div>