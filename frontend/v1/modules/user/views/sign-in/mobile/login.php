<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
$this->title=\Yii::t('frontend', 'meta_sign_in_title');
?>

<div class="main-content">
	<div class="content white-block">
		<div class="text-center">
          <h1><?=Yii::t('frontend', 'user_sign_in_modal_title')?></h1>
          <p><?=Yii::t('frontend', 'user_sign_in_modal_sub_title')?></p>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'signin-form',
            'enableClientValidation'=> true,
            'enableAjaxValidation'=> false,
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => true,
            'options'=>[
                'class'=>'signform signin-form'
            ]
            ]);
        ?>
	    <div class="signform__form-group">
	      <div class="signform__input-group">
	        <div class="signform__input-group-prepend">
	          <div class="signform__input-group-text">
	            <i class="icon-google-icon"></i>
	          </div>              
	        </div>
	        <a href="<?=Url::to(['/user/sign-in/oauth','authclient'=>'google'])?>" class="signform__button"><?=Yii::t('frontend', 'user_sign_with_google')?></a>
	      </div>
	    </div>
	    <div class="signform__form-group">
	      <div class="signform__input-group">
	        <div class="signform__input-group-prepend">
	          <div class="signform__input-group-text">
	            <i class="icon-facebook-icon"></i>
	          </div>              
	        </div>
	        <a href="<?=Url::to(['/user/sign-in/oauth','authclient'=>'facebook'])?>" class="signform__button"><?=Yii::t('frontend', 'user_sign_with_facebook')?></a>
	      </div>
	    </div>
	    <?php echo $form->field($model, 'identity',[
	        'options' => ['class' => 'signform__form-group'],
	        'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-mail-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
	    ->textInput([
	        'placeholder'=> Yii::t('frontend', 'user_sign_with_email'),
	        'maxlength' => true,
	        'class' => 'signform__form-control',
	    ]); 
	    ?> 

	    <?php
	    echo $form->field($model, 'password',[
	        'options' => ['class' => 'signform__form-group'],
	        'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-lock-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
	    ->passwordInput([
	        'placeholder'=>Yii::t('frontend', 'user_sign_password'),
	        'maxlength' => true,
	        'class' => 'signform__form-control signform__form-control--password',
	        
	    ]); 
	    ?>
	    <?php echo Html::submitButton('<span>'.Yii::t('frontend', 'user_sign_login').'</span>', ['class' => 'btn btn-success signform__btn w-100', 'name' => 'login-button']) ?>
		    
		<?php ActiveForm::end(); ?>
		<div class="modal__help">
		    <p class="modal__helptxt"><?=Yii::t('frontend', 'user_sign_not_account')?> <a id="signup-to" href="<?=Url::to(['/user/sign-in/signup'])?>"><?=Yii::t('frontend', 'user_sign_up_registration')?></a></p>
		    <p class="modal__helptxt"><a href="<?=Url::to(['/user/sign-in/request-password-reset'])?>"><?=Yii::t('frontend', 'user_sign_forgot_password')?></a></p>
		</div>
	</div>
</div>