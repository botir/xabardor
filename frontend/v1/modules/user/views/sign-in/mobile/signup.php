<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
$this->title=\Yii::t('frontend', 'meta_sign_up_title');
?>

<div class="main-content">
	<div class="content white-block">
		<div class="text-center">
          <h1><?=Yii::t('frontend', 'user_sign_up_modal_title')?></h1>
          <p><?=Yii::t('frontend', 'user_sign_up_modal_sub_title')?></p>
        </div>
        <form action="#" class="signform" id="social-container">
        <div class="signform__form-group">
            <div class="signform__input-group">
              <div class="signform__input-group-prepend">
                <div class="signform__input-group-text">
                  <i class="icon-google-icon"></i>
                </div>              
              </div>
              <a href="<?=Url::to(['/user/sign-in/oauth','authclient'=>'google'])?>" class="signform__button"><?=Yii::t('frontend', 'user_sign_with_google')?></a>
            </div>
        </div>
        <div class="signform__form-group">
          <div class="signform__input-group">
            <div class="signform__input-group-prepend">
              <div class="signform__input-group-text">
                <i class="icon-facebook-icon"></i>
              </div>              
            </div>
            <a href="<?=Url::to(['/user/sign-in/oauth','authclient'=>'facebook'])?>" class="signform__button"><?=Yii::t('frontend', 'user_sign_with_facebook')?></a>
          </div>
        </div>
      </form>
    	<div class="signup__text">
 	        <p class="modal__helptxt"><?=Yii::t('frontend', 'user_sign_has_account')?> <a href="<?=Url::to(['/user/sign-in/login'])?>"><?=Yii::t('frontend', 'user_sign_login')?></a></p>
        	<p class="modal__security"><?=Yii::t('frontend', 'user_sign {privacy} link', ['privacy'=>Url::to(['/policy/terms'])])?></p>
    	</div>
	 	<?php $form = ActiveForm::begin([
			'id' => 'signup-form',
			'enableClientValidation'=> true,
			'enableAjaxValidation'=> false,
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
			'options'=>[
			    'class'=>'signform'
			]
			]);
		?>
        
		<?php echo $form->field($model, 'firstname',[
			'options' => ['class' => 'signform__form-group'],
			'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-user-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
		->textInput([
		    'placeholder'=> Yii::t('frontend', 'user_sign_up_firstname'),
		    'maxlength' => true,
		    'class' => 'signform__form-control',
		]); 
		?>

		<?php echo $form->field($model, 'lastname',[
			'options' => ['class' => 'signform__form-group'],
			'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-user-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
		->textInput([
		    'placeholder'=> Yii::t('frontend', 'user_sign_up_lastname'),
		    'maxlength' => true,
		    'class' => 'signform__form-control',
		]); 
		?>

		<?php echo $form->field($model, 'email',[
			'options' => ['class' => 'signform__form-group'],
			'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-mail-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
		->textInput([
		    'placeholder'=> Yii::t('frontend', 'user_sign_up_email'),
		    'maxlength' => true,
		    'class' => 'signform__form-control',
		]); 
		?>

		<?php echo $form->field($model, 'password',[
			'options' => ['class' => 'signform__form-group'],
			'template' => '<div class="signform__input-group"><div class="signform__input-group-prepend"><div class="signform__input-group-text"><i class="icon-lock-icon"></i></div></div>{input}<div class="feedback invalid-feedback">{error}</div></div>'])
		->passwordInput([
		    'placeholder'=> Yii::t('frontend', 'user_sign_up_password'),
		    'maxlength' => true,
		    'class' => 'signform__form-control',
		]); 
		?>
		<?php echo Html::submitButton('<span>'.Yii::t('frontend', 'user_sign_up_submit').'</span>', ['class' => 'btn btn-success signform__btn w-100', 'name' => 'login-button']) ?>  
      <?php ActiveForm::end(); ?>
	</div>
</div>