<?php 
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
$trend_tags = ['url'=> Url::to(['/news/app/index', 'f'=>'tags'])];
$this->title = Yii::t('frontend','user_bookmarks_list');
?>
<div class="main-content">
	<div class="favorite">
		<div class="white-block mt-0">
			<div class="page-title">
				<i class="icon-ribbon-filled-icon page-title__icon"></i>
				<h3 class="page-title__title"><?=$this->title?></h3>
			</div>
			<div class="hot-news">
				<?php if ($list){ foreach($list as $data):?>
					<div class="hot-news__item">
						<div class="hot-news__body">
							<h4 class="hot-news__title news-title"><a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
							</h4>
							<p class="hot-news__content news-content"><?=$data['description']?></p>
							<div class="dashed-divider"></div>
							<ul class="unstyled taglist">
		                    	<li class="taglist__item">
		                        	<a href="<?=Url::to(['/user/bookmark/delete', 'tab_number'=>$data['tab_number']])?>" class="taglist__link remove-bookmark"><span><?=Yii::t('frontend','bookmark_remove')?></span></a>
		                      	</li>
		                    </ul>
						</div>
					</div>
				<?php endforeach;} else{ echo '<div class="white-block"><h2>Xech narsa saqlanmagan</h2></div>';}?>
	        	<?php if ($next_page):?>
		        	<div class="load-panel__button" id="load-more-btn">
		          	<a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
		        	</div>
	      		<?php endif;?>
			</div>
		</div>
	</div>
</div>