<?php

namespace frontend\v1\modules\user\components;

use yii\authclient\OAuth2;
use Yii;

abstract class SocialOAuth2 extends OAuth2
{
	/**
     * Composes default [[returnUrl]] value.
     * @return string return URL.
    */
    protected function defaultReturnUrl()
    {
        $params = Yii::$app->getRequest()->getQueryParams();
        unset($params['code']);
        unset($params['state']);
        unset($params['scope']);
        unset($params['q']);
        $params[0] = Yii::$app->controller->getRoute();
        //$url = Yii::$app->getUrlManager()->createAbsoluteUrl($params);
        //print_r($url); exit;
        //print_r(Yii::$app->controller->getRoute($params[0])); exit;

        return Yii::$app->getUrlManager()->createAbsoluteUrl($params);
    }

}
