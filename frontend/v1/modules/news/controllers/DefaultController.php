<?php

namespace frontend\v1\modules\news\controllers;

use frontend\v1\components\FrontendController;
use frontend\v1\modules\news\models\Article;
/**
 * Default controller for the `news` module
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class DefaultController extends FrontendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
       
    	$main_news = Article::getMainNews(2, 4);
    	$selected = Article::getMainNews(3, 3);
    	
        $list_news = $this->getLastNews($this->next);
        $this->setRegisterTag();
    	if ($this->is_mobile){
	        return $this->render('mobile/index', [
	        	'main_news'=>$main_news, 
	        	'selected'=>$selected,
	        	'list_news'=>$list_news
	        ]);
	    }else{
	    	return $this->render('desktop/index', [
	        	'main_news'=>$main_news, 
	        	'selected'=>$selected,
	        	'list_news'=>$list_news
        	]);
	    }
    }
}
