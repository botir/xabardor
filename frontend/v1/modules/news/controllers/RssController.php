<?php

namespace frontend\v1\modules\news\controllers;

use Yii;
use yii\web\Response;
use frontend\v1\components\FrontendController;
use frontend\v1\modules\news\models\Article;
/**
 * Rss controller for the `news` module
 * @author Botir Ziyatov <botirziyatov@gmail.com>
*/
class RssController extends FrontendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {	
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xml');
        $list_news = $this->getLastNews($this->next);
        $view = $this->renderPartial('index', ['list_news' => $list_news]);
        \Yii::$app->response->data  =  $view;
    }
}
