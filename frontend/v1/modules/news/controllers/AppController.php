<?php

namespace frontend\v1\modules\news\controllers;

use Yii;
use yii\rest\Controller;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Response;
use frontend\v1\modules\news\models\Article;
use frontend\v1\modules\news\models\Tag;

/**
 * App Controller 
 * @author Botir Ziyatov <botirziyatov@gmail.com>
*/
class AppController extends Controller
{

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['contentNegotiator'] = [
      'class' => 'yii\filters\ContentNegotiator',
      'formats' => [
          'application/json' => Response::FORMAT_JSON,
          'charset' => 'UTF-8',
      ]
    ];
    return $behaviors;
  }

  public function actionIndex()
  {
    if (isset($_GET['f']))
      $type = $_GET['f'];
    else
      $type = 'last';

    switch ($type) {
      case 'top':
        $news = Article::listTopArticles(4);
        $title = 'TOP NEWS';
        $top  = ['title'=>Yii::t('frontend', 'most_viewed_articles'), 'list'=>$this->getFormatNews($news)];
        $news_c = Article::listTopComments(4);
        $comment  = ['title'=>Yii::t('frontend', 'most_commented_articles'), 'list'=>$this->getFormatNews($news_c)];
        $list  = ['top'=>$top, 'comment'=>$comment];
        break;
      case 'comment':
        $list = [];
        $title = 'comments';
        break;
      case 'selected':
        $news = Article::listArticles(10, '2039-12-31', 4);
        $list  = $this->getFormatNews($news);
        $title = 'Muxarrir tanlovi';
        break;
      case 'tags':
        $tags = Tag::listTop(10);
        $list  = $this->getFormatTags($tags);
        $title = Yii::t('frontend', 'trend_tags');
        break;
      default:
        $list = [];
        $title = 'UNIKNOWN';
    }
    return ['title'=>$title, 'list'=>$list];
  }

  protected function getFormatNews($news){
      $result = [];
      foreach ($news as $item) {
        //print_r($item); exit;
        $array_data = Json::decode($item['more_option']);
        
        if (isset($array_data['tags']) && count($array_data['tags']) > 0){
            $tags_items = $array_data['tags'];
        }else{
            $tags_items = null;
        }
        $tags = [];
        if ($tags_items){
          foreach ($tags_items as $item_tag) {
            $tags [] = [
              'title'=>$item_tag['title'],
              'url'=>Url::to(['/news/article/tag', 'slug' => $item_tag['slug']])
            ]; 
          }
        }

        $result[]=[
          'title' => $item['title'],
          'description'=>$item['description'],
          'url'=> Url::to(['/news/article/view', 'slug' => $item['slug']]),
          'views_count'=> $item['views_count'],
          'comments_count'=> $item['comment_count'],
          'image'=>$item['image_base_url'].'/thumbnails/_medium'.$item['image_name'].'_medium.'.$item['image_ext'],
          'published'=>$item['published_at'],
          'tags'=>$tags,
          'show_desc'=>$item['show_desc'],
          'list_image'=>$item['list_image'],
          'content_type'=>$item['content_type'],
          'is_ads'=>$item['is_ads'],
          'is_selected'=>$item['is_selected']
          ];
        }
      return $result;
  }

  protected function getFormatTags($tags){
    $result = [];
    foreach ($tags as $item) {
      $result[]=[
        'title' => $item['title'],
        'url'=> Url::to(['/news/article/tag', 'slug' => $item['slug']]),
      ];
      }
    return $result;
  }



}
