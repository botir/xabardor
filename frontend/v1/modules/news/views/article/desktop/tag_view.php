<?php
use yii\helpers\Url;
$this->title = Yii::t('frontend','article is {tag}',['tag'=>$tag['title']]);
$top_tags = ['url'=> Url::to(['/news/app/index', 'f'=>'tags']), 'title'=>true];
?>
<div class="main-content">
	<div class="container">
		<div class="white-block mt-0">
			<div class="page-title">
            	<i class="icon-ribbon-filled-icon page-title__icon"></i>
            	<h3 class="page-title__title"><?=$tag['title']?></h3>
          </div>
		</div>
		<div class="main-section" id="category-lenta">
			<?php if ($articles):?>
				<?php foreach($articles as $data): $tags = $this->getTags($data['more_option']);?>
					<div class="single-news">
			            <div class="white-block">
			              <ul class="unstyled infolist">
			                <li class="infolist__item">
			                  <i class="icon-clock-icon"></i>
			                  <span class="no-timeago"><?=$this->timeAgo($data['published_at'])?></span>
			                </li>
			                <li class="infolist__item">
			                  <i class="icon-eye"></i>
			                  <?=$data['views_count']?>
			                </li>
			              </ul>
			              <div class="news-list single">
			                <div class="news-list__item">
			                  <div class="news-list__content">
			                    <h6 class="news-list__title news-title">
			                      <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
			                    </h6>
			                    <p class="news-list__text news-content"><?=$data['description']?></p>
			                    <div class="dashed-divider"></div>
			                    <?php 
				                    if ($tags){ echo '<ul class="unstyled taglist">';
				                        foreach ($tags as $tag) {
				                            echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
				                        }
				                        echo '</ul>';
				                    }
						        ?>
			                  </div>
			                  <a class="news-list__link" href="<?=$this->getArticleUrl($data['slug'])?>">
			                    <img src="<?=$this->getListImage($data)?>" />
			                  </a>
			                </div>
			              </div>
			            </div>
			          </div>
				<?php endforeach;?>
				<?php if ($next_page):?>
			        <div class="load-panel__button" id="load-more-btn">
			          <a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
			        </div>
			      <?php endif;?>
			<?php endif;?>
		</div>
		<div class="right-sidebar mt-12">
          	<div id="anchors-b"></div>
			<div id="sticky-b">
				<div class="white-block mt-0" id="trend_tags" data='<?=json_encode($top_tags)?>'></div>
          		<div class="ads-block">
      		 		<?=\frontend\v1\widgets\AdDesktop::widget(['place'=>2, 'link_class'=>'right-ads'])?>
    			</div>
    		</div>


		</div>

	</div>
</div>