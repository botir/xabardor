<?php
use yii\helpers\Url;
$top_tags = ['url'=> Url::to(['/news/app/index', 'f'=>'tags']), 'title'=>true];
$this->title = Yii::t('frontend', 'meta_kolumnist_title');
?>
<div class="main-content">
	<div class="container">
		<div class="content clearfix">
			<div class="main-section">

				<div class="white-block mt-0">
					<div class="unstyled news-list lenta" id="lenta-news">
						<?php if ($list_news):?>
							<?php foreach($list_news as $data): $tags = $this->getTags($data['more_option']);?>
								<div class="news-list__item">

				                  <a class="news-list__link" href="<?=$this->getArticleUrl($data['slug'])?>">
				                    <img src="<?=$this->getListImage($data)?>"/>
				                  </a>
				                  <div class="news-list__content">
				                    <h6 class="news-list__title news-title">
				                      <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
				                    </h6>
				                    <div class="dashed-divider"></div>
				                    <?php 
					                    if ($tags){ echo '<ul class="unstyled taglist">';
					                        foreach ($tags as $tag) {
					                            echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
					                        }
					                        echo '</ul>';
					                    }
					                ?>
					                <div class="mt-12">
					                <ul class="unstyled infolist">
						                <li class="infolist__item">
						                  <i class="icon-clock-icon"></i>
						                  <span class="no-timeago"><?=$this->timeAgo($data['published_at'])?></span>
						                </li>
						                <li class="infolist__item">
						                  <i class="icon-eye"></i>
						                  <?=$data['views_count']?>
						                </li>
						              </ul>
				                  	</div>
				                  </div>
				                </div>
							<?php endforeach;?>								
						<?php endif;?>
					</div>
				</div>
				<?php if ($next_page):?>
			        <div class="load-panel__button" id="load-more-btn">
			          <a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
			        </div>
			      <?php endif;?>
			</div>
			<div class="right-sidebar">
				<div id="anchors-b"></div>
				<div id="sticky-b">
				<div class="white-block mt-0" id="trend_tags" data='<?=json_encode($top_tags)?>'></div>
				
				<div class="ads-block">
              		 <?=\frontend\v1\widgets\AdDesktop::widget(['place'=>2, 'link_class'=>'right-ads'])?>
            	</div>
            	</div>
			</div>
		</div>
	</div>
</div>
