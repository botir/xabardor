<?php
$this->title = Yii::t('frontend','article is {tag}',['tag'=>$tag['title']]);
?>
<div class="main-content">
	<div class="white-block mt-0">
		<div class="page-title">
          <i class="icon-ribbon-filled-icon page-title__icon"></i>
          <h3 class="page-title__title"><?=$tag['title']?></h3>
        </div>
        <div class="hot-news" id="list-more">
        	<?php if ($articles):?>
        		<?php foreach($articles as $data): $tags = $this->getTags($data['more_option']);?>
        				<div class="hot-news__item item-al">
        					<?php if ($data['list_image']):?>
					            <a href="<?=$this->getArticleUrl($data['slug'])?>" class="hot-news__link">
					              <img class="hot-news__img post-img" src="<?=$this->getListImage($data)?>"/>
					            </a>
				            <?php endif;?>
				            <div class="hot-news__body">
				              	<h6 class="hot-news__title news-title">
				                	<a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
				              	</h6>
				              	<?php if ($data['show_desc']):?>
				              		<p class="hot-news__content news-content"><?=$data['description']?></p>
				            	<?php endif;?>  	
				              <div class="dashed-divider"></div>
				              <?php 
				                if ($tags){ echo '<ul class="unstyled taglist">';
				                  foreach ($tags as $tag) {
				                      echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" title="" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
				                  }
				                  echo '</ul>';
				                }
				            	?>
				            </div>
				          </div>
        		<?php endforeach;?>
        	<?php endif;?>
        </div>
	</div>
	<?php if ($next_page):?>
	    <div class="load-panel__button" id="load-more-btn">
	      <a href="<?=$next_page?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
	    </div>
  	<?php endif;?>
</div>