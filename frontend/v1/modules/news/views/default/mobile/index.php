<?php
use yii\helpers\Url;
$this->title = Yii::t('frontend','default_title');
?>

<?php if ($main_news):?>
	<div class="top-news">
		<div class="white-block">
			<?php foreach($main_news as $data): $tags = $this->getTags($data['more_option']);
            	if ( $data === reset( $main_news )):?>
            		<div class="bignews">
			            <a href="<?=$this->getArticleUrl($data['slug'])?>" class="bignews__link">
			              <img class="bignews__img post-img" src="<?=$this->getListImage($data)?>" />
			            </a>
			            <div class="bignews__body news-body">
			              <h5 class="news-body__title news-title">
			                <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
			              </h5>
			              <p class="news-body__content news-content">
			                <?=$data['description']?>
			              </p>
			              <div class="dashed-divider"></div>
			              <?php 
                              if ($tags){ echo '<ul class="unstyled taglist">';
                                foreach ($tags as $tag) {
                                  echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" title="" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
                                }
                                echo '</ul>';
                              }
                            ?>
			            </div>
			        </div>
			       	<div class="clearfix"></div>
			        <ul class="unstyled news-list">
                <?php else:?>
                	<li class="news-list__item">
			            <div class="news-list__content">
			                <h6 class="news-list__title news-title">
			                  <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
			                </h6>
			                <div class="dashed-divider"></div>
			                 <?php 
                              if ($tags){ echo '<ul class="unstyled taglist">';
                                foreach ($tags as $tag) {
                                  echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" title="" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
                                }
                                echo '</ul>';
                              }
                            ?>
			            </div>
			        </li>
                <?php endif;?>
                <?php endforeach;?>
                </ul>
		</div>
	</div>
<?php endif; ?>
<?php if ($selected):?>
	<div class="current-news">
		<div class="white-block">
			<div class="page-title">
            	<i class="icon-star-filled-icon page-title__icon yellow"></i>
            	<h3 class="page-title__title">Dolzarb mavzu</h3>
          	</div>
          	<div class="hot-news">
          		<?php foreach($selected as $data): $tags = $this->getTags($data['more_option']);?>
          			<div class="hot-news__item">
		              <a href="<?=$this->getArticleUrl($data['slug'])?>" class="hot-news__link">
		                <img class="hot-news__img post-img" src="<?=$this->getListImage($data)?>" />
		              </a>
		              <div class="hot-news__body">
		                <h6 class="hot-news__title news-title">
		                  <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
		                </h6>
		                <p class="hot-news__content news-content"><?=$data['description']?> </p>
		                <div class="dashed-divider"></div>
		                <?php 
                            if ($tags){ echo '<ul class="unstyled taglist">';
                                foreach ($tags as $tag) {
                                  echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" title="" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
                                }
                                echo '</ul>';
                              }
                            ?>
		              </div>                
		            </div>

          		<?php endforeach;?>
          	</div>
		</div>
	</div>
<?php endif; ?>
<div class="ads-block">
    <?=\frontend\v1\widgets\AdMobile::widget(['place'=>4, 'link_class'=>'center-ads'])?>
</div>
<div class="news-container" id="list-more">
	<?php foreach($list_news['articles'] as $data): $tags = $this->getTags($data['more_option']);?>
		<?php if ($data['content_type'] == 1 and $data['list_image']):?>
			<div class="single-news item-al">
	            <div class="white-block">
	              <ul class="unstyled infolist">
	                <li class="infolist__item">
	                  <i class="icon-clock-icon"></i>
	                  <span class="no-timeago"> <?=$this->timeAgo($data['published_at'])?></span>
	                </li>
	                
	                <li class="infolist__item">
	                  <i class="icon-eye"></i>
	                  <?=$data['views_count']?>
	                </li>
	              </ul>
	              <div class="news-list single">
	                <div class="news-list__item">
	                  <a class="news-list__link" href="<?=$this->getArticleUrl($data['slug'])?>">
	                    <img class="post-img" src="<?=$this->getListImage($data)?>"/>
	                  </a>
	                  <div class="news-list__content">
	                    <h6 class="news-list__title news-title">
	                      <a href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
	                    </h6>
	                    <?php if ($data['show_desc']):?>
			                <p class="news-list__text news-content"><?=$data['description']?></p>
			              <?php endif;?>
	                    <div class="dashed-divider"></div>
	                   	<?php 
			                if ($tags){ echo '<ul class="unstyled taglist">';
			                  foreach ($tags as $tag) {
			                      echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" title="" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
			                  }
			                  echo '</ul>';
			                }
			            ?>
	                  </div>
	                </div>
	              </div>
	            </div>
          	</div>
        <?php else:?>
        	<div class="main-news item-al">
        		<div class="white-block">
                <ul class="unstyled infolist">
                  <li class="infolist__item">
                    <i class="icon-clock-icon"></i>
                    <span class="no-timeago"><?=$this->timeAgo($data['published_at'])?></span>
                  </li>
                  <li class="infolist__item">
                    <i class="icon-bubble-icon"></i>
                    <?=$data['comment_count']?>
                  </li>
                  <li class="infolist__item">
                    <i class="icon-eye"></i>
                    <?=$data['views_count']?>
                  </li>
                </ul>
                <h6 class="main-news__title news-title">
                  <a class="main-news__link" href="<?=$this->getArticleUrl($data['slug'])?>"><?=$data['title']?></a>
                </h6>
                <?php if ($data['show_desc']):?>
                <p class="main-news__content news-content"><?=$data['description']?></p>
                <?php endif;?>
                <div class="dashed-divider"></div>
                <?php if ($data['list_image']):?>
	                <a href="<?=$this->getArticleUrl($data['slug'])?>" class="main-news__img">
	                  <img class="post-img" src="<?=$this->getListImage($data)?>"/>
	                </a>
                <?php endif;?>
                <div class="dashed-divider"></div>
                <?php 
	                if ($tags){ echo '<ul class="unstyled taglist">';
	                  foreach ($tags as $tag) {
	                      echo '<li class="taglist__item"><a href="'.$this->getTagUrl($tag['slug']).'" title="" class="taglist__link">&#35 <span>'.$tag['title'].'</span></a></li>';
	                  }
	                  echo '</ul>';
	                }
	            ?>
              </div>
        	</div>
		<?php endif;?>
	<?php endforeach;?>		
	 <?php if ($list_news['next_page']):?>
        <div class="load-panel__button" id="load-more-btn">
          <a href="<?=$list_news['next_page']?>" class="next loadMore"><?=Yii::t('frontend','load_more_btn')?></a>
        </div>
      <?php endif;?>
</div>