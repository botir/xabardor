<?php
use yii\helpers\Url;
use \common\components\NewsHelper;
?>
<?= '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <language>uz</language>
        <title>xabardor.uz</title>
        <description><?=Yii::t('frontend','default_description')?></description>
        <link><?= Yii::$app->urlManager->createAbsoluteUrl(['/news/default/index'])?></link>
        <atom:link rel="self" type="application/rss+xml" href="<?=Url::to(['/news/rss/index'])?>"/>
        <?php foreach ($list_news['articles'] as $data):?>
        <item>
            <guid><?=$this->getArticleUrl($data['slug'], true)?></guid>
            <title><?=$data['title']?></title>
            <link><?=$this->getArticleUrl($data['slug'], true)?></link>
            <description>
                <![CDATA[<?=$data['description']?>]]>
            </description>
            <pubDate><?=date("D, d M Y G:i:s",strtotime($data['published_at']))?></pubDate>
            <category><?= $this->getFirstTagName($data['more_option'])?></category>
        </item>
        <?php endforeach; ?>
    </channel>
</rss>
