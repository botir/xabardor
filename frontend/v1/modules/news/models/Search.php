<?php

namespace frontend\v1\modules\news\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;

/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
*/
class Search extends Model
{
    public $q = '';

    public function attributeLabels()
    {
        return [
            'q' => \Yii::t('app', 'Do Search') . '...'
        ];
    }

    public function rules()
    {
        return [
            ['q', 'string', 'min' => 3, 'skipOnEmpty' => false],
        ];
    }

    public function searchNews($next, $limit)
    {
        $arrs = explode(" ",mb_strtolower($this->q, 'UTF-8'));
        $query_text = "news_article.status = 2 and published_at<'".$next."'";
        $iq = 0;

        foreach ($arrs as $value) {
            $one_str = str_replace("'", "%''%", $value);

            if ($one_str && $one_str!=''){
                if ($iq > 0 ){
                    $query_text.= " & ".$one_str.":*";
                }
                else{
                    $query_text .= " and (document_vector @@ tsquery('".$one_str.":*";
                }
                $iq++;
            }
        }
        $query_text.="'))";

        $query = (new Query())
            ->select('title, description, slug, "published_at"::timestamp, more_option, ("more_option"->>\'list_image\')::boolean AS "list_image", ("more_option"->>\'show_desc\')::boolean AS "show_desc", content_type, is_ads, is_selected, views_count, image_base_url, image_path,  LEFT(image_path, POSITION(\'.\' IN image_path)-1) as image_name, right(image_path, POSITION(\'.\' IN REVERSE(image_path)) - 1) as image_ext')
            ->from('news_article')
            ->innerJoin('news_content','news_article.id=news_content.article_id')
            ->where($query_text)
            ->orderBy(['published_at'=>SORT_DESC])
            ->limit($limit);

       return $query->all();
    }

}
