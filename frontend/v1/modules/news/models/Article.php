<?php

namespace frontend\v1\modules\news\models;

use Yii;
use yii\db\Query;
use yii\data\Pagination;
use yii\helpers\Url;

/**
 * @author Botir Ziyatov <botirziyatov@gmail.com>
*/
class Article extends \common\models\NewsArticle
{
  private static $select_str = 'title, description, slug, "published_at"::timestamp, more_option, ("more_option"->>\'list_image\')::boolean AS "list_image", ("more_option"->>\'show_desc\')::boolean AS "show_desc", content_type, is_ads, is_selected, views_count, comment_count, image_base_url, image_path,  LEFT(image_path, POSITION(\'.\' IN image_path)-1) as image_name, right(image_path, POSITION(\'.\' IN REVERSE(image_path)) - 1) as image_ext';

  public static function getMainNews($type, $limit=4)
  {
    $query = (new Query())
      ->select(self::$select_str)
      ->from('news_article')
      ->orderBy(['published_at'=>SORT_DESC])
      ->limit($limit);
    
    if ($type == 2){
      $query->where('status=2 and content_type=2 and published_at<(now()+ interval \'5 hours\')');
    }elseif($type == 3){
      $query->where('status=2 and content_type=3 and published_at<(now()+ interval \'5 hours\')');
    }

    $models = $query->all();

    return $models;
  }

  public static function listArticles($type=1,$next=1,$limit=12, $author_id=null)
  {
    $query = (new Query())
      ->select(self::$select_str)
      ->from('news_article')
      ->orderBy(['published_at'=>SORT_DESC])
      ->limit($limit);
    if ($type == 10){
      $query->where('status=2 and is_selected is true and published_at<now()');
    }if ($type == 20){
      $query->where('status=2 and is_column is true and published_at<now()');
    }elseif($author_id){
      $query->where('status=2 and published_at<:next and author_id = :author_id');
      $query->params([':next' =>$next, ':author_id'=>$author_id]);
    }else{
      $query->where('status=2 and published_at<:next');
      $query->params([':next' =>$next]);
    }
    return $query->all();
  }

  public static function listTagArticles($tag_id, $next=1, $limit=12, $author_id=null)
  {
    if ($author_id){
      $where = ' and news_article.author_id='.$author_id;
    }else{
      $where = '';
    }
   $sql = 'select '.self::$select_str.' from news_article
    JOIN LATERAL jsonb_array_elements(more_option->\'tags\') as obj ON (obj->>\'id\')::int4=:tag_id
      where status = 2 and published_at<:next '.$where.'
      order by published_at desc
      limit :limit
      ';
    
    $result =  \Yii::$app->db->createCommand($sql, [':tag_id'=>$tag_id, ':next' =>$next, ':limit'=>$limit])->queryAll();
    return $result;
  }



  public static function listTopArticles($limit=4)
  {
    $query = (new Query())
        ->select(self::$select_str)
        ->from('news_article')
        ->where('status=2 and published_at<now() and DATE(published_at AT TIME ZONE \'Asia/Tashkent\')>= ( current_date - INTERVAL \'5\' DAY )')
        ->orderBy(['views_count'=>SORT_DESC, 'published_at'=>SORT_DESC])
        ->limit($limit);
    return $query->all();
  }

  public static function listTopComments($limit=4)
  {
    $query = (new Query())
        ->select(self::$select_str)
        ->from('news_article')
        ->where('status=2 and comment_count > 0')
        ->orderBy(['comment_count'=>SORT_DESC, 'published_at'=>SORT_DESC])
        ->limit($limit);
    return $query->all();
  }


  public static function listkolumnistTags($author_id, $limit=5)
  {
    $sql = 'select jsonb_array_elements_text(more_option->\'tags\')::jsonb->>\'title\' as title,jsonb_array_elements_text(more_option->\'tags\')::jsonb->>\'slug\' as slug
      from news_article
      where author_id = :author_id and status = 2 and published_at<now()
      group by jsonb_array_elements_text(more_option->\'tags\') 
      ';
    $result =  \Yii::$app->db->createCommand($sql, [':author_id'=>$author_id])->queryAll();
    return $result;
  }

  public static function getCountArticles($author_id)
  {
    $sql = 'select count(1) as cnt
      from news_article
      where author_id = :author_id and status = 2 and published_at<now()
      ';
    $result =  \Yii::$app->db->createCommand($sql, [':author_id'=>$author_id])->queryOne();

    return $result['cnt'];
  }

  public static function getOneArticle($slug)
  {
    $query = (new Query())
      ->select('news_article."id","title", news_article."description", "content",open_content, "slug", "tab_number", "published_at"::timestamp, "content_type", "more_option", 
"is_ads", "is_selected", "is_premium", "author_id", "views_count", "comment_count", image_base_url, image_path, LEFT(image_path, POSITION(\'.\' IN image_path)-1) as image_name, right(image_path, POSITION(\'.\' IN REVERSE(image_path)) - 1) as image_ext, "lastname" ||\' \' || "firstname" as full_name, nickname, user_author.description as user_status,  avatar_path, avatar_base_url')
      ->from('news_article')
      ->innerJoin('user_author','news_article.author_id=user_author.id')
      ->where('news_article.status=2 and slug=:slug')
      ->params([':slug'=>$slug])
      ->limit(1);

    return $query->one();

  }

  public static function getOneArticlewithTab($tab_number)
  {
    $query = (new Query())
      ->select('news_article."id","title", news_article."description", "content",open_content, "slug", "tab_number", "published_at"::timestamp, "content_type", "more_option", 
"is_ads", "is_selected", "is_premium", "author_id", "views_count", "comment_count", image_base_url, image_path, LEFT(image_path, POSITION(\'.\' IN image_path)-1) as image_name, right(image_path, POSITION(\'.\' IN REVERSE(image_path)) - 1) as image_ext, "lastname" ||\' \' || "firstname" as full_name, nickname, user_author.description as user_status,  avatar_path, avatar_base_url')
      ->from('news_article')
      ->innerJoin('user_author','news_article.author_id=user_author.id')
      ->where('news_article.status=2 and news_article.tab_number=:tab_number')
      ->params([':tab_number'=>$tab_number])
      ->limit(1);

    return $query->one();

  }

  public static function getRelatedArticles($article_id, $tags, $limit=2)
  {
    $tags_str = '';
    foreach ($tags as $key => $data) {
      if ($key == 0){
        $tags_str = '(obj->>\'id\')::int4='.$data['id'];
      }else{
        $tags_str .= ' or (obj->>\'id\')::int4='.$data['id'];
      }
    }
   $sql = 'select '.self::$select_str.' from (select id from news_article
    JOIN LATERAL jsonb_array_elements(more_option->\'tags\') as obj ON '. $tags_str.'
      where status = 2 and published_at<now() and id!=:article_id group by id order by published_at desc limit 2) as tg
      inner join news_article on tg.id= news_article.id
      order by published_at desc
      ';
    $result =  \Yii::$app->db->createCommand($sql, [':article_id'=>$article_id])->queryAll();
    return $result;
  }

}
