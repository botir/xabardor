<?php
namespace frontend\v1\components;

use Yii;
use yii\helpers\Url;
use yii\helpers\Json;

class View extends \yii\web\View
{
    private $_ads = false;
    private $_bclass = 'main-c';

    public function init()
    {
        parent::init();
        $this->ads = true;
    }

    public function getAds()
    {
        return $this->_ads;
    }

    public function setAds($value)
    {
        $this->_ads = $value;
    }

    public function getBclass()
    {
        return $this->_bclass;
    }

    public function setBclass($value)
    {
        $this->_bclass = $value;
    }

    public function getTags($json_data){
        $array_data = Json::decode($json_data);
        
        if (isset($array_data['tags']) && count($array_data['tags']) > 0){
            return $array_data['tags'];
        }else{
            return false;
        }
    }

    public function getFirstTagName($json_data){
        $array_data = Json::decode($json_data);
        
        if (isset($array_data['tags']) && count($array_data['tags']) > 0){
            return $array_data['tags'][0]['title'];
        }else{
            return false;
        }
    }


    public function getArticleUrl($slug, $abs=false){
    	return Url::to(['/news/article/view', 'slug' => $slug], $abs);
    }

    public function getShortUrl($code){
        return Url::toRoute(['/news/article/sview', 'tab_number' => $code], true);
    }

    public function getTagUrl($slug){
    	return Url::to(['/news/article/tag', 'slug' => $slug]);
    }

    public function getAuthorUrl($nickname, $slug=null){
        return Url::to(['/news/article/author', 'nickname' => $nickname, 'hashtag'=>$slug]);
    }

    public function getPublishedDay($dt){ 
        return date('H:i / d.m.Y', strtotime($dt));
    }

    public function getListImage($data){
        $mode = '_medium';
        return $data['image_base_url'].'/thumbnails/'.$mode.$data['image_name'].$mode.'.'.$data['image_ext'];
    }
    
    public function getOptionArticle($json_data){
        $array_data = Json::decode($json_data);
        return $array_data;
    }

    

    public function timeAgo($timestamp){
        //date_default_timezone_set("Asia/Kolkata");         
        $time_ago        = strtotime($timestamp);
        $current_time    = time();
        $time_difference = $current_time - $time_ago;
        $seconds         = $time_difference;
        $minutes = round($seconds / 60); // value 60 is seconds  
        $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
        $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
        $weeks   = round($seconds / 604800); // 7*24*60*60;  
        $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
        $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
                
        if ($seconds <= 60){
            return "hozirgina";
        } else if ($minutes <= 60){
            if ($minutes == 1){
                return "1 daqiqa oldin";
            } else {
                return "$minutes daqiqa oldin";
            }
        } else if ($hours <= 24){
            if ($hours == 1){
                return "1 soat oldin";
            } else {
                return "$hours soat oldin";
            }
        } else if ($days == 1){
                return "kecha ".date('H:i', $time_ago);
        } else if ($months <= 12){
            return date("d/m. H:i",$time_ago);
        } else {
            return date("d/m/Y. H:i",$time_ago);
        }
    }

    public function getMonth($num){
        $data = $this->getMonthList();
        return isset($data[intval($num)]) ? $data[intval($num)] : '';
    }

     public function getMonthList(){

        return  [
            1   => Yii::t('frontend', 'January'),
            2   => Yii::t('frontend', 'February'),
            3   => Yii::t('frontend', 'March'),
            4   => Yii::t('frontend', 'April'),
            5   => Yii::t('frontend', 'May'),
            6   => Yii::t('frontend', 'June'),
            7   => Yii::t('frontend', 'July'),
            8   => Yii::t('frontend', 'August'),
            9   => Yii::t('frontend', 'September'),
            10  => Yii::t('frontend', 'October'),
            11  => Yii::t('frontend', 'November'),
            12  => Yii::t('frontend', 'December')
        ];
    }

    public function timeAgo2($timestamp){
        //date_default_timezone_set("Asia/Kolkata");         
        $time_ago        = strtotime($timestamp);
        $current_time    = time();
        $time_difference = $current_time - $time_ago;
        $seconds         = $time_difference;
        $minutes = round($seconds / 60); // value 60 is seconds  
        $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
        $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
        $weeks   = round($seconds / 604800); // 7*24*60*60;  
        $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
        $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60
                
        if ($seconds <= 60){
            return "Just Now";
        } else if ($minutes <= 60){
            if ($minutes == 1){
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        } else if ($hours <= 24){
            if ($hours == 1){
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        } else if ($days <= 7){
            if ($days == 1){
                return "yesterday";
            } else {
            return "$days days ago";
            }
        } else if ($weeks <= 4.3){
            if ($weeks == 1){
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        } else if ($months <= 12){
            if ($months == 1){
                return "a month ago";
            } else {
                return "$months months ago";
            }
        } else {
            if ($years == 1){
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

        
}
