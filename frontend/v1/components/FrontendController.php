<?php
namespace frontend\v1\components;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;

use frontend\v1\modules\news\models\Article;

class FrontendController extends Controller
{
	const ARTICLE_LIMIT = 15;
	const CATEGORY_LIMIT = 15;
	public $is_mobile;
	public $next;


	public function beforeAction($action) {
		if (isset($_GET['next'])){
        	try {
        			$this->next = date('Y-m-d H:i:s', $_GET['next']);
        		} 
        	catch (\Exception $e) {
        		$this->next = '2030-12-31';
        	}
        }
      	else{
      		$this->next = '2030-12-31';
      	}

		if (Yii::$app->devicedetect->isMobile()){
			$this->is_mobile = true;
			$this->layout = "//main_mobile";
		}else{
			$this->is_mobile = false;
			$this->layout = "//main";
		}

		return parent::beforeAction($action);
	}

	public function getLastNews($next)
	{
		$list = Article::listArticles(1, $this->next, self::ARTICLE_LIMIT);
		$last_item = $this->getLoadMore($list);
		
		if ($last_item['count'] == self::ARTICLE_LIMIT && $last_item['next']){
			$next_page =  Url::to(['/news/default/index', 'next' => $last_item['next']]);
		}else{
			$next_page = null;
		}

		return ['articles' => $list, 'next_page' => $next_page];
	}

 	protected function getLoadMore($obj)
	{
		$count = count($obj);
		if ($count > 0){
			$dt1 = \DateTime::createFromFormat('Y-m-d H:i:s', $obj[$count-1]['published_at']);
			$time = strtotime($dt1->format('Y-m-d H:i:s'));
			return ['next'=>$time, 'pub'=>$obj[$count-1]['published_at'], 'count'=>$count];
		}
		else
			return ['next'=>null,'pub'=>null,'count'=>0];
 	}

 	public function setRegisterTag($title=null, $description=null, $image=null){
		
 		if (!$title){
 			$title = Yii::t('frontend','default_title');
 			$description = Yii::t('frontend','default_description');
 		}

		//Yii::$app->view->registerMetaTag(['name' => 'title', 'content' =>$title], 'title');
		Yii::$app->view->registerMetaTag(['name' => 'description', 'content' =>$description], 'description');
		Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords')], 'keywords');
		$og_type = 'article';
		if ($image){
			Yii::$app->view->registerMetaTag(['name' => 'link', 'rel'=>'image_src', 'type'=>'image/jpeg', 'href' => $image], 'link');	
			Yii::$app->view->registerMetaTag(['property'=>'og:image', 'content' => $image], 'og:image');
			$og_type = 'website';
		}
		Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' =>$title], 'og:title');
		Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => $description], 'og:description');

		Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => $og_type], 'og:type');
		Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
		Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => 'Xabardor.uz'], 'og:site_name');
	}
}
