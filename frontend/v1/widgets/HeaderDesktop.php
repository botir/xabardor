<?php
namespace frontend\v1\widgets;


use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\v1\modules\news\models\Tag;
/**
 * Class HeaderDesktop
 * @package frontend\v1\widgets
 */
class HeaderDesktop extends \yii\base\Widget
{
  private $tags;

  public function init()
  { 
    $this->tags =  $this->getTagMenu(); 
    parent::init();
  }

    public function run()
    {
      
      echo '<header class="header wrapper__header"><div class="container"><div class="row"><div class="col-lg-2 col-md-2"><div class="logo"><h2 class="logo__text"><a href="/" title="Xabardor">Xabardor</a></h2></div></div><div class="col-lg-7 col-md-7"><div class="xb_topcenter">';
      echo '<ul class="unstyled topmenu"><li class="topmenu__item"><li class="topmenu__item"><a class="topmenu__link" href="'.Url::to(["/news/article/lenta"]).'">'.Yii::t("frontend","page_lenta").'</a></li>';
      if ($this->tags){
        foreach ($this->tags as $tag) {
          echo '<li class="topmenu__item"><a class="topmenu__link" href="'.Url::to(['/news/article/tag', 'slug' => $tag['slug']]).'">'.$tag['title'].'</a></li>';
        }
      }
      echo '</ul>';
        $this->renderForm();
      echo '</div></div>';
      echo '<div class="col-lg-3 col-md-3"><ul class="unstyled toplinks">';
      /*
      if (Yii::$app->user->isGuest){
        echo '<li class="toplinks__item"><a href="'.Url::to(['/user/sign-in/login']).'" id="sign-modal" class="toplinks__link"><i class="toplinks__icon icon-user-icon"></i><span>Kirish</span></a></li></ul>';
      }else{
        $view = $this->getView();
        $assets = \frontend\v1\assets\DesktopAsset::register($view);
        $baseUrl = $assets->baseUrl;
        $avatar = Yii::$app->user->identity->userProfile->getAvatar();

        echo '<li class="toplinks__item"><a class="toplinks__link" href="'.Url::to(['/user/default/index']).'">';
        if ($avatar){
          echo '<img class="toplinks__img" src="'.Yii::$app->user->identity->userProfile->getAvatar().'" />';
        }else{
          echo '<span class="dis-table"><span class="table-cell"><span class="profile__logo">Xabardor</span></span></span>';
        }
        
        echo '<span>'.Yii::$app->user->identity->getPublicIdentity().'</span></a> <ul class="dropdown unstyled">
                  <li><a href="'.Url::to(['/user/default/index']).'"><i class="toplinks__icon icon-user-icon"></i> '.Yii::t("frontend","user_profile_link").'</a></li>
                  <li><a href="'.Url::to(['/user/list/bookmark']).'"><i class="toplinks__icon icon-ribbon-filled-icon"></i> '.Yii::t("frontend","user_list_bookmark_link").'</a></li>
                  <li><a href="'.Url::to(['/user/sign-in/logout']).'"><i class="toplinks__icon icon-enter-right-icon"></i> '.Yii::t("frontend","user_profile_logout_link").'</a></li>
                </ul></li>';
        echo '</ul>';
      }
      */
      echo '</div></div></div></header>';
    }

    protected function renderForm()
    {
        echo Html::beginForm(Url::to(['/news/article/search']), 'get', ['class' => 'navbar-form navbar-form-search']);
        echo '<div class="search-form-container hdn search-input-container"><div class="search-input-group"><div class="form-group">';
        echo Html::textInput('q', '',
          ['placeholder' => Yii::t('frontend','search_input'),'required'=>'true', 'class'=>'form-control', 'id'=>'search-input'
        ]);
        echo '</div><button type="button" class="btnClose hide-search-input-container"><i class="icon-close-icon"></i></button></div></div><button type="submit" class="navbar-form__btn search-button"><i class="navbar-form__icon icon-loupe-icon"></i></button>';
        echo Html::endForm();
    }

    protected function getTagMenu(){
      $tags = Tag::listMenu();
      return $tags;
    }

}