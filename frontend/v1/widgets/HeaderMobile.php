<?php
namespace frontend\v1\widgets;


use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\v1\modules\news\models\Tag;
/**
 * Class HeaderMobile
 * @package frontend\v1\widgets
 */
class HeaderMobile extends \yii\base\Widget
{
  private $tags;

  public function init()
  { 
    $this->tags =  $this->getTagMenu(); 
    parent::init();
  }

    public function run()
    {
      
      echo '<header class="header wrapper__header"><div class="header-content"><div class="mobnav-container"><button class="menu-toggle"><i class="icon-menu-icon"></i></button></div><div class="logo"><h2 class="logo__text"><a href="/" title="Xabardor">Xabardor</a></h2></div><ul class="unstyled toplinks">';
      // if (Yii::$app->user->isGuest){
      //   echo '<li class="toplinks__item"><a href="'.Url::to(['/user/sign-in/login']).'" class="toplinks__link"><i class="toplinks__icon icon-user-icon"></i><span>Kirish</span></a></li>';
      // }else{
      //     echo '<li class="toplinks__item"><a href="'.Url::to(['/user/default/index']).'" class="toplinks__link"><i class="toplinks__icon icon-user-icon"></i><span>'.Yii::$app->user->identity->getPublicIdentity().'</span></a></li>';
      // }

      echo '</ul></div></header><div class="mobnav-container"><div class="mobile-menu"><div class="main-menu"><div class="main-menu">';
        $this->renderForm();
        echo '<ul class="unstyled topmenu"><li class="topmenu__item topmenu__item"><a class="topmenu__link" href="'.Url::to(["/news/article/lenta"]).'">'.Yii::t("frontend","page_lenta").'</a></li>';
        if ($this->tags){
          foreach ($this->tags as $tag) {
            echo '<li class="topmenu__item"><a class="topmenu__link" href="'.Url::to(['/news/article/tag', 'slug' => $tag['slug']]).'">'.$tag['title'].'</a></li>';
          }
        }
        echo '</ul>';
      echo '</div>';
      echo '<div class="secondary-menu"><ul class="unstyled infoblock"><li class="infoblock__item infoblock__item"><a href="'.Url::to(['/page/view', 'slug'=>'about']).'" class="infoblock__link">'.Yii::t('frontend', 'page_about_us').'</a></li><li class="infoblock__item"><a href="'.Url::to(['/contact/index']).'" class="infoblock__link">'.Yii::t('frontend','page_contact_us').'</a></li></ul></div>';
      echo'</div></div>';
      
    }

    protected function renderForm()
    {
        echo Html::beginForm(Url::to(['/news/article/search']), 'get', ['class' => 'navbar-form navbar-form-search']);
        echo '<div class="search-form-container hdn search-input-container"><div class="search-input-group"><div class="form-group">';
        echo Html::textInput('q', '',
          ['placeholder' => Yii::t('frontend','search_input'),'required'=>'true', 'class'=>'form-control', 'id'=>'search-input'
        ]);
        echo '</div><button type="button" class="btnClose hide-search-input-container"><i class="icon-close-icon"></i></button></div></div><button type="submit" class="navbar-form__btn search-button"><i class="navbar-form__icon icon-loupe-icon"></i></button>';
        echo Html::endForm();
    }

    protected function getTagMenu(){
      $tags = Tag::listMenu();
      return $tags;
    }

}