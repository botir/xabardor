<?php
namespace frontend\v1\widgets;


use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class FooterDesktop
 * @package frontend\v1\widgets
 */
class FooterDesktop extends \yii\base\Widget
{

    public function init()
    {   
        parent::init();
    }

  public function run()
  {
    echo '<footer class="footer"><div class="container-fluid"><div class="row"><div class="col-lg-6 col-md-12 col-12 white-panel"><div class="row"><div class="col-md-6 col-sm-6 col-12"><p class="copyright">&copy; 2018 Xabardor <br>'.Yii::t('frontend', 'footer_privacy_info').'</p></div><div class="col-md-6 col-sm-6 col-12"><p><span class="blue-color">'.Yii::t('frontend','footer_documentation').'<br><br>'.Yii::t('frontend','footer_address').'</p></div></div></div>';
    echo '<div class="col-lg-6 col-md-12 col-12 colorful-panel"><div class="row"><div class="col-md-4 col-sm-4 col-12 infopanel"><h5 class="infopanel__title f-title">'.Yii::t('frontend', 'footer_info_pages').'</h5><ul class="unstyled infopanel__list">';
    echo '<li class="infopanel__item"><a class="infopanel__link" href="'.Url::to(['/page/view', 'slug'=>'about']).'">'.Yii::t('frontend', 'page_about_us').'</a></li><li class="infopanel__item"><a class="infopanel__link" href="'.Url::to(['/contact/index']).'">'.Yii::t('frontend', 'page_contact_us').'</a></li><li class="infopanel__item"><a class="infopanel__link" href="'.Url::to(['/news/rss/index']).'">'.Yii::t('frontend', 'page_rss').'</a></li></ul></div>';
    echo '<div class="col-md-8 col-sm-8 col-12 social">';
    echo '<div class="social-box"><h5 class="social-box__title f-title">'.Yii::t('frontend', 'footer_social').'</h5><ul class="unstyled social-links"><li class="social-links__item"><a class="social-links__link social-links__link--facebook" href="https://www.facebook.com/habardor.uz" target="__BLANK"><i class="icon-facebook-icon"></i></a></li><li class="social-links__item"><a class="social-links__link social-links__link--twitter" href="https://twitter.com/xabardor" target="__BLANK"><i class="icon-twitter-icon"></i></a></li><li class="social-links__item"><a class="social-links__link social-links__link--telegram" href="https://t.me/xabardoruz" target="__BLANK"><i class="icon-telegram"></i></a></li><li class="social-links__item"><a class="social-links__link social-links__link--instagram" href="http://instagram.com/xabardoruz" target="__BLANK"><i class="icon-instagram-icon"></i></a></li></ul></div></div></div></div></div></div></footer>';
  }

}