<?php
use yii\helpers\Url;
use yii\helpers\Json;
$assets = \frontend\v1\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$this->title='Xabardorga qo\'shiling';
?>

<div class="infopanel">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-12">
            <h3 class="infopanel__title">O’qishni yaxshi ko’radigan insonlar uchun. Cheksiz.</h3>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-6 col-md-12 col-12">
		<div class="subscribe__item">
			<div class="sbscr-item__header">
            	<h3 class="sbscr-item__title"><?=$service['title']?></h3>
            	<p class="sbscr-item__price"><b><?=$service['price']?></b> so’m</p>
          	</div>
           	<div class="sbscr-item__body">
           		<p class="text-center"><strong>Xabardorga obuna bo’lish bu:</strong></p>
	           	<div class="yes-box">
	           		<ul class="unstyled yesno__list">
	           			<?php $service_options = Json::decode($service['more_option']); foreach($service_options as $service_option){
				    		echo '<li class="yesno__item">'.$service_option['title'].'</li>';
				    	}?>
	           		</ul>
	           	</div>
           </div>
		</div>
	</div>
	<div class="col-lg-6 col-md-12 col-12">
		
			<div class="row">
				<div class="col-md-12">
					<div class="subscribe__item">
						<form id="paycom" method="POST" action="http://checkout.test.paycom.uz" class="payment__form">
						 	<div class="form-group">
						 		<div class="row justify-content-start">
						 			<div class="col-md-6">
				                      	<img src="<?=$baseUrl?>/images/payme.png" />
				                      	<input type="hidden" name="merchant" value="5c6a5c9f369f81ed56750419" />
										<input type="hidden" name="amount" value="<?= intval($order->price) *100?>" />
										<input type="hidden" name="account[order]" value="<?=$order->id;?>" />
				                    </div>
				                    <div class="col-md-6">
				                      <button type="submit" class="btn btn-success">To’lash</button>
				                    </div>
						 		</div>
						 	</div>
						</form>
					</div>	
				</div>	
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="subscribe__item">
						<form id="click" method="POST" action="https://test.paycom.uz" class="payment__form">
						 	<div class="form-group">
						 		<div class="row justify-content-start">
						 			<div class="col-md-6">
				                      	<img src="<?=$baseUrl?>/images/click.png" />
				                      	<input type="hidden" name="merchant" value="5c6a5c9f369f81ed56750419" />
										<input type="hidden" name="amount" value="<?= intval($order->price) *100?>" />
										<input type="hidden" name="account[order]" value="<?=$order->id;?>" />
				                    </div>
				                    <div class="col-md-6">
				                      <button type="submit" class="btn btn-success">To’lash</button>
				                    </div>
						 		</div>
						 	</div>
						</form>
					</div>	
				</div>	
			</div>
	</div>
</div>