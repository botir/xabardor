<?php
/* @var $content string */

$this->beginContent('@frontend/v1/views/layouts/_clear.php')
?>
    <div class="wrapper">
		<?=\frontend\v1\widgets\HeaderDesktop::widget()?>
    <?php if ($this->ads):?>
    	<div class="ads-container">
      		<div class="container">
      			<?=\frontend\v1\widgets\AdDesktop::widget(['place'=>1, 'link_class'=>'top-ads'])?>
      		</div>      
    	</div>
    <?php endif;?>
    	<?php echo $content ?>
	</div>
	<?=\frontend\v1\widgets\FooterDesktop::widget()?>
<?php $this->endContent() ?>