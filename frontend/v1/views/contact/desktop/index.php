<?php
$this->title = \Yii::t('frontend', 'meta_contact_title');
$assets = \frontend\v1\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$this->ads = false;
?>

<div class="main-content">
  <div class="container">
    <div class="content">
      <div class="contact">
        <div class="row justify-content-center">
          <div class="col-lg-10 col-md-12 col-12">
            <div class="divices">
              <div class="row justify-content-center">
                <div class="col-md-8 col-sm-12">
                  <img class="divices__img" src="<?=$baseUrl?>/images/devices.svg"/>
                  <h2 class="divices__title"><?=Yii::t('frontend', 'reklama_content_title')?></h2>
                </div>
              </div>                  
            </div>                
            <div class="row">
              <div class="col-md-4 col-sm-12 col-12">
                <div class="contact__item item-phone">
                  <div class="contact__icon">
                    <img src="<?=$baseUrl?>/images/phone.svg" />
                  </div>
                  <ul class="unstyled contact__numbers numlist">
                    <li class="numlist__item">
                      <a class="numlist__link" href="tel:+998951462323">+998 (95) 146 23 23</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-md-4 col-sm-12 col-12">
                <div class="contact__item item-email">
                  <div class="contact__initem">
                    <a class="item-email__link" href="mailto:info@xabardor.uz" title="info@xabardor.uz">
                      <i class="icon-mail-icon"></i>
                      <span>info@xabardor.uz</span>
                    </a>
                  </div>                      
                </div>
              </div>
              <div class="col-md-4 col-sm-12 col-12">
                <div class="contact__item item-email">
                  <div class="contact__initem">
                    <ul class="unstyled socialbtn">
                      
                      <li class="socialbtn__item">
                        <a class="socialbtn__link" href="https://www.facebook.com/habardor.uz">
                          <img src="<?=$baseUrl?>/images/facebook.svg"/>
                          <span>@xabardor</span>
                        </a>
                      </li>
                    </ul>
                  </div>                      
                </div>
              </div>
            </div>
            
          </div>
        </div>            
      </div>
    </div>        
  </div>
</div>