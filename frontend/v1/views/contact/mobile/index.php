<?php
$this->title = \Yii::t('frontend', 'meta_contact_title');
$assets = \frontend\v1\assets\MobileAsset::register($this);
$baseUrl = $assets->baseUrl;
$this->ads = false;
?>
<div class="main-content">
	<div class="content contact">
		<div class="divices">
			<img class="divices__img" src="<?=$baseUrl?>/images/devices.svg"/>
			<h2 class="divices__title">Reklama bilan bog’liq istalgan savollarni quyidagi kontaktlar orqali aloqaga chiqingiz mumkin</h2>
		</div>
		<div class="contact__item item-phone">
			<div class="contact__icon"><img src="<?=$baseUrl?>/images/phone.svg" /></div>
			<ul class="unstyled contact__numbers numlist">
				<a class="numlist__link" href="tel:+998951462323">+998 (95) 146 23 23</a>
			</ul>
		</div>
		<div class="contact__item item-email">
			<div class="contact__initem">
				<a class="item-email__link" href="mailto:info@xabardor.uz" title="info@xabardor.uz"><i class="icon-mail-icon"></i><span>info@xabardor.uz</span></a>
			</div>
		</div>
		<div class="contact__item item-email">
			<div class="contact__initem">
				<ul class="unstyled socialbtn">
					<li class="socialbtn__item">
						<a class="socialbtn__link" href="https://www.facebook.com/habardor.uz">
							<img src="<?=$baseUrl?>/images/facebook.svg"/>
							<span>@xabardor</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>