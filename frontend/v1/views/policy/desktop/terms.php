<?php 
$this->title = $model->title;
$assets = \frontend\v1\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$this->ads = false;
?>

<div class="main-content about">
  <div class="container">
   
    <div class="content">
     <?=$model->content?>     
    </div>        
  </div>
</div>