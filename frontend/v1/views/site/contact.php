<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = Yii::t('frontend', 'contact_page_title');

?>
<div class="main-content full-content">
<div class="container">
    <div class="white-block mt-0 full-height">
    <div class="site-contact">
        <h1><?php echo Yii::t('frontend', 'contact_page_send_message') ?></h1>
<?php if(Yii::$app->session->hasFlash('alert')):?>
                                <?php echo \yii\bootstrap\Alert::widget([
                                    'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                                    'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                                ])?>
                            <?php endif; ?>
        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                    

                    <?php echo $form->field($model, 'name',[
                    'options' => ['class' => 'signform__form-group'],
                    'template' => '{label}<div class="signform__input-group"><div class="signform__input-group-prepend"></div>{input}</div>{error}'])
                    ->textInput([
                        'placeholder'=>Yii::t('frontend', 'contact_form_name'),
                        'maxlength' => true,
                        'class' => 'signform__form-control',
                    ]); 
                    ?>
                    <?php echo $form->field($model, 'phone',[
                    'options' => ['class' => 'signform__form-group'],
                    'template' => '{label}<div class="signform__input-group"><div class="signform__input-group-prepend"></div>{input}</div>{error}'])
                    ->textInput([
                        'placeholder'=>Yii::t('frontend', 'contact_form_phone'),
                        'maxlength' => true,
                        'class' => 'signform__form-control',
                    ]); 
                    ?>

                    <?php echo $form->field($model, 'subject',[
                    'options' => ['class' => 'signform__form-group'],
                    'template' => '{label}<div class="signform__input-group"><div class="signform__input-group-prepend"></div>{input}</div>{error}'])
                    ->textInput([
                        'placeholder'=>Yii::t('frontend', 'contact_form_subject'),
                        'maxlength' => true,
                        'class' => 'signform__form-control',
                    ]); 
                    ?>
                    <?php echo $form->field($model, 'body',[
                    'options' => ['class' => 'signform__form-group'],
                    'template' => '{label}<div class="signform__input-group"><div class="signform__input-group-prepend"></div>{input}</div>{error}'])
                    ->textArea([
                        'placeholder'=>Yii::t('frontend', 'contact_form_body'),
                        'rows' => 5,
                        'class' => 'signform__form-control',
                    ]); 
                    ?>

                    
                    <?php echo $form->field($model, 'verifyCode')->widget(Captcha::class, [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>
                    <div class="form-group">
                        <?php echo Html::submitButton(Yii::t('frontend', 'contact_form_submit'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>
</div>
</div>
