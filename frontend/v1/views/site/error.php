<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$assets = \frontend\v1\assets\DesktopAsset::register($this);
$baseUrl = $assets->baseUrl;
$this->ads = false;
?>


<div class="main-content">
      <div class="container">
        <div class="content">
          <div class="error-page">
            <div class="error-page__text"><?php echo Html::encode($this->title) ?></div>
            <div class="error-page__img">
              <img class="img-fluid" src="<?=$baseUrl?>/images/404.png" alt="" />
            </div>
            <div class="error-page__content">
              <p><?php echo nl2br(Html::encode($message)) ?></p>
              <p><a class="blue-color" href="index.html">Bosh sahifaga</a> qaystish</p>
            </div>
          </div>
        </div>        
      </div>
    </div>