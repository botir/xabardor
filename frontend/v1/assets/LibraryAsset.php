<?php
namespace frontend\v1\assets;

use common\assets\Html5shiv;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Frontend application asset
 */
class LibraryAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/v1/web/static/main';

    /**
     * @var array
     */
    public $js = [
        'ias/callbacks.js',
        'ias/jquery-ias.js',
        'ias/extension/history.js',
        'ias/extension/noneleft.js',
        'ias/extension/paging.js',
        'ias/extension/spinner.js',
        'ias/extension/trigger.js',

        'load-image/load-image.all.min.js',
        'fileupload/vendor/jquery.ui.widget.js',
        'fileupload/jquery.iframe-transport.js',
        'fileupload/jquery.fileupload.js',
        'fileupload/jquery.fileupload-process.js',
        'fileupload/jquery.fileupload-image.js',
        'fileupload/jquery.fileupload-validate.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        Html5shiv::class,
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
}
