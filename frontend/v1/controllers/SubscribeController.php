<?php

namespace frontend\v1\controllers;

use Yii;
use frontend\v1\components\FrontendController;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use common\models\PaymentUser;
use common\models\PaymentService;
use common\models\PaymentOrder;

/**
 * Subscribe controller
 */
class SubscribeController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionView($name)
    {
        $user = Yii::$app->user->identity;
        $service = $this->findService($name);
        $order = $this->addOrder($service['id'], $service['price'], $user->id);
        if (Yii::$app->devicedetect->isMobile()){
            return $this->render('mobile/view', ['service'=>$service, 'order'=>$order]);
        }else{
            $this->layout = "//center";
            return $this->render('view', ['service'=>$service, 'order'=>$order]);
        }
    }

    protected function findService($name)
    {  
        $service = PaymentService::getByName($name);
        if ($service !== null && $service) {
            return $service;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function addOrder($service_id, $price, $user_id)
    {  
        $order = new PaymentOrder();
        $order->service_id = $service_id;
        $order->status = 1;
        $order->price = $price;
        $order->user_id = $user_id;
        if ($order->save()) {
            return $order;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
