<?php

namespace frontend\v1\controllers;

use yii\web\NotFoundHttpException;
use frontend\v1\components\FrontendController;
use Yii;

/**
 * Contact controller
 */
class ContactController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {   
        if ($this->is_mobile){
            return $this->render('mobile/index');
        }else{
            return $this->render('desktop/index');
        }
    }

    /**
     * Finds the PageStatic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageStatic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = PageStatic::find()->where(['slug'=>$slug])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
