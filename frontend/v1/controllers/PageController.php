<?php

namespace frontend\v1\controllers;

use yii\web\NotFoundHttpException;
use common\models\PageStatic;
use frontend\v1\components\FrontendController;
use Yii;

/**
 * Page controller
 */
class PageController extends FrontendController
{
    /**
     * @return string
     */
    public function actionView($slug)
    {
        $model = $this->findModel($slug);
        if ($model->view_file){
            $view = $model->view_file;
        }else{
            $view = 'view';
        }
        if ($this->is_mobile){
            return $this->render('mobile/'.$view, ['model' => $model]);
        }else{
            return $this->render('desktop/'.$view, ['model' => $model]);
        }
    }

    /**
     * Finds the PageStatic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageStatic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = PageStatic::find()->where(['slug'=>$slug])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
