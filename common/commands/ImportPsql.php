<?php

namespace common\commands;

use Yii;
use yii\base\BaseObject;
use yii\db\Query;
use yii\helpers\Json;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;
use Imagine\Image\Box;
use common\models\NewsArticle;

class ImportPsql extends BaseObject implements \yii\queue\Job
//class ImportPsql extends BaseObject
{

    const THUMBNAIL_OUTBOUND = ManipulatorInterface::THUMBNAIL_OUTBOUND;
    const THUMBNAIL_INSET = ManipulatorInterface::THUMBNAIL_INSET;
    public static $cacheAlias = 'thumbnails';

    public $news_list;


    public function execute($queue)
    {
        try {
            $news = $this->news_list;
            $bulkInsertArray = [];
            foreach ($news as $data) {
                $tags=[];
                $path = $data['image'];
                $full_path =  Yii::getAlias('@storage').'/web/source'.$data['image'];
                $main_path = Yii::getAlias('@storage').'/web/source/';
                if (file_exists($full_path)){
                    self::thumbnailFileUrl($main_path, $full_path, $path, '_medium',640, 360);
                    self::thumbnailFileUrl($main_path, $full_path, $path, '_small',256, 144);
                    self::thumbnailFileUrl($main_path, $full_path, $path, '_ns',313, 234);
                }else{
                    echo 'NO';
                }
                //print_r($full_path); exit;
                $tags[] = ['id'=>intval($data['category_id']), 'slug'=>$data['category_slug'], 'title'=>$data['category_name']];
                $more_option = [
                    'show_image' => true,
                    'list_image' => true,
                    'show_desc' => true,
                    'is_allowcoment' => false,
                    'caption_image' => null,
                    'tags' => $tags,
                    'like_count' => 0
                ];
                //print_r($more_option); exit;
                $tab_number = $this->generateCode();
                $media_base_url = Yii::getAlias('@storageUrl').'/source';
                $bulkInsertArray[]=[
                    'tab_number' => $tab_number,
                    'content_type' => 1,
                    'category_id' => $data['category_id'],
                    'title' => $data['title'],
                    'description' => $data['description'],
                    'content' => str_replace('/upload/',$media_base_url.'/upload/',$data['content']),
                    'slug' => $data['slug'],
                    'views_count' => $data['views_count'],
                    'status' => 2,
                    'published_at' => $data['creation_date'],
                    'created_at' => $data['creation_date'],
                    'updated_at' => $data['creation_date'],
                    'author_id' => 1,
                    'user_id' => 1,
                    'image_path' => $path,
                    'image_base_url' => $media_base_url,
                    'more_option' => Json::encode($more_option)
                ];
                //$this->addHashTag($data['category_id']);
            
            }

            $connection =  \Yii::$app->db;
            $connection->createCommand()->batchInsert('news_article',  
                [
                    'tab_number',
                    'content_type', 
                    'category_id', 
                    'title',
                    'description',
                    'content',
                    'slug',
                    'views_count',
                    'status',
                    'published_at',
                    'created_at',
                    'updated_at',
                    'author_id',
                    'user_id',
                    'image_path',
                    'image_base_url',
                    'more_option',
                ], $bulkInsertArray)->execute();
        }catch (\Exception $e) {
            \Yii::warning( $e->getMessage(), 'IMPORT');
        }
        return 1;
    }

     protected function getNewsList($from){
        $query = (new Query())
            ->select('categories.id as category_id, categories.name_en as category_name, categories.url_code as category_slug, products.price_amount as views_count, products.url_code as slug, products.creation_date, products.list_view_img_url as image,
products.name_en as title,product_contents.short_description as description, product_contents.description as content')
            ->from('products')
            ->innerJoin('product_contents',"products.id=product_contents.product_id and product_contents.lang='en'")
            ->innerJoin('category_product',"products.id=category_product.product_id")
            ->innerJoin('categories',"category_product.category_id=categories.id")
            ->where('products.creation_date>:from and products.price_amount>1')
            //->groupBy('products.url_code')
            ->orderBy('products.creation_date asc')
            ->limit(500)
            ->params([
             ':from' =>$from
           ]);
        return $query->all(\Yii::$app->db_mysql);
    }

    public function generateCode()
    {
        $number=100000;
        while(true){
           $number = rand(100000,999999); 
           $result = NewsArticle::find()
            ->select(['tab_number'])
            ->where(['tab_number' => $number])
            ->asArray()
            ->scalar();
            if (!$result){
                break;
            }
        }
        return $number;
    }

     protected function getCategoryList(){
        $query = (new Query())
            ->select('categories.id as id, categories.ordering as ordering, categories.name_en as category_name, categories.url_code')
            ->from('products')
            ->innerJoin('product_contents',"products.id=product_contents.product_id and product_contents.lang='en'")
            ->innerJoin('category_product',"products.id=category_product.product_id")
            ->innerJoin('categories',"category_product.category_id=categories.id")
            ->groupBy('category_product.category_id')
            ->orderBy('category_product.category_id')
            ->limit(100);
        return $query->all(\Yii::$app->db_mysql);
    }

    protected function addHashTag($id){
        Yii::$app->db->createCommand('UPDATE news_tag SET c_weight=c_weight+1 WHERE id='.$id)->execute();
    }

    public static function thumbnailFileUrl($main_path, $full_path, $file_path, $t_name, $width, $height, $mode = self::THUMBNAIL_INSET)
    {
        if (!is_file($full_path)) {
            return $main_path;
        }
        $file_obj = explode(DIRECTORY_SEPARATOR,$file_path);
        $filename = $file_obj[count($file_obj)-1];
        //echo $filename; exit;
        $cachePath = $main_path.self::$cacheAlias;
        $thumbnailFileExt = strrchr($filename, '.');
        $y = explode('.',$filename);
        $thumbnailFileName = $y[0].$t_name;

        $thumbnailFilePath = $cachePath . DIRECTORY_SEPARATOR .$t_name;

        for($i=1; $i<(count($file_obj)-1); $i++){ 
            $thumbnailFilePath.=DIRECTORY_SEPARATOR . $file_obj[$i]; 
        }

        $thumbnailFile = $thumbnailFilePath . DIRECTORY_SEPARATOR .$thumbnailFileName . $thumbnailFileExt;
        $ext = substr($thumbnailFileExt, 1);
        if (!is_dir($thumbnailFilePath)) {
            mkdir($thumbnailFilePath, 0755, true);
        }
        if (!$height||$height<0) $height = $width;
        $box = new Box($width, $height);
        $image = Image::getImagine()->open($full_path);
        $image = $image->thumbnail($box, $mode);
        if ($ext=='jpe') $ext='jpeg';
        //print_r($thumbnailFile); exit;
        $image->save($thumbnailFile,['format'=>$ext, 'quality' => 95]);
        
        return $thumbnailFile;
    }

}
