<?php

namespace common\commands;

use yii\base\BaseObject;
use yii\helpers\Json;

class SaveNewsContent extends BaseObject
{

    public $news_list;

    public function execute($queue)
    {
        $news = $this->news_list;
        $bulkInsertArray = [];
        foreach ($news as $data) {
            $connection =  \Yii::$app->db;
                
            $news_id = $data['id'];
            $lang_id = 1;
            $title = $data['title'];
            $description = $data['description'];
            $content = $data['content'];
            $array_data = Json::decode($data['more_option']);
            $caption_image = $array_data['caption_image'];

            $bulkInsertArray[]=[
                'news_id' => $news_id,
                'lang_id' => $lang_id,
                'title' => $title,
                'description' => $description,
                'content' => $content,
                'caption_image' => $caption_image,
               ];

            if ($lang_id == 1){

                $bulkInsertArray[]=[
                    'news_id' => $news_id,
                    'lang_id' => 2,
                    'title' => $title,
                    'description' => $description,
                    'content' => $content,
                    'caption_image' => $caption_image,
               ];

               
            }
        }
        $connection->createCommand()->batchInsert('news_data',  ['news_id', 'lang_id', 'title', 'description', 'content', 'caption_image'], $bulkInsertArray)->execute();
        return 1;
    }

    public function execute2222($queue)
    {
        $news = $this->news_list;
        $bulkInsertArray = [];
        foreach ($news as $data) {
            $connection =  \Yii::$app->db;
                
            $news_id = $data['id'];
            $lang_id = 1;
            $title = $data['title'];
            $description = $data['description'];
            $content = $data['content'];
            $array_data = Json::decode($data['more_option']);
            $caption_image = $array_data['caption_image'];

            $bulkInsertArray[]=[
                'news_id' => $news_id,
                'lang_id' => $lang_id,
                'title' => $title,
                'description' => $description,
                'content' => $content,
                'caption_image' => $caption_image,
               ];

            if ($lang_id == 1){
                $title = $title;
                $description =  $description;
                $content = $content;
                if ($caption_image)
                    $caption_image = $caption_image;

                $bulkInsertArray[]=[
                    'news_id' => $news_id,
                    'lang_id' => 2,
                    'title' => $title,
                    'description' => $description,
                    'content' => $content,
                    'caption_image' => $caption_image,
               ];

               
            }
        }
        $connection->createCommand()->batchInsert('news_data',  ['news_id', 'lang_id', 'title', 'description', 'content', 'caption_image'], $bulkInsertArray)->execute();
        return 1;
    }

}
