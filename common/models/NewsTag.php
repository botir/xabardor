<?php

namespace common\models;

use common\behaviors\sluggable\SluggableBehavior;
use Yii;

/**
 * This is the model class for table "news_tag".
 *
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property int $sort
 * @property int $status
 * @property int $c_weight
 * @property int $is_main
 */
class NewsTag extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['sort', 'status'], 'default', 'value' => 2],
            [['sort', 'status', 'c_weight'], 'integer'],
            [['slug', 'title'], 'string', 'max' => 150],
            [['is_menu', 'is_main'], 'boolean'],
            [['slug', 'title'], 'unique', 'targetAttribute' => ['slug', 'title']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'slug'      => 'ЧПУ',
            'title'     => 'Название',
            'sort'      => 'Сортировка',
            'status'    => 'Статус',
            'c_weight'  => 'Weight',
            'is_menu'   => 'Меню',
            'is_main'   => 'Главный',
        ];
    }
    /**
     * @return array all status list
     */
    public static function allStatusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_DELETED => 'Удаленный',
        ];
    }

    /**
     * @return array all status list
     */
    public static function statusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        ];
    }

    /**
     * @return get category status
     */
    public function currentStatus()
    {
        $data = self::allStatusList();
        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }
    
    public static  function getListData()
    {

        $tags = self::find()->where(['status' => 2])->all();

        $list = [];

        foreach ($tags as $tag) {
            $list[$tag->id] = $tag->title;
        }

        return $list;
    }

    public function behaviors()
    {

        return [
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'title',
                'immutable' => true
            ]
        ];
    }
}
