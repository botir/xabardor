<?php

namespace common\models;

use yii\db\Query;
use yii\behaviors\TimestampBehavior;
use common\behaviors\PageBehavior;
use Yii;

/**
 * This is the model class for table "page_static".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $content_type
 * @property int $views_count
 * @property bool $is_active
 * @property bool $is_menu
 * @property string $slug
 * @property string $view_file
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class PageStatic extends \yii\db\ActiveRecord
{
    
    const TYPE_STATIC_PAGE_V1 = 1;
    const TYPE_FAQ_PAGE_V1 = 2;


    public $title_uz;
    public $content_uz;
    public $title_kr;
    public $content_kr;
    public $title_ru;
    public $content_ru;
    public $title_en;
    public $content_en;

    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_static';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'slug'], 'required'],
            [['title_uz', 'content_uz', 'title_kr', 'content_kr', 'title_ru', 'content_ru', 'title_en', 'content_en'], 'required'],
            [['content'], 'string'],
            [['content_type'], 'default', 'value' => 1],
            [['views_count'], 'default', 'value' => 1],
            [['content_type', 'views_count', 'user_id'], 'integer'],
            [['is_active', 'is_menu'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 510],
            [['title_uz', 'title_kr', 'title_ru', 'title_en'], 'string', 'max' => 405],
            [['slug'], 'string', 'max' => 150],
            [['view_file'], 'string', 'max' => 255],
            [['content_type', 'slug'], 'unique', 'targetAttribute' => ['content_type', 'slug']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Контент',
            'content_type' => 'Тип страница',
            'views_count' => 'Количество просмотров',
            'is_active' => 'Актив',
            'is_menu' => 'Меню',
            'slug' => 'Slug',
            'view_file' => 'Рендер файл',
            'user_id' => 'Пользователь',
            'created_at' => 'Создано',
            'updated_at' => 'Последнее обновление',
            'title_uz' => 'Заголовок(uz)',
            'content_uz' => 'Текст(uz)',
            'title_kr' => 'Заголовок(kr)',
            'content_kr' => 'Текст(kr)',
            'title_ru' => 'Заголовок(ru)',
            'content_ru' => 'Текст(ru)',
            'title_en' => 'Заголовок(en)',
            'content_en' => 'Текст(en)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array type list
     */
    public static function getTypeList()
    {
        return [
            self::TYPE_STATIC_PAGE_V1 => 'Статик',
            self::TYPE_FAQ_PAGE_V1 => 'FAQ'
        ];
    }

    /**
     * @return get content type
     */
    public function getContentType()
    {
        $data = self::getTypeList();
        return isset($data[$this->content_type]) ? $data[$this->content_type] : '*unknown*';
    }

    public function behaviors()
    {

        return [
             [
                'class'=>PageBehavior::className()
            ],
            [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
            ]
        ];
    }


    public static function getPageBySlug($slug)
  {
    $query = (new Query())
        ->select('page_data.title, page_data.content')
        ->from('page_static')
        ->innerJoin('page_data','page_static.id=page_data.page_id and page_data.lang_id = 1')      
        ->where('page_static.is_active=true and page_static.slug=:slug')
        ->params([':slug'=>$slug, ':lang_id'=>$lang_id])
        ->limit(1);

    return $query->one();

  }
}
