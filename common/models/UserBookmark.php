<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;

/**
 * This is the model class for table "user_bookmark".
 *
 * @property int $news_id
 * @property int $user_id
 * @property string $created_at
 *
 * @property NewsArticle $news
 * @property User $user
 */
class UserBookmark extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_bookmark';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'user_id'], 'required'],
            [['news_id', 'user_id'], 'default', 'value' => null],
            [['news_id', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['news_id', 'user_id'], 'unique', 'targetAttribute' => ['news_id', 'user_id']],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewsArticle::className(), 'targetAttribute' => ['news_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(NewsArticle::className(), ['id' => 'news_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function behaviors()
    {

        return [
            [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => false,
              'value' => date('Y-m-d H:i:s'),
            ]
        ];
    }

    public static function listArticles($user_id, $next, $limit=12)
    {
      $query = (new Query())
        ->select('tab_number, title, description, slug, "published_at"::timestamp, more_option, ("more_option"->>\'list_image\')::boolean AS "list_image", ("more_option"->>\'show_desc\')::boolean AS "show_desc", content_type, is_ads, is_selected, views_count, "image_base_url"||\'/thumbnails\'||LEFT(image_path, POSITION(\'.\' IN image_path)-1) as thumb_name, right(image_path, POSITION(\'.\' IN REVERSE(image_path)) - 1) as image_ext')
        ->from('user_bookmark')
        ->innerJoin('news_article','user_bookmark.news_id=news_article.id and news_article.status=2 and published_at<:next')
        ->where('user_bookmark.user_id = :user_id')
        ->orderBy(['published_at'=>SORT_DESC])
        ->limit($limit)
        ->params([':next' =>$next, ':user_id'=>$user_id]);
      return $query->all();
    }
}
