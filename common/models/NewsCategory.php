<?php

namespace common\models;

use common\behaviors\sluggable\SluggableBehavior;
//use yii\behaviors\SluggableBehavior;
use Yii;

/**
 * This is the model class for table "news_category".
 *
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $title_oz
 * @property int $sort
 * @property bool $is_menu
 * @property int $lang_id
 * @property int $status
 * @property bool $is_main
 *
 * @property NewsArticle[] $newsArticles
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'lang_id'], 'required'],
            [['sort', 'status'], 'default', 'value' => 2],
            [['sort', 'status'], 'integer'],
            [['slug', 'title'], 'string', 'max' => 150],
            [['slug'], 'unique'],
            [['is_menu', 'is_main'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'slug'      => 'ЧПУ',
            'title'     => 'Название',
            'title_oz'  => 'Название(Крилчада)',
            'sort'      => 'Сортировка',
            'status'    => 'Статус',
            'is_menu'   => 'Меню',
            'lang_id'   => 'Язык',
            'is_main'   => 'Главный',
        ];
    }

    /**
     * @return array all status list
     */
    public static function allStatusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_DELETED => 'Удаленный',
        ];
    }

    /**
     * @return array all status list
     */
    public static function statusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        ];
    }

    /**
     * @return get category status
     */
    public function currentStatus()
    {
        $data = self::allStatusList();
        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsArticles()
    {
        return $this->hasMany(NewsArticle::className(), ['category_id' => 'id']);
    }

    public static  function getListData()
    {

        $categories = self::find()->where(['status' => 2])->all();

        $list = [];

        foreach ($categories as $category) {
            $list[$category->id] = $category->title;
        }

        return $list;
    }

    public function behaviors()
    {

        return [
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'title',
                'immutable' => true
            ],
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'title',
                'immutable' => true
            ],
        ];
    }
}
