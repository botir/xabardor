<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use common\behaviors\ArticleBehavior;
use Yii;

/**
 * This is the model class for table "news_article".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $open_content
 * @property int $category_id
 * @property int $tab_number
 * @property string $image_path
 * @property string $image_base_url
 * @property int $content_type
 * @property bool $is_actual
 * @property bool $is_selected
 * @property bool $is_ads
 * @property bool $is_premium
 * @property int $views_count
 * @property int $status
 * @property string $published_at
 * @property string $created_at
 * @property string $updated_at
 * @property array $more_option
 * @property string $slug
 * @property int $author_id
 * @property int $user_id
 * @property bool $day_photo
 *
 * @property NewsCategory $category
 * @property User $user
 * @property UserAuthor $author
 */
class NewsArticle extends \yii\db\ActiveRecord
{
    
    const TYPE_POST = 1;
    const TYPE_MAIN = 2;
    const TYPE_SELECTED = 3;

    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;

    public $lang_id;
    public $lead_info;
    public $hash_tags;
    public $caption_image;
    public $show_image;
    public $list_image;
    public $show_desc;
    public $is_allowcoment;
    public $category_title;

    /**
     * @var array
    */
    public $photo;

    /*
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'content', 'published_at', 'status', 'hash_tags', 'lang_id', 'category_id'], 'required'],
            [['content', 'open_content', 'lead_info'], 'string'],
            
            [['views_count'], 'default', 'value' => 0],
             
            [['category_id', 'tab_number', 'content_type', 'views_count', 'author_id', 'user_id', 'lang_id'], 'integer'],
            [['is_actual', 'is_selected', 'is_ads', 'is_premium', 'is_column'], 'boolean'],
            [['published_at', 'created_at', 'updated_at', 'more_option', 'photo', 'lead_info'], 'safe'],
            [['title', 'image_path', 'image_base_url'], 'string', 'max' => 510],
            [['description'], 'string', 'max' => 600],
            [['slug', 'caption_image'], 'string', 'max' => 150],
            [['slug'], 'unique'],
            [['tab_number'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewsCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserAuthor::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['published_at'],'datetime', 'format'=>'php:d.m.Y, H:i:s'],

            [['show_image', 'list_image', 'show_desc', 'is_allowcoment', 'day_photo'], 'boolean'],
            ['hash_tags', 'checkIsArray'],
        ];
    }

    public function checkIsArray(){
     if(!is_array($this->hash_tags)){
         $this->addError('hash_tags','hash_tag error');
     }
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'title'             => 'Название',
            'description'       => 'Описание',
            'content'           => 'Текст',
            'open_content'      => 'Открытый текст',
            'category_id'       => 'Категории',
            'tab_number'        => 'Код',
            'image_path'        => 'Image Path',
            'image_base_url'    => 'Image Base Url',
            'content_type'      => 'Тип статья',
            'is_actual'         => 'Долзарб',
            'is_selected'       => 'Muxarrir tanlovi',
            'is_ads'            => 'Реклама',
            'is_column'         => 'Колумнист',
            'is_premium'        => 'Премиум ',
            'views_count'       => 'Количество просмотров',
            'status'            => 'Статус',
            'published_at'      => 'Дата публикации',
            'created_at'        => 'Создано',
            'updated_at'        => 'Последнее обновление',
            'more_option'       => 'Опция',
            'slug'              => 'ЧПУ',
            'author_id'         => 'Автор',
            'user_id'           => 'Пользователь',

            'show_image'        => 'Главное Фото',
            'list_image'        => 'Лента Фото',
            'show_desc'         => 'Описание',
            'is_allowcoment'    => 'Комментарий',
            'caption_image'     => 'Источник фото',
            'hash_tags'         => 'Хештеги',
            'photo'             => 'Фото',
            'day_photo'         => 'Фото дня',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(UserAuthor::className(), ['id' => 'author_id']);
    }

    /**
     * @return array all status list
     */
    public static function allStatusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_DELETED => 'Удаленный',
        ];
    }

    /**
     * @return array status list
     */
    public static function getTypeList()
    {
        return [
            self::TYPE_POST => 'Макола',
            self::TYPE_MAIN => 'Асосий',
            self::TYPE_SELECTED => 'Танланган'
        ];
    }

    /**
     * @return array status list
     */
    public static function statusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        ];
    }

    /**
     * @return get category status
     */
    public function currentStatus()
    {
        $data = self::allStatusList();
        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }

    /**
     * @return get category status
     */
    public function getContentType()
    {
        $data = self::getTypeList();
        return isset($data[$this->content_type]) ? $data[$this->content_type] : '*unknown*';
    }

    public function getShortUrl(){
        $url = Yii::getAlias('@frontendUrl').'/x/'.$this->tab_number;
        return $url;
    }

    public static function updateViews($id){

        Yii::$app->db->createCommand('UPDATE news_article SET views_count=views_count+1 WHERE id='.$id)
            ->execute();
    }

    public static function updateLikesCount($tab_number){

        Yii::$app->db->createCommand('update news_article  
SET more_option = jsonb_set(more_option, \'{like_count}\', (COALESCE(more_option->>\'like_count\',\'0\')::int + 1)::text::jsonb)
where "tab_number" = '.$tab_number)
            ->execute();
    }

    public function behaviors()
    {

        return [
            [
                'class'=>ArticleBehavior::className()
            ],
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'title',
                'immutable' => true
            ],
            [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'photo',
                'pathAttribute' => 'image_path',
                'baseUrlAttribute' => 'image_base_url'
            ],
        ];
    }
}
