<?php

namespace common\models;

use yii\db\Query;
use yii\behaviors\TimestampBehavior;
use common\behaviors\sluggable\SluggableBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "user_author".
 *
 * @property int $id
 * @property string $nickname
 * @property string $email
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $description
 * @property string $avatar_path
 * @property string $avatar_base_url
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property NewsArticle[] $newsArticles
 */
class UserAuthor extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;

    /**
     * @var array
    */
    public $avatar;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_author';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['created_at', 'updated_at', 'avatar'], 'safe'],
            [['nickname'], 'string', 'max' => 32],
            [['email', 'firstname', 'middlename', 'lastname'], 'string', 'max' => 255],
            [['description', 'avatar_path', 'avatar_base_url'], 'string', 'max' => 510],
            [['email'], 'unique'],
            [['nickname'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'nickname'          => 'Никнейм',
            'email'             => 'Email',
            'firstname'         => 'Имя',
            'middlename'        => 'Отчество',
            'lastname'          => 'Фамилия',
            'description'       => 'Слоган',
            'avatar_path'       => 'Avatar Path',
            'avatar_base_url'   => 'Avatar Base Url',
            'avatar'            => 'Фото',
            'status'            => 'Статус',
            'created_at'        => 'Создано',
            'updated_at'        => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsArticles()
    {
        return $this->hasMany(NewsArticle::className(), ['author_id' => 'id']);
    }

    /**
     * @return array all status list
     */
    public static function allStatusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_DELETED => 'Удаленный',
        ];
    }

    /**
     * @return array status list
     */
    public static function statusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        ];
    }
    /**
     * @return get category status
     */
    public function currentStatus()
    {
        $data = self::allStatusList();
        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }

    public static function getListData()
    {

        $authors = self::find()->where(['status' => 2])->orderBy('nickname asc')->all();

        $list = [];

        foreach ($authors as $author) {
            $list[$author->id] = $author->firstname.' '.$author->lastname;
        }

        return $list;
    }

    public static function getOneAuthor($author_id)
    {
        $query = (new Query())
            ->select('"lastname" ||\' \' || "firstname" as full_name, nickname, created_at, description, email, avatar_path, avatar_base_url')
            ->from('user_author')
            ->where('status=2 and id=:author_id')
            ->params(['author_id' => $author_id]);
        return $query->one();
    }

    public static function nickOneAuthor($nickname)
    {
        $query = (new Query())
            ->select('"id","lastname" ||\' \' || "firstname" as full_name, nickname, created_at, description, email, avatar_path, avatar_base_url')
            ->from('user_author')
            ->where('nickname=:nickname')
            ->params(['nickname' => $nickname]);
        return $query->one();
    }

    public function behaviors()
    {

        return [
            // [
            //     'class'=>SluggableBehavior::className(),
            //     'attribute' => 'firstname',
            //     'attributes'=>['firstname', 'lastname'],
            //     'slugAttribute' => 'nickname',
            //     'immutable' => true
            // ],
            [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'avatar',
                'pathAttribute' => 'avatar_path',
                'baseUrlAttribute' => 'avatar_base_url'
            ],
        ];
    }
}
