<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payment_transaction".
 *
 * @property int $id
 * @property int $price
 * @property string $cancel_time
 * @property string $payment_time
 * @property string $perform_time
 * @property string $pay_trans_id
 * @property int $reason
 * @property string $receivers
 * @property int $order_id
 * @property string $created_at
 * @property int $status
 *
 */

class PaymentTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reason'], 'default', 'value' => null],
            [['price', 'reason', 'order_id', 'status'], 'integer'],
            [['cancel_time', 'perform_time', 'created_at'], 'safe'],
            [['receivers'], 'string'],
            [['order_id', 'price'], 'required'],
            [['payment_time'], 'string', 'max' => 20],
            [['pay_trans_id'], 'string', 'max' => 100],
            [['pay_trans_id'], 'unique'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentService::className(), 'targetAttribute' => ['order_id' => 'id']]
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'price'             => 'Цена',
            'cancel_time'       => 'Cancel Time',
            'payment_time'      => 'Payment Time',
            'perform_time'      => 'Perform Time',
            'pay_trans_id'      => 'Pay Trans ID',
            'reason'            => 'Reason',
            'receivers'         => 'Receivers',
            'order_id'          => 'Услуга',
            'created_at'        => 'Создано',
            'status'            => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(PaymentOrder::className(), ['id' => 'order_id']);
    }

    public function behaviors()
    {

        return [
            [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => false,
              'value' => date('Y-m-d H:i:s'),
            ]
        ];
    }
}
