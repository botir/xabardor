<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "feedback_message".
 *
 * @property int $id
 * @property string $subject
 * @property string $message_txt
 * @property string $phone
 * @property string $email
 * @property string $full_name
 * @property int $user_id
 * @property bool $is_mobile
 * @property string $created_at
 *
 * @property User $user
 */
class FeedbackMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject', 'message_txt', 'full_name'], 'required'],
            [['message_txt'], 'string'],
            [['user_id'], 'default', 'value' => null],
            [['user_id'], 'integer'],
            [['is_mobile'], 'boolean'],
            [['created_at'], 'safe'],
            [['subject', 'phone', 'email', 'full_name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'message_txt' => 'Message Txt',
            'phone' => 'Phone',
            'email' => 'Email',
            'full_name' => 'Full Name',
            'user_id' => 'User ID',
            'is_mobile' => 'Is Mobile',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
    */
    public function behaviors()
    {
       return [
            [
                'class'=>TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => date('Y-m-d H:i:s'),
            ]
        ];
    }
}
