<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_data".
 *
 * @property int $news_id
 * @property int $lang_id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $caption_image
 * @property string $lead_info
 * @property string $document_vector
 *
 * @property NewsArticle $news
 */
class NewsData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'lang_id', 'title', 'description', 'content'], 'required'],
            [['news_id', 'lang_id'], 'default', 'value' => null],
            [['news_id', 'lang_id'], 'integer'],
            [['content', 'document_vector'], 'string'],
            [['title'], 'string', 'max' => 405],
            [['description', 'lead_info'], 'string', 'max' => 600],
            [['caption_image'], 'string', 'max' => 150],
            [['news_id', 'lang_id'], 'unique', 'targetAttribute' => ['news_id', 'lang_id']],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewsArticle::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'caption_image' => 'Caption Image',
            'lead_info' => 'Lead Info',
            'document_vector' => 'Document Vector',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(NewsArticle::className(), ['id' => 'news_id']);
    }
}
