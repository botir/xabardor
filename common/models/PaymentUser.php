<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "payment_user".
 *
 * @property int $transaction_id
 * @property string $start_date
 * @property string $end_date
 * @property int $user_id
 *
 * @property User $user
 */
class PaymentUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_id', 'start_date', 'end_date', 'user_id'], 'required'],
            [['transaction_id', 'user_id'], 'default', 'value' => null],
            [['transaction_id', 'user_id'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['transaction_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'transaction_id' => 'Transaction ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public static function getUserPeriod($date, $user_id)
    {
        $query = (new Query())
              ->select('start_date, end_date')
              ->from('payment_user')
              ->where('user_id=:user_id and :current_date BETWEEN start_date AND end_date')
              ->params([':user_id'=>$user_id, ':current_date'=>$date])
              ->limit(1);

            return $query->one();

    }
}
