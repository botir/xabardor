<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use common\behaviors\AudioBehavior;
use Yii;

/**
 * This is the model class for table "news_audio".
 *
 * @property int $id
 * @property int $lang_id
 * @property int $code
 * @property array $content
 * @property string $audio_path
 * @property string $base_url
 * @property string $photo_path
 * @property string $photo_base_url
 * @property int $status
 * @property int $user_id
 * @property int $author_id
 * @property int $listen_count
 * @property string $duration
 * @property string $published_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property UserAuthor $author
 */
class NewsAudio extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;

    public $audio_file;
    public $photo_file;


    public $title;
    public $title_uz;
    public $title_kr;
    public $title_ru;
    public $title_en;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_audio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lang_id', 'published_at', 'audio_file', 'photo_file'], 'required'],
            [['title_uz', 'title_kr', 'title_ru', 'title_en'], 'required'],
            [['lang_id', 'code', 'status', 'user_id', 'author_id', 'listen_count'], 'default', 'value' => null],
            [['lang_id', 'code', 'status', 'user_id', 'author_id', 'listen_count'], 'integer'],
            [['content', 'published_at', 'created_at', 'updated_at'], 'safe'],
            [['audio_path', 'base_url', 'photo_path', 'photo_base_url', 'duration'], 'string', 'max' => 1024],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserAuthor::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang_id' => 'Язык',
            'code' => 'код',
            'title' => 'Заголовок',
            'title_en' => 'Заголовок (en)',
            'title_ru' => 'Заголовок (ru)',
            'title_kr' => 'Заголовок (kr)',
            'title_uz' => 'Заголовок (uz)',
            'content' => 'Content',
            'audio_path' => 'Audio Path',
            'base_url' => 'Base Url',
            'photo_path' => 'Photo Path',
            'photo_base_url' => 'Photo Base Url',
            'status' => 'Статус',
            'user_id' => 'Создатель',
            'author_id' => 'Автор',
            'listen_count' => 'Listen Count',
            'duration' => 'Duration',
            'published_at' => 'Публиковать',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'photo_file' => 'Изображение',
            'audio_file' => 'Аудиофайл',
        ];
    }

     /**
     * @return array all status list
     */
    public static function allStatusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_DELETED => 'Удаленный',
        ];
    }

    /**
     * @return array status list
     */
    public static function statusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        ];
    }

    /**
     * @return get category status
     */
    public function currentStatus()
    {
        $data = self::allStatusList();
        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(UserAuthor::className(), ['id' => 'author_id']);
    }

    public function getLanguageСode()
    {
        switch($this->lang_id){
            case 3: $code = 'ru';break;
            case 4: $code = 'en'; break;
            default: $code = 'uz';
        }
        return $code;
    }

    public function behaviors()
    {

        return [
            [
                'class'=>AudioBehavior::className()
            ],
            [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
            ],
            [
              'class'=>BlameableBehavior::className(),
              'createdByAttribute' => 'user_id',
              'updatedByAttribute' => false,

          ],
          [
              'class' => UploadBehavior::className(),
              'attribute' => 'audio_file',
              'pathAttribute' => 'audio_path',
              'baseUrlAttribute' => 'base_url'
          ],
          [
              'class' => UploadBehavior::className(),
              'attribute' => 'photo_file',
              'pathAttribute' => 'photo_path',
              'baseUrlAttribute' => 'photo_base_url'
          ]
        ];
    }

    public function getPublishDate($type=1){

        $time = strtotime($this->published_at);
        $now = date('Y-m-d');
        if ($now==date('Y-m-d',$time)){
            return date("H:i",$time);
        }else{
          return date("H:i / d.m.Y",$time);
        }

    }
}
