<?php

namespace common\models;

use Yii;
use yii\db\Query;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payment_order".
 *
 * @property int $id
 * @property int $service_id
 * @property int $status
 * @property int $user_id
 * @property int $price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PaymentService $service
 * @property User $user
 * @property PaymentTransaction[] $paymentTransactions
 */
class PaymentOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'user_id'], 'required'],
            [['service_id', 'status', 'user_id', 'price'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentService::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'status' => 'Status',
            'user_id' => 'User ID',
            'price' => 'Price',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(PaymentService::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTransactions()
    {
        return $this->hasMany(PaymentTransaction::className(), ['order_id' => 'id']);
    }

    public function behaviors()
    {

        return [
            [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
            ]        
        ];
    }

    public static function getUserHistory($user_id, $limit=20)
    {
        $query = (new Query())
              ->select('payment_order.price, (payment_order.updated_at)::timestamp as payment_date, payment_service.title as service_title')
              ->from('payment_order')
              ->innerJoin('payment_service','payment_order.service_id=payment_service.id')
              ->where('payment_order.status=2 and payment_order.user_id=:user_id')
              ->params([':user_id'=>$user_id])
              ->orderBy(['payment_order.updated_at'=>SORT_DESC])
              ->limit($limit);
            return $query->all();
    }
}
