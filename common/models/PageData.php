<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_data".
 *
 * @property int $page_id
 * @property int $lang_id
 * @property string $title
 * @property string $content
 *
 * @property PageStatic $page
 */
class PageData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'lang_id', 'title', 'content'], 'required'],
            [['page_id', 'lang_id'], 'default', 'value' => null],
            [['page_id', 'lang_id'], 'integer'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 405],
            [['page_id', 'lang_id'], 'unique', 'targetAttribute' => ['page_id', 'lang_id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => PageStatic::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'Page ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(PageStatic::className(), ['id' => 'page_id']);
    }
}
