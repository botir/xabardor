<?php

namespace common\models;

use Yii;
use yii\db\Query;
use yii\behaviors\TimestampBehavior;
use common\behaviors\ServiceBehaviors;

/**
 * This is the model class for table "payment_service".
 *
 * @property int $id
 * @property string $uniq_name
 * @property string $title
 * @property int $status
 * @property int $price
 * @property bool $front_active
 * @property array $more_option
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PaymentTransaction[] $paymentTransactions
 */
class PaymentService extends \yii\db\ActiveRecord
{

    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;

    public $options = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uniq_name', 'title', 'status'], 'required'],
            //[['status', 'price'], 'default', 'value' => null],
            [['status', 'price'], 'integer'],
            [['front_active'], 'boolean'],
            [['more_option', 'created_at', 'updated_at', 'options'], 'safe'],
            [['uniq_name'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 255],
            [['uniq_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'uniq_name'         => 'id',
            'title'             => 'Название',
            'status'            => 'Статус',
            'price'             => 'Цена',
            'front_active'      => 'Показать',
            'more_option'       => 'Опции',
            'options'           => 'Опции',
            'created_at'        => 'Создано',
            'updated_at'        => 'Последнее обновление',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTransactions()
    {
        return $this->hasMany(PaymentTransaction::className(), ['service_id' => 'id']);
    }

    /**
     * @return array all status list
     */
    public static function allStatusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_DELETED => 'Удаленный',
        ];
    }

    /**
     * @return array status list
     */
    public static function statusList()
    {
        return [
            self::STATUS_DRAFT => 'Не активно',
            self::STATUS_ACTIVE => 'Активно'
        ];
    }

    /**
     * @return get payment service status
     */
    public function currentStatus()
    {
        $data = self::allStatusList();
        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }

    public function behaviors()
    {

        return [
            [
              'class'=>ServiceBehaviors::className()
            ],
            [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
            ]        
        ];
    }


    public static function listPlans($limit=2)
    {
        $query = (new Query())
            ->select('uniq_name, title, price, more_option')
            ->from('payment_service')
            ->where('status=2 and front_active is true')
            ->orderBy(['price'=>SORT_ASC])
            ->limit($limit);
        return $query->all();
    }

    public static function getByName($name)
    {
        $query = (new Query())
            ->select('id, uniq_name, title, price, more_option')
            ->from('payment_service')
            ->where('status=2 and uniq_name=:name')
            ->params([':name'=>$name]);
        return $query->one();
    }
}
