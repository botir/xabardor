<?php
namespace common\components\getid3;
use yii\base\BaseObject;
include("getid3/getid3.php");
class Yii2Getid3 extends BaseObject
{
    public $filename;
	
    public function getData($filename)
    {
		
        $this->filename = $filename;
        try {
        	$getID3 = new \getID3;
        	$file = $getID3->analyze($filename);
       		$result=@$file['playtime_string'];
       	}
       	catch (\Exception $e) {
         $result=null;
        }
       return $result;
    }
    
}