<?php

namespace  common\components\detect;

use Yii;
use Detection\MobileDetect;


class DeviceDetect extends \yii\base\Component {

	private $_mobileDetect;

	// Automatically set view parameters based on device type
	public $setParams = true;

	public function __call($name, $parameters) {
		return call_user_func_array(
			array($this->_mobileDetect, $name),
			$parameters
		);
	}

	public function __construct($config = array()) {
		parent::__construct($config);
	}

	public function init() {
		$this->_mobileDetect = new MobileDetect();
		parent::init();
        \Yii::$app->params['devicedetect'] = [
            'isMobile' => $this->_mobileDetect->isMobile(),
            'isTablet' => $this->_mobileDetect->isTablet()
        ];

        \Yii::$app->params['devicedetect']['isDesktop'] =
            !\Yii::$app->params['devicedetect']['isMobile'] &&
            !\Yii::$app->params['devicedetect']['isTablet'];


	}
	public function ae_detect_ie($user_agent)
	{
		if ($user_agent){

			if (stripos($user_agent, 'opera mini') != false || stripos($user_agent, 'ucmini') != false || stripos($user_agent, 'msie') != false){
				return true;
			}else{
				return false;
			}

		}else{
			return true;
		}
	}


	protected function isOperaMini($agent)
    {
        if (stripos($agent, 'opera mini') != false) {
            return true;
        } else {
        	return false;	
        }
    }

}
