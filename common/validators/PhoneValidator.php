<?php
namespace common\validators;
use yii\validators\Validator;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\NumberParseException;
use Exception;


class PhoneValidator extends Validator
{
    public $strict = true;
    public $countryAttribute;
    public $country;
    public $format = true;
    public function validateAttribute($model, $attribute)
    {
        if ($this->format === true) {
            $this->format = PhoneNumberFormat::INTERNATIONAL;
        }
        // if countryAttribute is set
        if (!isset($country) && isset($this->countryAttribute)) {
            $countryAttribute = $this->countryAttribute;
            $country = $model->$countryAttribute;
        }
        // if country is fixed
        if (!isset($country) && isset($this->country)) {
            $country = $this->country;
        }
        // if none select from our models with best effort
        if (!isset($country) && isset($model->country_code))
            $country = $model->country_code;
        if (!isset($country) && isset($model->country))
            $country = $model->country;
        // if none and strict
        if (!isset($country) && $this->strict) {
            $this->addError($model, $attribute, \Yii::t('common', 'uncorrect phone'));
            return false;
        }
        if (!isset($country)) {
            return true;
        }
        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $numberProto = $phoneUtil->parse($model->$attribute, $country);
            $n_number = $numberProto->getNationalNumber();
            if (strlen($n_number)==strlen($model->$attribute)){
                $numberProto = $phoneUtil->parse('+'.$model->$attribute, $country);
            }
            if ($phoneUtil->isValidNumber($numberProto)) {
                if (is_numeric($this->format)) {
                    $model->$attribute = $phoneUtil->format($numberProto, $this->format);
                }
                return true;
            } else {
                $this->addError($model, $attribute, \Yii::t('common', 'uncorrect phone'));
                return false;
            }
        } catch (NumberParseException $e) {
            $this->addError($model, $attribute, \Yii::t('common', 'uncorrect phone'));
        } catch (Exception $e) {
            $this->addError($model, $attribute, \Yii::t('common', 'uncorrect phone'));
        }
    }
}