<?php

use yii\db\Migration;
use yii\db\Query;
use common\commands\SaveNewsContent;
/**
 * Class m191120_070932_export_data_news_data
 */
class m191120_070932_export_data_news_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $start_date = '2000-01-01';
        $this->execute('truncate news_content');
        while(true){
            $news = $this->getNewsList($start_date,500);
            echo $start_date.'='.count($news);
            echo '***';
            if (!$news) break;
            
            $t = new SaveNewsContent(['news_list'=>$news]);
            $t->news_list = $news;
            $t->execute(1);
            //break;
            $start_date = $news[count($news)-1]['created_at'];
        }

    }

    protected function getNewsList($from, $limit){

        $query = (new Query())
            ->select('*')
            ->from('news_article')
            ->where('news_article.created_at>:from')
            ->orderBy('news_article.created_at asc')
            ->limit($limit)
            ->params([
             ':from' =>$from
           ]);
        return $query->all();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191120_070932_export_data_news_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191120_070932_export_data_news_data cannot be reverted.\n";

        return false;
    }
    */
}
