<?php

use yii\db\Migration;

/**
 * Class m190108_090128_create_table_news
 */
class m190108_090128_create_table_news extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE news_category (
                id serial NOT NULL,
                slug varchar(150) NOT NULL,
                title varchar(150) NOT NULL,
                sort int4 NOT NULL,
                status int2 NOT NULL DEFAULT 2,
                CONSTRAINT news_category_pkey PRIMARY KEY (id),
                CONSTRAINT news_category_slug_uniq UNIQUE (slug)
            )
        ');

        $this->execute('
            CREATE TABLE news_tag (
                id serial NOT NULL,
                slug varchar(150) NOT NULL,
                title varchar(150) NOT NULL,
                sort int4 NOT NULL,
                status int2 NOT NULL DEFAULT 2,
                CONSTRAINT news_tag_pkey PRIMARY KEY (id),
                CONSTRAINT news_tag_slug_uniq UNIQUE (slug, title)
            )
        ');

        $this->execute('
            CREATE TABLE news_article (
                id serial NOT NULL,
                title varchar(510) NOT NULL,
                description varchar(600) NOT NULL,
                content text NOT NULL,
                open_content text NULL,
                category_id int4 NULL,
                tab_number int4 NULL,
                image_path varchar(510) NULL,
                image_base_url varchar(510) NULL,
                content_type int2 NOT NULL DEFAULT 1,
                is_actual bool NOT NULL DEFAULT false,
                is_selected bool NOT NULL DEFAULT false,
                is_ads bool NOT NULL DEFAULT false,
                is_premium bool NOT NULL DEFAULT false,
                views_count int4 NOT NULL DEFAULT 0,
                status int2 NOT NULL DEFAULT 2,
                published_at timestamptz NOT NULL,
                created_at timestamptz NOT NULL,
                updated_at timestamptz NOT NULL,
                more_option jsonb NOT NULL DEFAULT \'{"tags": [], "show_desc": true, "font_style": "normal", "list_image": true, "show_image": true, "caption_image": "", "is_allowcoment": false}\'::jsonb,
                slug varchar(150) NOT NULL,
                author_id int4 NULL,
                user_id int4 NULL,
                CONSTRAINT news_article_pkey PRIMARY KEY (id),
                CONSTRAINT news_article_slug_uniq UNIQUE (slug),
                CONSTRAINT news_article_tab_number_uniq UNIQUE (tab_number),
                FOREIGN KEY (category_id) REFERENCES "news_category"(id) ON DELETE SET NULL,
                FOREIGN KEY (author_id) REFERENCES "user_author"(id) ON DELETE SET NULL,
                FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE SET NULL
            )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190108_090128_create_table_news cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190108_090128_create_table_news cannot be reverted.\n";

        return false;
    }
    */
}
