<?php

use yii\db\Migration;

/**
 * Class m190220_051701_alter_table_add_column
 */
class m190220_051701_alter_table_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_article ADD COLUMN "comment_count" int4 NOT NULL DEFAULT 0');
        $this->execute('
        CREATE FUNCTION article_comment_count() RETURNS trigger AS $$
    DECLARE
        fk_value integer;
        record record;
    BEGIN
        IF TG_OP = \'UPDATE\' THEN
            record := NEW;
            IF NEW.status = 2 THEN
                IF OLD.status !=2 THEN
                    fk_value := 1;
                ELSE
                    fk_value := 0;
                END IF;
            ELSE
                IF OLD.status =2 THEN
                    fk_value := -1;
                ELSE
                    fk_value := 0;
                END IF;
            END IF;
        END IF;

        IF TG_OP = \'DELETE\' THEN
            record := OLD;
            IF OLD.status =2 THEN
                fk_value := -1;
            ELSE
                fk_value := 0;
            END IF;
        END IF;

        IF TG_OP = \'INSERT\' THEN
            record := NEW;
            IF NEW.status = 2 THEN
                fk_value := 1;
            ELSE
                fk_value := 0;
            END IF;
        END IF;

        IF fk_value !=0 THEN
            UPDATE news_article SET comment_count = comment_count + fk_value where id = record.article_id;
        END IF;
      RETURN record;
    END;
$$ LANGUAGE plpgsql;');
        $this->execute('CREATE TRIGGER update_news_article_comments_count  
  AFTER INSERT OR UPDATE OR DELETE ON news_comment
  FOR EACH ROW EXECUTE PROCEDURE article_comment_count();');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190220_051701_alter_table_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190220_051701_alter_table_add_column cannot be reverted.\n";

        return false;
    }
    */
}
