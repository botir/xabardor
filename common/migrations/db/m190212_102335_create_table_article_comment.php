<?php

use yii\db\Migration;

/**
 * Class m190212_102335_create_table_article_comment
 */
class m190212_102335_create_table_article_comment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE news_comment (
                id  bigserial NOT NULL,
                parent_id int8 NULL DEFAULT NULL,
                article_id int4 NOT NULL,
                user_id int4 NOT NULL,
                content text NOT NULL,
                status int2 NOT NULL DEFAULT 2,
                created_at timestamptz NOT NULL,
                updated_at timestamptz NOT NULL,
                CONSTRAINT news_comment_pkey PRIMARY KEY (id),
                FOREIGN KEY (parent_id) REFERENCES "news_comment"(id) ON UPDATE CASCADE ON DELETE CASCADE,
                FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
                FOREIGN KEY (article_id) REFERENCES "news_article"(id) ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190212_102335_create_table_article_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190212_102335_create_table_article_comment cannot be reverted.\n";

        return false;
    }
    */
}
