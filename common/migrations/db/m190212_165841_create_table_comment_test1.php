<?php

use yii\db\Migration;

/**
 * Class m190212_165841_create_table_comment_test1
 */
class m190212_165841_create_table_comment_test1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('DROP TABLE news_comment');
        $this->execute('
            CREATE TABLE news_comment (
                id  bigserial NOT NULL,
                parent_id int8 NULL DEFAULT NULL,
                position INTEGER,
                parent_path LTREE,
                article_id int4 NOT NULL,
                user_id int4 NOT NULL,
                content text NOT NULL,
                status int2 NOT NULL DEFAULT 2,
                created_at timestamptz NOT NULL,
                updated_at timestamptz NOT NULL,
                CONSTRAINT news_comment_pkey PRIMARY KEY (id),
                FOREIGN KEY (parent_id) REFERENCES "news_comment"(id) ON UPDATE CASCADE ON DELETE CASCADE,
                FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
                FOREIGN KEY (article_id) REFERENCES "news_article"(id) ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');
        $this->execute('CREATE INDEX news_comment_path_idx ON news_comment USING GIST (parent_path);');
        $this->execute('CREATE INDEX news_comment_parent_idx ON news_comment (parent_id);');
        $this->execute("CREATE OR REPLACE FUNCTION update_news_comment_parent_path() RETURNS TRIGGER AS $$
    DECLARE
        path ltree;
    BEGIN
        IF NEW.parent_id IS NULL THEN
            NEW.parent_path = 'root'::ltree;
        ELSEIF TG_OP = 'INSERT' OR OLD.parent_id IS NULL OR OLD.parent_id != NEW.parent_id THEN
            SELECT parent_path || id::text FROM news_comment WHERE id = NEW.parent_id INTO path;
            IF path IS NULL THEN
                RAISE EXCEPTION 'Invalid parent_id %', NEW.parent_id;
            END IF;
            NEW.parent_path = path;
        END IF;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;");
         $this->execute("CREATE TRIGGER parent_path_tgr
    BEFORE INSERT OR UPDATE ON news_comment
    FOR EACH ROW EXECUTE PROCEDURE update_news_comment_parent_path();");

//         $this->execute("CREATE OR REPLACE FUNCTION json_child_tree(parent text) RETURNS json AS 
// $
//     var rows = plv8.execute('SELECT * FROM news_comment2 WHERE parent_path <@ $1 ORDER BY nlevel(parent_path),position', [parent]);
//     var all = {},
//     out = [],
//     top,r,i;
//     for(i=0; i<rows.length; i++){
//         r = rows[i];
//         r.children = [];
//         all[r.id] = r;
//         if(r.parent_path == parent){
//             out.push(r);
//         }
//     }
//     for(i=0; i<rows.length; i++){
//         r = rows[i];
//         if(all[ r.parent_id ]){
//             all[ r.parent_id ].children.push(r);
//         }
//     }
//     return JSON.stringify(out,null,4);
// $
// LANGUAGE plv8;");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190212_165841_create_table_comment_test1 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190212_165841_create_table_comment_test1 cannot be reverted.\n";

        return false;
    }
    */
}
