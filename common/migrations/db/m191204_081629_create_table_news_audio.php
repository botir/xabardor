<?php

use yii\db\Migration;

/**
 * Class m191204_081629_create_table_news_audio
 */
class m191204_081629_create_table_news_audio extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
  CREATE TABLE "news_audio"
  (
    id serial NOT NULL,
    lang_id int2 NOT NULL,
    code int4 NOT NULL,
    "content" JSONB NOT NULL DEFAULT \'{"uz": "", "kr": "", "ru": "", "en": ""}\'::jsonb,
    audio_path varchar(1024) NULL,
    base_url varchar(1024) NULL,
    photo_path varchar(1024) NULL,
    photo_base_url varchar(1024) NULL,
    status int2 NOT NULL DEFAULT 2,
    user_id int4 NULL,
    author_id int4 NULL,
    listen_count int4 DEFAULT 0 NOT NULL,
    duration varchar(1024) NULL,
    published_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NULL,
    updated_at TIMESTAMP WITHOUT TIME ZONE NULL,
    CONSTRAINT news_audioid_pkey PRIMARY KEY (id),
    FOREIGN KEY (author_id) REFERENCES "user_author"(id) ON DELETE SET NULL,
    FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE SET NULL
  )
          ');
        $this->execute('CREATE INDEX ix_news_audio_published ON news_audio(status, published_at);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191204_081629_create_table_news_audio cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191204_081629_create_table_news_audio cannot be reverted.\n";

        return false;
    }
    */
}
