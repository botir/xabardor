<?php

use yii\db\Migration;

/**
 * Class m190617_102029_article_option_data_update
 */
class m190617_102029_article_option_data_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('update news_article  set "more_option" = jsonb_set("more_option", \'{"show_image"}\', \'false\'::jsonb) ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190617_102029_article_option_data_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190617_102029_article_option_data_update cannot be reverted.\n";

        return false;
    }
    */
}
