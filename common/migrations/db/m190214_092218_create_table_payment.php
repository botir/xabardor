<?php

use yii\db\Migration;
use yii\db\Expression;

/**
 * Class m190214_092218_create_table_payment
 */
class m190214_092218_create_table_payment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE payment_service (
                id serial NOT NULL,
                uniq_name varchar(100) NOT NULL,
                title varchar(255) NOT NULL,
                status int2 NOT NULL DEFAULT 2,
                price int4 NULL,
                front_active bool NOT NULL DEFAULT false,
                more_option jsonb NOT NULL DEFAULT \'[]\'::jsonb,
                created_at timestamptz NOT NULL,
                updated_at timestamptz NOT NULL,
                CONSTRAINT payment_service_pkey PRIMARY KEY (id),
                CONSTRAINT payment_service_uniqname UNIQUE (uniq_name)
            )
        ');

        $this->execute('
            CREATE TABLE payment_order (
                id  bigserial NOT NULL,
                service_id int4 NOT NULL,
                status int2 NOT NULL DEFAULT 1,
                user_id int4 NOT NULL,
                price int4 NULL,
                created_at timestamptz NOT NULL,
                updated_at timestamptz NOT NULL,
                CONSTRAINT payment_order_pkey PRIMARY KEY (id),
                FOREIGN KEY (service_id) REFERENCES payment_service(id) DEFERRABLE INITIALLY DEFERRED,
                FOREIGN KEY (user_id) REFERENCES "user"(id) DEFERRABLE INITIALLY DEFERRED
            )
        ');

        $this->execute('
            CREATE TABLE payment_transaction (
                id  bigserial NOT NULL,
                price int4 NOT NULL,
                agent_type varchar(10) NULL,
                cancel_time timestamptz NULL,
                payment_time varchar(20) NULL,
                perform_time timestamptz NULL,
                pay_trans_id varchar(100) NULL,
                reason int2 NULL,
                receivers text NULL,
                order_id int4 NOT NULL,
                created_at timestamptz NOT NULL,
                status int2 NOT NULL DEFAULT 1,
                CONSTRAINT payment_transaction_pkey PRIMARY KEY (id),
                CONSTRAINT payment_transaction_pay_trans_id UNIQUE (pay_trans_id),
                FOREIGN KEY (order_id) REFERENCES payment_order(id) DEFERRABLE INITIALLY DEFERRED
            )
        ');

        $this->execute('
            CREATE TABLE payment_user (
                transaction_id int8 NOT NULL,
                start_date date NOT NULL,
                end_date date NOT NULL,
                user_id int4 NOT NULL,
                CONSTRAINT payment_user_pkey PRIMARY KEY (transaction_id),
                FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');

         $this->insert('{{%payment_service}}', [
            'id' => 1,
            'uniq_name' => 'monthly',
            'title' => 'Oylik obuna',
            'status' => 2,
            'price' => 20000,
            'front_active' => true,
            'more_option' => [
                        ["title"=> "Har qanday qurilmadan saytdagi materiallarga kirish imkoniyati"], 
                        ["title"=> "Lenta bo’limida eng so’ngi va dolzarb maqolalar"], 
                        ["title"=> "Kommentlar qoldirish imkoniyati"], 
                        ["title"=> "Yopiq materiallarni do’stlar bilan bo’lishish imkoniyati"]
                    ],
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()')
        ]);

        $this->insert('{{%payment_service}}', [
            'id' => 2,
            'uniq_name' => '3-monthly',
            'title' => '3 oylik obuna',
            'status' => 2,
            'price' => 50000,
            'front_active' => true,
            'more_option' => [
                ["title"=> "Har qanday qurilmadan saytdagi materiallarga kirish imkoniyati"], 
                ["title"=> "Lenta bo’limida eng so’ngi va dolzarb maqolalar"], 
                ["title"=> "Kommentlar qoldirish imkoniyati"], 
                ["title"=> "Yopiq materiallarni do’stlar bilan bo’lishish imkoniyati"]
            ],            
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190214_092218_create_table_payment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_092218_create_table_payment cannot be reverted.\n";

        return false;
    }
    */
}
