<?php

use yii\db\Migration;

/**
 * Class m191204_071918_add_column_news_article
 */
class m191204_071918_add_column_news_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_article ADD COLUMN "day_photo" bool NOT NULL DEFAULT false');
        $this->execute('CREATE INDEX ix_news_article_day_photo ON news_article(status, published_at, day_photo);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191204_071918_add_column_news_article cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191204_071918_add_column_news_article cannot be reverted.\n";

        return false;
    }
    */
}
