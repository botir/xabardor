<?php

use yii\db\Migration;

/**
 * Class m190122_083830_add_column_news_tags
 */
class m190122_083830_add_column_news_tags extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_tag ADD COLUMN "c_weight" int4 NOT NULL DEFAULT 0;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190122_083830_add_column_news_tags cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190122_083830_add_column_news_tags cannot be reverted.\n";

        return false;
    }
    */
}
