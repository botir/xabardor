<?php

use yii\db\Migration;

/**
 * Class m190129_103144_create_table_page
 */
class m190129_103144_create_table_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE page_static (
                id serial NOT NULL,
                title varchar(510) NOT NULL,
                content text NOT NULL,
                content_type int2 NOT NULL DEFAULT 1,
                views_count int4 NOT NULL DEFAULT 0,
                is_active bool NOT NULL DEFAULT true,
                is_menu bool NOT NULL DEFAULT false,
                slug varchar(150) NOT NULL,
                view_file varchar(255) NULL DEFAULT NULL,
                user_id int4 NULL,
                created_at timestamptz NOT NULL,
                updated_at timestamptz NOT NULL,
                CONSTRAINT page_static_pkey PRIMARY KEY (id),
                CONSTRAINT page_static_slug_uniq UNIQUE (content_type, slug),
                FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE SET NULL
            )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190129_103144_create_table_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190129_103144_create_table_page cannot be reverted.\n";

        return false;
    }
    */
}
