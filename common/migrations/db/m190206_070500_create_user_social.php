<?php

use yii\db\Migration;

/**
 * Class m190206_070500_create_user_social
 */
class m190206_070500_create_user_social extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE user_social (
                id serial NOT NULL,
                oauth_client varchar(255) NOT NULL,
                oauth_client_user_id varchar(255) NOT NULL,
                user_id int4 NOT NULL,
                CONSTRAINT user_social_pkey PRIMARY KEY (id),
                CONSTRAINT user_social_uniq UNIQUE (oauth_client, user_id),
                FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');
        $this->execute('CREATE INDEX idx_user_social_client ON user_social USING btree(oauth_client, oauth_client_user_id);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190206_070500_create_user_social cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190206_070500_create_user_social cannot be reverted.\n";

        return false;
    }
    */
}
