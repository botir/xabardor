<?php

use yii\db\Migration;

/**
 * Class m190612_072840_create_index_news_article
 */
class m190612_072840_create_index_news_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE INDEX ix_news_article_content_type ON news_article(status, content_type, published_at);');
        $this->execute('CREATE INDEX ix_news_article_latest ON news_article(status, published_at);');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190612_072840_create_index_news_article cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190612_072840_create_index_news_article cannot be reverted.\n";

        return false;
    }
    */
}
