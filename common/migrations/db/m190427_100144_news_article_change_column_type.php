<?php

use yii\db\Migration;

/**
 * Class m190427_100144_news_article_change_column_type
 */
class m190427_100144_news_article_change_column_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_article ALTER COLUMN published_at TYPE timestamp USING published_at::timestamp;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190427_100144_news_article_change_column_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190427_100144_news_article_change_column_type cannot be reverted.\n";

        return false;
    }
    */
}
