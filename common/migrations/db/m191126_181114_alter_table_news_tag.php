<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\Json;


/**
 * Class m191126_181114_alter_table_news_tag
 */
class m191126_181114_alter_table_news_tag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_tag 
            ADD COLUMN "content" JSONB NOT NULL DEFAULT \'{"uz": "", "kr": "", "ru": "", "en": ""}\'::jsonb;');

        $tags = $this->getTagList();
        foreach ($tags as $data) {
            $content['uz'] = $data['title'];
            $content['kr'] = $data['title'];
            $content['ru'] = '';
            $content['en'] = '';
            Yii::$app->db->createCommand('update news_tag  
SET content = :js
where "id" = '.$data['id'])->bindValue(':js', Json::encode($content))
            ->execute();
        }

    }

    protected function getTagList(){

        $query = (new Query())
            ->select('*')
            ->from('news_tag')
            ->orderBy('news_tag.id asc')
            ->limit(500);
        return $query->all();
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191126_181114_alter_table_news_tag cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191126_181114_alter_table_news_tag cannot be reverted.\n";

        return false;
    }
    */
}
