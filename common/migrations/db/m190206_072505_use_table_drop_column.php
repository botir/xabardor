<?php

use yii\db\Migration;

/**
 * Class m190206_072505_use_table_drop_column
 */
class m190206_072505_use_table_drop_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user}}', 'oauth_client');
        $this->dropColumn('{{%user}}', 'oauth_client_user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190206_072505_use_table_drop_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190206_072505_use_table_drop_column cannot be reverted.\n";

        return false;
    }
    */
}
