<?php

use yii\db\Migration;

/**
 * Class m190108_085509_create_table_authors
 */
class m190108_085509_create_table_authors extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE {{%user_author}}
            (
                id serial NOT NULL,
                nickname varchar(32) NOT NULL,
                email varchar(255) NULL DEFAULT NULL,
                firstname varchar(255) NOT NULL,
                middlename varchar(255) NULL,
                lastname varchar(255) NOT NULL,
                description varchar(510) NULL,
                avatar_path varchar(510) NULL,
                avatar_base_url varchar(510) NULL,
                status int2 NOT NULL DEFAULT 2,
                created_at timestamptz NOT NULL,
                updated_at timestamptz NOT NULL,
                CONSTRAINT user_author_pkey PRIMARY KEY (id),
                CONSTRAINT user_author_nickname_uniq UNIQUE (nickname),
                CONSTRAINT user_author_email_uniq UNIQUE (email)
            )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_author}}');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190108_085509_create_table_authors cannot be reverted.\n";

        return false;
    }
    */
}
