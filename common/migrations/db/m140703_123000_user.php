<?php

use common\models\User;
use yii\db\Migration;

class m140703_123000_user extends Migration
{
    /**
     * @return bool|void
     */
    public function safeUp()
    {

        $this->execute('
            CREATE TABLE {{%user}}
            (
                id serial NOT NULL,
                tab_number int4 NOT NULL,
                username varchar(32) NOT NULL,
                email varchar(255) NULL DEFAULT NULL,
                auth_key varchar(32) NOT NULL,
                password_hash varchar(255) NOT NULL,
                oauth_client varchar(255) NULL,
                oauth_client_user_id varchar(255) NULL,
                status int2 NOT NULL DEFAULT 2,
                created_at timestamptz NOT NULL,
                updated_at timestamptz NOT NULL,
                logged_at timestamptz NULL,
                CONSTRAINT user_pkey PRIMARY KEY (id),
                CONSTRAINT user_tab_number_uniq UNIQUE (tab_number),
                CONSTRAINT user_username_uniq UNIQUE (username),
                CONSTRAINT user_email_uniq UNIQUE (email)
            )
        ');

        $this->createTable('{{%user_profile}}', [
            'user_id' => $this->primaryKey(),
            'firstname' => $this->string(),
            'middlename' => $this->string(),
            'lastname' => $this->string(),
            'avatar_path' => $this->string(),
            'avatar_base_url' => $this->string(),
            'locale' => $this->string(32)->notNull(),
            'gender' => $this->smallInteger(1)
        ]);

        $this->addForeignKey('fk_user', '{{%user_profile}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');

    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user', '{{%user_profile}}');
        $this->dropTable('{{%user_profile}}');
        $this->dropTable('{{%user}}');

    }
}
