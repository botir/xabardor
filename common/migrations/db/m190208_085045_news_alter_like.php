<?php

use yii\db\Migration;

/**
 * Class m190208_085045_news_alter_like
 */
class m190208_085045_news_alter_like extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_article ALTER COLUMN more_option SET DEFAULT \'{"tags": [], "show_desc": true, "font_style": "normal", "list_image": true, "show_image": true, "caption_image": "", "is_allowcoment": false, "like_count":0}\'::jsonb');
        $this->execute('update news_article  SET more_option = more_option ||\'{"like_count":0}\'::jsonb');

        $this->execute('
            CREATE TABLE news_like (
                news_id int4 NOT NULL,
                user_id int4 NOT NULL,
                created_at timestamptz NOT NULL,
                CONSTRAINT news_like_pkey PRIMARY KEY (news_id, user_id),
                FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
                FOREIGN KEY (news_id) REFERENCES "news_article"(id) ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190208_085045_news_alter_like cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190208_085045_news_alter_like cannot be reverted.\n";

        return false;
    }
    */
}
