<?php

use yii\db\Migration;

/**
 * Class m190128_125216_create_table_article_content
 */
class m190128_125216_create_table_article_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE "news_content"
            (   
                article_id int4 NOT NULL,
                document_vector tsvector NULL,
                PRIMARY KEY (article_id),
                CONSTRAINT fknews_content_article_id FOREIGN KEY (article_id)
                    REFERENCES "news_article" (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');

        $this->execute('CREATE INDEX ix_news_content_search_vector ON public.news_content USING gin (document_vector);');

        $this->execute('
        CREATE OR REPLACE FUNCTION news_document_vector_update() RETURNS TRIGGER AS $$
            BEGIN
          IF(OLD.title<>NEW.title or OLD.description<>NEW.description or OLD.content<>NEW.content) THEN

            if (select(EXISTS(SELECT 1 FROM public.news_content a WHERE a.article_id = OLD.id))) THEN
              UPDATE public.news_content SET document_vector = (to_tsvector(NEW.title) || to_tsvector(NEW.description) || to_tsvector(NEW.content)) WHERE article_id=OLD.id;
            ELSE
              INSERT INTO public.news_content (article_id, document_vector)
                VALUES (NEW.id, (to_tsvector(NEW.title) || to_tsvector(NEW.description) || to_tsvector(NEW.content)));
            end if;
          end if;
          RETURN NEW;
        END;
      $$ LANGUAGE plpgsql;');
      $this->execute('
      CREATE TRIGGER news_content_vector_changed
        BEFORE INSERT OR UPDATE ON news_article
        FOR EACH ROW
        EXECUTE PROCEDURE news_document_vector_update();');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190128_125216_create_table_article_content cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190128_125216_create_table_article_content cannot be reverted.\n";

        return false;
    }
    */
}
