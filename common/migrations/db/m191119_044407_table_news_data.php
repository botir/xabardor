<?php

use yii\db\Migration;

/**
 * Class m191119_044407_table_news_data
 */
class m191119_044407_table_news_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE "news_data"
            (   
                news_id int4 NOT NULL,
                lang_id int2 NOT NULL,
                title varchar(405) NOT NULL,
                description varchar(600) NOT NULL,
                content text NOT NULL,
                caption_image varchar(150) NULL,
                lead_info varchar(600) NULL,
                document_vector tsvector NULL,
                PRIMARY KEY (news_id, lang_id),
                CONSTRAINT fknews_news_data_id FOREIGN KEY (news_id)
                    REFERENCES "news_article" (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');

        $this->execute('CREATE INDEX ix_news_data_search_vector ON news_data USING gin (document_vector);');

        $this->execute('
        CREATE OR REPLACE FUNCTION news_data_vector_update() RETURNS TRIGGER AS $$
            BEGIN
            NEW.document_vector := to_tsvector(NEW.title) || to_tsvector(NEW.description) || to_tsvector(NEW.content);
          RETURN NEW;
        END;
      $$ LANGUAGE plpgsql;');
        $this->execute('
      CREATE TRIGGER news_data_vector_changed
        BEFORE INSERT OR UPDATE ON news_data
        FOR EACH ROW
        EXECUTE PROCEDURE news_data_vector_update();');



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191119_044407_table_news_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191119_044407_table_news_data cannot be reverted.\n";

        return false;
    }
    */
}
