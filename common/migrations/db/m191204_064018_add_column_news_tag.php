<?php

use yii\db\Migration;

/**
 * Class m191204_064018_add_column_news_tag
 */
class m191204_064018_add_column_news_tag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {   
        $this->execute('ALTER TABLE news_tag ADD COLUMN "is_main" bool NOT NULL DEFAULT false');
        $this->execute('CREATE INDEX ix_news_tag_main ON news_tag(status, is_main);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191204_064018_add_column_news_tag cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191204_064018_add_column_news_tag cannot be reverted.\n";

        return false;
    }
    */
}
