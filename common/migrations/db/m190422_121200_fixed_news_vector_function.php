<?php

use yii\db\Migration;

/**
 * Class m190422_121200_fixed_news_vector_function
 */
class m190422_121200_fixed_news_vector_function extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE OR REPLACE FUNCTION news_document_vector_update() RETURNS TRIGGER AS $$
            BEGIN
                IF TG_OP = 'UPDATE' THEN  
                    IF(OLD.title<>NEW.title or OLD.description<>NEW.description or OLD.content<>NEW.content) THEN

                        if (select(EXISTS(SELECT 1 FROM public.news_content a WHERE a.article_id = OLD.id))) THEN
                          UPDATE public.news_content SET document_vector = (setweight(to_tsvector(coalesce(lower(new.title),'')), 'A') || setweight(to_tsvector(coalesce(lower(new.description),'')), 'D') || setweight(to_tsvector(coalesce(lower(new.content),'')), 'C')) WHERE article_id=OLD.id;
                        ELSE
                          INSERT INTO public.news_content (article_id, document_vector)
                            VALUES (NEW.id, (setweight(to_tsvector(coalesce(lower(new.title),'')), 'A') || setweight(to_tsvector(coalesce(lower(new.description),'')), 'D') || setweight(to_tsvector(coalesce(lower(new.content),'')), 'C')));
                        end if;
                    end if;
                ELSE
                    INSERT INTO public.news_content (article_id, document_vector)
                            VALUES (NEW.id, (setweight(to_tsvector(coalesce(lower(new.title),'')), 'A') || setweight(to_tsvector(coalesce(lower(new.description),'')), 'D') || setweight(to_tsvector(coalesce(lower(new.content),'')), 'C')));
                end if;
            RETURN NEW;
        END;
      $$ LANGUAGE plpgsql;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190422_121200_fixed_news_vector_function cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190422_121200_fixed_news_vector_function cannot be reverted.\n";

        return false;
    }
    */
}
