<?php

use yii\db\Migration;

/**
 * Class m190312_090230_fixed_trigger_update_vector
 */
class m190312_090230_fixed_trigger_update_vector extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS news_content_vector_changed ON news_article;');
        // $this->execute('DROP FUNCTION IF EXISTS news_document_vector_update');
        $this->execute('
        CREATE OR REPLACE FUNCTION news_document_vector_update() RETURNS TRIGGER AS $$
            BEGIN
                IF TG_OP = \'UPDATE\' THEN  
                    IF(OLD.title<>NEW.title or OLD.description<>NEW.description or OLD.content<>NEW.content) THEN

                        if (select(EXISTS(SELECT 1 FROM public.news_content a WHERE a.article_id = OLD.id))) THEN
                          UPDATE public.news_content SET document_vector = (to_tsvector(NEW.title) || to_tsvector(NEW.description) || to_tsvector(NEW.content)) WHERE article_id=OLD.id;
                        ELSE
                          INSERT INTO public.news_content (article_id, document_vector)
                            VALUES (NEW.id, (to_tsvector(NEW.title) || to_tsvector(NEW.description) || to_tsvector(NEW.content)));
                        end if;
                    end if;
                ELSE
                    INSERT INTO public.news_content (article_id, document_vector)
                            VALUES (NEW.id, (to_tsvector(NEW.title) || to_tsvector(NEW.description) || to_tsvector(NEW.content)));
                end if;
            RETURN NEW;
        END;
      $$ LANGUAGE plpgsql;');
    $this->execute('
      CREATE TRIGGER news_content_vector_changed
        AFTER INSERT OR UPDATE ON news_article
        FOR EACH ROW
        EXECUTE PROCEDURE news_document_vector_update();');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190312_090230_fixed_trigger_update_vector cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190312_090230_fixed_trigger_update_vector cannot be reverted.\n";

        return false;
    }
    */
}
