<?php

use yii\db\Migration;

/**
 * Class m190503_110314_create_table_feedback_message
 */
class m190503_110314_create_table_feedback_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE feedback_message (
            id serial NOT NULL,
            subject varchar(255) NOT NULL,
            message_txt text NOT NULL,
            phone varchar(255) NULL DEFAULT NULL,
            email varchar(255) NULL DEFAULT NULL,
            full_name varchar(255) NOT NULL,
            user_id int4 NULL,
            is_mobile bool NOT NULL DEFAULT false,
            created_at timestamp NOT NULL,
            PRIMARY KEY (id),
            FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE 
                ON DELETE SET NULL
        );');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190503_110314_create_table_feedback_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190503_110314_create_table_feedback_message cannot be reverted.\n";

        return false;
    }
    */
}
