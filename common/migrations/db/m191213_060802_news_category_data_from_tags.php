<?php

use yii\db\Migration;
use yii\db\Query;
use common\models\NewsCategory;


/**
 * Class m191213_060802_news_category_data_from_tags
 */
class m191213_060802_news_category_data_from_tags extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_category ADD COLUMN "is_main" bool NOT NULL DEFAULT false');
        $this->execute('CREATE INDEX ix_news_category_main ON news_category(lang_id, status, is_main);');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191213_060802_news_category_data_from_tags cannot be reverted.\n";

        return false;
    }

    protected function getTagList(){
        $sql = 'select jsonb_array_elements_text(more_option->\'tags\')::jsonb->>\'title\' as title,jsonb_array_elements_text(more_option->\'tags\')::jsonb->>\'slug\' as slug
      from news_article
      group by jsonb_array_elements_text(more_option->\'tags\') 
      ';
        $result =  \Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191213_060802_news_category_data_from_tags cannot be reverted.\n";

        return false;
    }
    */
}
