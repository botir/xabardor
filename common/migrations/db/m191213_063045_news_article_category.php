<?php

use yii\db\Migration;
use yii\db\Query;
/**
 * Class m191213_063045_news_article_category
 */
class m191213_063045_news_article_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $categories = $this->listCategories();
        foreach ($categories as $category) {
            
            //$this->execute('UPDATE news_article SET category_id='.$category_id.' WHERE id='.$id);
            $this->execute("update news_article set category_id=".$category['id']."
FROM news_article a1
JOIN LATERAL jsonb_array_elements(a1.more_option->'tags') as obj ON (obj->>'slug')='".$category['slug']."'
WHERE a1.id = news_article.id
           ");
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191213_063045_news_article_category cannot be reverted.\n";

        return false;
    }

    public static function listCategories()
    {
        $query = (new Query())
            ->from('news_category');
        return $query->all();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191213_063045_news_article_category cannot be reverted.\n";

        return false;
    }
    */
}
