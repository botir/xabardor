<?php

use yii\db\Migration;

/**
 * Class m190127_065618_alter_news_article_add_column
 */
class m190127_065618_alter_news_article_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->execute('ALTER TABLE news_article ADD COLUMN "is_column" bool NOT NULL DEFAULT false');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190127_065618_alter_news_article_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190127_065618_alter_news_article_add_column cannot be reverted.\n";

        return false;
    }
    */
}
