<?php

use yii\db\Migration;

/**
 * Class m190424_211431_article_view_index
 */
class m190424_211431_article_view_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE INDEX ix_news_article_active_slug ON news_article(status, slug);');
        $this->execute('CREATE INDEX ix_news_article_active_tabnumber ON news_article(status, tab_number);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190424_211431_article_view_index cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190424_211431_article_view_index cannot be reverted.\n";

        return false;
    }
    */
}
