<?php

use common\models\User;
use yii\db\Migration;
use yii\db\Expression;

class m150725_192740_seed_data extends Migration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'id' => 1,
            'tab_number' => 111999,
            'username' => 'xabardoradmin',
            'email' => 'admin@xabardor.uz',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('admin@xabardor'),
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'status' => User::STATUS_ACTIVE,
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()')
        ]);
        $this->insert('{{%user}}', [
            'id' => 2,
            'tab_number' => 112999,
            'username' => 'xabardormanager',
            'email' => 'manager@xabardor.uz',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('manager@xabardor'),
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'status' => User::STATUS_ACTIVE,
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()')
        ]);
        $this->insert('{{%user}}', [
            'id' => 3,
            'tab_number' => 113999,
            'username' => 'userxabardor',
            'email' => 'user@xabardor.com',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('user@xabardor'),
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'status' => User::STATUS_ACTIVE,
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()')
        ]);

        $this->insert('{{%user_profile}}', [
            'user_id' => 1,
            'locale' => Yii::$app->sourceLanguage,
            'firstname' => 'Admin',
            'lastname' => 'Main'
        ]);
        $this->insert('{{%user_profile}}', [
            'user_id' => 2,
            'locale' => Yii::$app->sourceLanguage,
            'firstname' => 'Manager'
        ]);
        $this->insert('{{%user_profile}}', [
            'user_id' => 3,
            'locale' => Yii::$app->sourceLanguage,
            'firstname' => 'User'
        ]);

        
        $this->insert('{{%key_storage_item}}', [
            'key' => 'backend.theme-skin',
            'value' => 'skin-blue',
            'comment' => 'skin-blue, skin-black, skin-purple, skin-green, skin-red, skin-yellow'
        ]);

        $this->insert('{{%key_storage_item}}', [
            'key' => 'backend.layout-fixed',
            'value' => 0
        ]);

        $this->insert('{{%key_storage_item}}', [
            'key' => 'backend.layout-boxed',
            'value' => 0
        ]);

        $this->insert('{{%key_storage_item}}', [
            'key' => 'backend.layout-collapsed-sidebar',
            'value' => 0
        ]);

        $this->insert('{{%key_storage_item}}', [
            'key' => 'frontend.maintenance',
            'value' => 'disabled',
            'comment' => 'Set it to "enabled" to turn on maintenance mode'
        ]);

    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->delete('{{%key_storage_item}}', [
            'key' => 'frontend.maintenance'
        ]);

        $this->delete('{{%key_storage_item}}', [
            'key' => [
                'backend.theme-skin',
                'backend.layout-fixed',
                'backend.layout-boxed',
                'backend.layout-collapsed-sidebar',
            ],
        ]);
        
        $this->delete('{{%user_profile}}', [
            'user_id' => [1, 2, 3]
        ]);

        $this->delete('{{%user}}', [
            'id' => [1, 2, 3]
        ]);
    }
}
