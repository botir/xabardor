<?php

use yii\db\Migration;

/**
 * Class m190212_062701_create_table_bookmarks
 */
class m190212_062701_create_table_bookmarks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE user_bookmark (
                news_id int4 NOT NULL,
                user_id int4 NOT NULL,
                created_at timestamptz NOT NULL,
                CONSTRAINT user_bookmark_pkey PRIMARY KEY (news_id, user_id),
                FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
                FOREIGN KEY (news_id) REFERENCES "news_article"(id) ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190212_062701_create_table_bookmarks cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190212_062701_create_table_bookmarks cannot be reverted.\n";

        return false;
    }
    */
}
