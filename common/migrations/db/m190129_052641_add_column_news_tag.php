<?php

use yii\db\Migration;

/**
 * Class m190129_052641_add_column_news_tag
 */
class m190129_052641_add_column_news_tag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_tag ADD COLUMN "is_menu" bool NOT NULL DEFAULT false');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190129_052641_add_column_news_tag cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190129_052641_add_column_news_tag cannot be reverted.\n";

        return false;
    }
    */
}
