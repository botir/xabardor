<?php

use yii\db\Migration;
use yii\db\Query;
use common\models\NewsCategory;
/**
 * Class m191215_171731_news_category_data
 */
class m191215_171731_news_category_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tags = $this->getTagList();
        $this->execute('delete from news_category');
        foreach ($tags as $tag) {
            $t = new NewsCategory();
            $t->title = $tag['title'];
            $t->title_oz = $tag['title'];
            //$t->slug = $tag['slug'];
            $t->lang_id = 1;
            $t->save();
        }

         $categories = $this->listCategories();
        foreach ($categories as $category) {
            
            //$this->execute('UPDATE news_article SET category_id='.$category_id.' WHERE id='.$id);
            $this->execute("update news_article set category_id=".$category['id']."
FROM news_article a1
JOIN LATERAL jsonb_array_elements(a1.more_option->'tags') as obj ON (obj->>'slug')='".$category['slug']."'
WHERE a1.id = news_article.id
           ");
        }
    }

    protected function getTagList(){
        $sql = 'select jsonb_array_elements_text(more_option->\'tags\')::jsonb->>\'title\' as title,jsonb_array_elements_text(more_option->\'tags\')::jsonb->>\'slug\' as slug
      from news_article
      group by jsonb_array_elements_text(more_option->\'tags\') 
      ';
        $result =  \Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191215_171731_news_category_data cannot be reverted.\n";

        return false;
    }

    public static function listCategories()
    {
        $query = (new Query())
            ->from('news_category');
        return $query->all();
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191215_171731_news_category_data cannot be reverted.\n";

        return false;
    }
    */
}
