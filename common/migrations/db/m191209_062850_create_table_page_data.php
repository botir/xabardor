<?php

use yii\db\Migration;

/**
 * Class m191209_062850_create_table_page_data
 */
class m191209_062850_create_table_page_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE "page_data"
            (   
                page_id int4 NOT NULL,
                lang_id int2 NOT NULL,
                title varchar(405) NOT NULL,
                content text NOT NULL,
                PRIMARY KEY (page_id, lang_id),
                CONSTRAINT fkpage_data_id FOREIGN KEY (page_id)
                    REFERENCES "page_static" (id) MATCH SIMPLE
                    ON UPDATE CASCADE ON DELETE CASCADE
            )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191209_062850_create_table_page_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191209_062850_create_table_page_data cannot be reverted.\n";

        return false;
    }
    */
}
