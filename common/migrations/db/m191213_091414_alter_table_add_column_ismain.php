<?php

use yii\db\Migration;

/**
 * Class m191213_091414_alter_table_add_column_ismain
 */
class m191213_091414_alter_table_add_column_ismain extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $this->execute('ALTER TABLE news_category ADD COLUMN "is_main" bool NOT NULL DEFAULT false');
        // $this->execute('CREATE INDEX ix_news_category_main ON news_category(lang_id, status, is_main);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191213_091414_alter_table_add_column_ismain cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191213_091414_alter_table_add_column_ismain cannot be reverted.\n";

        return false;
    }
    */
}
