<?php

use yii\db\Migration;

/**
 * Class m191213_055905_alter_table_add_column
 */
class m191213_055905_alter_table_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE news_category ADD COLUMN "title_oz" varchar(300) NULL');
        $this->execute('ALTER TABLE news_category ADD COLUMN "is_menu" bool NOT NULL DEFAULT false');
        $this->execute('ALTER TABLE news_category ADD COLUMN lang_id int2 NOT NULL DEFAULT 1');
        $this->execute('CREATE INDEX ixnews_category_menu ON news_category(lang_id, status, is_menu);');
        $this->execute('CREATE INDEX ixnews_category_lang_active ON news_category(lang_id, status);');
        $this->execute('CREATE INDEX ixnews_category_lang_slug ON news_category(lang_id, slug);');
        $this->execute('CREATE INDEX ixnews_category_lang_all ON news_category(lang_id);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191213_055905_alter_table_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191213_055905_alter_table_add_column cannot be reverted.\n";

        return false;
    }
    */
}
