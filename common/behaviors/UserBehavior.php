<?php
namespace common\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\di\Instance;
use common\models\User;


class UserBehavior extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $owner;

    
    public function events()
    {
        $events = [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsertSingle'
        ];
        return $events;
    }
    
    public function beforeInsertSingle()
    {
        $this->owner->tab_number = $this->generateCode();
    }

    public function generateCode()
    {
        $number=100000;
        while(true){
           $number = rand(100000,999999); 
           $result = User::find()
            ->select(['tab_number'])
            ->where(['tab_number' => $number])
            ->asArray()
            ->scalar();
            if (!$result){
                break;
            }
        }
        return $number;
    }
}
