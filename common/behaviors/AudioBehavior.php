<?php
namespace common\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\NewsAudio;
use common\components\getid3\Yii2Getid3;


class AudioBehavior extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $owner;

    
    public function events()
    {
        $events = [
            ActiveRecord::EVENT_INIT => 'eventInit',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFindSingle',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsertSingle',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdateSingle',
        ];
        return $events;
    }

    /**
     * @return void
     */
    public function eventInit()
    {   
        $now = new \DateTime();
        $this->owner->published_at = $now->format('d.m.Y, H:i:s');
        $this->owner->status = 2;
        $this->owner->author_id = 1;
        $this->owner->lang_id = 1;
        $this->owner->listen_count = 0;
    }   

    /**
     * @return void
     */
    public function afterFindSingle()
    {   
        $owner_id = $this->owner->getAttribute('id');
        $lang_id = $this->owner->lang_id;
        if ($owner_id){
            $content = $this->owner->getAttribute('content');
            $this->owner->title_uz = $content['uz']; 
            $this->owner->title_kr = $content['kr']; 
            $this->owner->title_ru = $content['ru']; 
            $this->owner->title_en = $content['en']; 
            if ($lang_id == 4){
                $this->owner->title = $content['en'];
            }elseif($lang_id == 3){
                $this->owner->title = $content['ru'];
            }else{
                $this->owner->title = $content['uz'];
            }
        }

        $published_date = $this->owner->published_at;
        $dt1 = \DateTime::createFromFormat('Y-m-d H:i:s', $published_date);
        $this->owner->published_at = $dt1->format('d.m.Y, H:i:s');
    }

    public function beforeInsertSingle()
    {

        $now = new \DateTime();
        $published_date = $this->owner->published_at;
        if (!$published_date){
            $published_date = $now->format('Y-m-d H:i:s');
        }else{
            $dt1 = \DateTime::createFromFormat('d.m.Y, H:i:s', $published_date);
            $published_date = $dt1->format('Y-m-d H:i:s');
        }
        $this->owner->published_at = $published_date;
        $this->owner->code = $this->generateCode();
        $this->owner->duration = $this->getDuration($this->owner);
        $content = [
            'uz' => $this->owner->title_uz,
            'kr' => $this->owner->title_kr,
            'ru' => $this->owner->title_ru,
            'en' => $this->owner->title_en
        ];
        $this->owner->content =  $content;
    }

    public function beforeUpdateSingle()
    {

        $now = new \DateTime();
        $published_date = $this->owner->published_at;
        if (!$published_date){
            $published_date = $now->format('Y-m-d H:i:s');
        }else{
            $dt1 = \DateTime::createFromFormat('d.m.Y, H:i:s', $published_date);
            $published_date = $dt1->format('Y-m-d H:i:s');
        }
        $this->owner->published_at = $published_date;
        $content = [
            'uz' => $this->owner->title_uz,
            'kr' => $this->owner->title_kr,
            'ru' => $this->owner->title_ru,
            'en' => $this->owner->title_en
        ];
        $this->owner->content =  $content;
        $this->owner->duration = $this->getDuration($this->owner);
    }  
   

    public function getDuration($model){
        $getID3 = new Yii2Getid3();
        $filename =  Yii::getAlias('@storage').'/web/source/'.$model->audio_file['path'];
        $duration = $getID3->getData($filename);
        return $duration;
    }

    public function generateCode()
    {
        $number=100000;
        while(true){
           $number = rand(100000,999999); 
           $result = NewsAudio::find()
            ->select(['code'])
            ->where(['code' => $number])
            ->asArray()
            ->scalar();
            if (!$result){
                break;
            }
        }
        return $number;
    }
}
