<?php
namespace common\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\components\NewsHelper;
use common\models\NewsArticle;


class ArticleBehavior extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $owner;

    
    public function events()
    {
        $events = [
            ActiveRecord::EVENT_INIT => 'eventInit',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFindSingle',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsertSingle',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdateSingle',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdateSingle',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsertSingle',
        ];
        return $events;
    }

    /**
     * @return void
     */
    public function eventInit()
    {   
        $this->owner->show_image = true;
        $this->owner->list_image = true;
        $this->owner->show_desc = true;
        $this->owner->is_allowcoment = false;
        $this->owner->caption_image = '';
        $now = new \DateTime();
        $this->owner->published_at = $now->format('d.m.Y, H:i:s');
        $this->owner->status = 2;
        $this->owner->content_type = 1;
        $this->owner->author_id = 1;
        $this->owner->lang_id = 1;
    }   

    /**
     * @return void
     */
    public function afterFindSingle()
    {   
        $owner_id = $this->owner->getAttribute('id');
        $options = $this->owner->more_option;
        $lang_id = $this->owner->lang_id;

        if (isset($options['show_image']))
            $this->owner->show_image = $options['show_image'];
        else{
            $this->owner->show_image = true;
        }

        if (isset($options['list_image']))
            $this->owner->list_image = $options['list_image'];
        else 
            $this->owner->list_image = true;
        if (isset($options['show_desc']))
            $this->owner->show_desc = $options['show_desc'];
        else
            $this->owner->show_desc = true;
        if (isset($options['is_allowcoment']))
            $this->owner->is_allowcoment = $options['is_allowcoment'];
        else
            $this->owner->is_allowcoment = false;
        if (isset($options['caption_image']))
            $this->owner->caption_image = $options['caption_image'];
        else
            $this->owner->caption_image = '';
        if (isset($options['tags']) && $options['tags'] && is_array($options['tags'])){
            $tags = [];
            foreach ($options['tags'] as $tag) {
                $tags[] = $tag['id'];
            }
            $this->owner->hash_tags = $tags;
        }

        $news_data = $this->getNewsData($owner_id, $lang_id);
        if ($news_data){
            $this->owner->title = $news_data['title']; 
            $this->owner->description = $news_data['description']; 
            $this->owner->content = $news_data['content']; 
            $this->owner->caption_image = $news_data['caption_image']; 
            $this->owner->lead_info = $news_data['lead_info']; 
        }

        //$published_date = $this->owner->published_at;
        //$dt1 = \DateTime::createFromFormat('Y-m-d H:i:sT', $published_date);
        
        //$this->owner->published_at = $dt1->format('d.m.Y, H:i:s');

    }

    public function beforeInsertSingle()
    {

        $now = new \DateTime();
        $published_date = $this->owner->published_at;
        if (!$published_date){
            $published_date = $now->format('Y-m-d H:i:s');
        }else{
            $dt1 = \DateTime::createFromFormat('d.m.Y, H:i:s', $published_date);
            $published_date = $dt1->format('Y-m-d H:i:s');
        }
        $this->owner->published_at = $published_date;
        $this->owner->tab_number = $this->generateCode();

        $tags = [];
        if ($this->owner->hash_tags && is_array($this->owner->hash_tags)){
            $tag_records = $this->getHashTags($this->owner->hash_tags);
            if ($tag_records){
                foreach ($tag_records as $record) {
                    $tags[] = ['id'=>$record['id'], 'slug'=>$record['slug'], 'title'=>$record['title']];  
                    $this->addHashTag($record['id']);
                }
            }
        }

        $more_option = [
            'show_image' => boolval($this->owner->show_image),
            'list_image' => boolval($this->owner->list_image),
            'show_desc' => boolval($this->owner->show_desc),
            'is_allowcoment' => boolval($this->owner->is_allowcoment),
            'caption_image' => $this->owner->caption_image,
            'tags' => $tags,
            'like_count' => 0
        ];
        $this->owner->more_option =  $more_option;
        
    }

    public function beforeUpdateSingle()
    {

        $now = new \DateTime();
        $published_date = $this->owner->published_at;
        if (!$published_date){
            $published_date = $now->format('Y-m-d H:i:s');
        }else{
            $dt1 = \DateTime::createFromFormat('d.m.Y, H:i:s', $published_date);
            $published_date = $dt1->format('Y-m-d H:i:s');
        }
        $this->owner->published_at = $published_date;
        
        if (isset($this->owner->more_option['like_count']))
            $like_count = intval($this->owner->more_option['like_count']);
        else
            $like_count = 0;
        $current_tags = $this->getCurruntTags($this->owner->id);
        $tags = [];
        $has_tags = [];
        if ($this->owner->hash_tags && is_array($this->owner->hash_tags)){
            $tag_records = $this->getHashTags($this->owner->hash_tags);
            if ($tag_records){
                foreach ($tag_records as $record) {
                    $tags[] = ['id'=>$record['id'], 'slug'=>$record['slug'], 'title'=>$record['title']];
                    $has_tags[] = $record['id'];
                    if (!in_array($record['id'],  $current_tags)) {
                        $this->addHashTag($record['id']);
                    }
                }
            }
            foreach ($current_tags as $current_tag) {
                if (!in_array($current_tag, $has_tags)){
                    $this->removeHashTag($current_tag);
                }
            }
        }

        $more_option = [
            'show_image' => boolval($this->owner->show_image),
            'list_image' => boolval($this->owner->list_image),
            'show_desc' => boolval($this->owner->show_desc),
            'is_allowcoment' => boolval($this->owner->is_allowcoment),
            'caption_image' => $this->owner->caption_image,
            'tags' => $tags,
            'like_count' => $like_count
        ];
        $this->owner->more_option =  $more_option;
        
    }

    public function afterInsertSingle()
    {       
        $owner_id = $this->owner->getAttribute('id');
        $lang_id = $this->owner->lang_id;
        $title = $this->owner->title;
        $description = $this->owner->description;
        $content = $this->owner->content;
        $caption_image = $this->owner->caption_image;
        $lead_info = $this->owner->lead_info;

        $this->insertNewsData($owner_id, $lang_id, $title, $description, $content, $caption_image, $lead_info);
        if ($lang_id == 1){
            $title = $title;
            $lead_info = $lead_info;
            $description = $description;
            $content = $content;
            if ($caption_image)
                $caption_image = $caption_image;
            $this->insertNewsData($owner_id, 2, $title, $description, $content, $caption_image, $lead_info);
        }
    }

    public function afterUpdateSingle()
    {
        $owner_id = $this->owner->getAttribute('id');
        $lang_id = $this->owner->lang_id;
        $title = $this->owner->title;
        $description = $this->owner->description;
        $lead_info = $this->owner->lead_info;
        $content = $this->owner->content;
        $caption_image = $this->owner->caption_image;
        $has_content = $this->hasNewsData($owner_id, $lang_id);
        if($has_content){
            $this->updateNewsData($owner_id, $lang_id, $title, $description, $content, $caption_image, $lead_info);
        }else{
            $this->insertNewsData($owner_id, $lang_id, $title, $description, $content, $caption_image, $lead_info);
        }
        if ($lang_id == 1){
            $has_content = $this->hasNewsData($owner_id, 2);
            $title = $title;
            $description = $description;
            $lead_info = $lead_info;
            $content = $content;
            if ($caption_image)
                $caption_image = $caption_image;
            if($has_content){
                $this->updateNewsData($owner_id, 2, $title, $description, $content, $caption_image, $lead_info);
            }else{
                $this->insertNewsData($owner_id, 2, $title, $description, $content, $caption_image, $lead_info);
            }
        }        
    }

    public function generateCode()
    {
        $number=100000;
        while(true){
           $number = rand(100000,999999); 
           $result = NewsArticle::find()
            ->select(['tab_number'])
            ->where(['tab_number' => $number])
            ->asArray()
            ->scalar();
            if (!$result){
                break;
            }
        }
        return $number;
    }

    protected function getHashTags($tags){
        $ids = implode(",",$tags);
        $result = \Yii::$app->db->createCommand("select id, slug, title from news_tag where id in (".$ids.") order by sort asc")->queryAll();
        return $result;
    }

    protected function addHashTag($id){
        Yii::$app->db->createCommand('UPDATE news_tag SET c_weight=c_weight+1 WHERE id='.$id)->execute();
    }

    protected function removeHashTag($id){
        Yii::$app->db->createCommand('UPDATE news_tag SET c_weight=c_weight-1 WHERE id='.$id)->execute();
    }

    protected function getCurruntTags($id){
        $result = \Yii::$app->db->createCommand('select more_option from news_article where id='.$id)->queryOne();
        $tags = [];
        if ($result){
            $array_data = Json::decode($result['more_option']);
            if (isset($array_data['tags']) && count($array_data['tags']) > 0){
                foreach ($array_data['tags'] as $tag) {
                    $tags[] = $tag['id'];
                }
            }
        }     
        return $tags;
    }

    protected function in_array_field($needle, $needle_field, $haystack, $strict = false) {
        if ($strict) {
            foreach ($haystack as $item)
                if (isset($item->$needle_field) && $item->$needle_field === $needle)
                    return true;
        }
        else {
            foreach ($haystack as $item)
                if (isset($item->$needle_field) && $item->$needle_field == $needle)
                    return true;
        }
        return false;
    }


    protected function hasNewsData($news_id, $lang_id){
        $result = \Yii::$app->db->createCommand("select exists(select 1 from news_data where news_id=:news_id and lang_id=:lang_id)",[':news_id'=>$news_id, ':lang_id'=>$lang_id])->queryOne();
        return $result['exists'];
    }

    protected function getNewsData($news_id, $lang_id){
        $result = \Yii::$app->db->createCommand("select news_id, title, description, content, caption_image, lead_info from news_data where news_id=:news_id and lang_id=:lang_id",[':news_id'=>$news_id, ':lang_id'=>$lang_id])->queryOne();
        return $result;
    }

    protected static function insertNewsData($news_id, $lang_id, $title, $description, $content, $caption_image=null, $lead_info=null){
        $connection =  \Yii::$app->db;
        $connection->createCommand()->insert('news_data', [
            'news_id' => $news_id,
            'lang_id' => $lang_id,
            'title' => $title,
            'description' => $description,
            'content' => $content,
            'caption_image' => $caption_image,
            'lead_info' => $lead_info
        ])->execute();
    }

    public static function updateNewsData($news_id, $lang_id, $title, $description, $content, $caption_image=null, $lead_info=null){
        $connection =  \Yii::$app->db;
        $connection->createCommand()->update('news_data', [
            'title' => $title,
            'description' => $description,
            'content' => $content,
            'caption_image' => $caption_image,
            'lead_info' => $lead_info
        ], 
        'news_id = :news_id and lang_id=:lang_id', [':news_id' => $news_id, ':lang_id'=>$lang_id])->execute();
    }
}
