<?php
namespace common\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\components\NewsHelper;
use common\models\NewsArticle;



class PageBehavior extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $owner;

    
    public function events()
    {
        $events = [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFindSingle',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdateSingle',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsertSingle',
        ];
        return $events;
    } 

    /**
     * @return void
     */
    public function afterFindSingle()
    {   
        $owner_id = $this->owner->getAttribute('id');
        $all_data = $this->getallPageData($owner_id);
        foreach ($all_data as $data) {
            if ($data['lang_id'] == 1){
                $this->owner->title_uz = $data['title'];
                $this->owner->content_uz = $data['content'];
            }elseif($data['lang_id'] == 2){
                $this->owner->title_kr = $data['title'];
                $this->owner->content_kr = $data['content'];
            }elseif($data['lang_id'] == 3){
                $this->owner->title_ru = $data['title'];
                $this->owner->content_ru = $data['content'];
            }
            elseif($data['lang_id'] == 4){
                $this->owner->title_en = $data['title'];
                $this->owner->content_en = $data['content'];
            }
        }

    }   

    public function afterInsertSingle()
    {       
        $owner_id = $this->owner->getAttribute('id');
        $title_uz = $this->owner->title_uz;
        $content_uz = $this->owner->content_uz;

        $title_kr = $this->owner->title_kr;
        $content_kr = $this->owner->content_kr;

        $title_ru = $this->owner->title_ru;
        $content_ru = $this->owner->content_ru;

        $title_en = $this->owner->title_en;
        $content_en = $this->owner->content_en;


        $this->insertPageData($owner_id, 1, $title_uz, $content_uz);
        $this->insertPageData($owner_id, 2, $title_kr, $content_kr);
        $this->insertPageData($owner_id, 3, $title_ru, $content_ru);
        $this->insertPageData($owner_id, 4, $title_en, $content_en);
    }

    public function afterUpdateSingle()
    {
        $owner_id = $this->owner->getAttribute('id');
        $title_uz = $this->owner->title_uz;
        $content_uz = $this->owner->content_uz;

        $title_kr = $this->owner->title_kr;
        $content_kr = $this->owner->content_kr;

        $title_ru = $this->owner->title_ru;
        $content_ru = $this->owner->content_ru;

        $title_en = $this->owner->title_en;
        $content_en = $this->owner->content_en;
        $has_content_uz = $this->hasPageData($owner_id, 1);
        if($has_content_uz){
            $this->updatePageData($owner_id, 1, $title_uz, $content_uz);
        }else{
            $this->insertPageData($owner_id, 1, $title_uz, $content_uz);
        }
        $has_content_kr = $this->hasPageData($owner_id, 2);
        if($has_content_kr){
            $this->updatePageData($owner_id, 2, $title_kr, $content_kr);
        }else{
            $this->insertPageData($owner_id, 2, $title_kr, $content_kr);
        }
        $has_content_ru = $this->hasPageData($owner_id, 3);
        if($has_content_kr){
            $this->updatePageData($owner_id, 3, $title_ru, $content_ru);
        }else{
            $this->insertPageData($owner_id, 3, $title_ru, $content_ru);
        }
        $has_content_en = $this->hasPageData($owner_id, 4);
        if($has_content_en){
            $this->updatePageData($owner_id, 4, $title_en, $content_en);
        }else{
            $this->insertPageData($owner_id, 4, $title_en, $content_en);
        }
    }


    protected function hasPageData($page_id, $lang_id){
        $result = \Yii::$app->db->createCommand("select exists(select 1 from page_data where page_id=:page_id and lang_id=:lang_id)",[':page_id'=>$page_id, ':lang_id'=>$lang_id])->queryOne();
        return $result['exists'];
    }

    protected function getPageData($page_id, $lang_id){
        $result = \Yii::$app->db->createCommand("select page_id, title, content from page_data where page_id=:page_id and lang_id=:lang_id",[':page_id'=>$page_id, ':lang_id'=>$lang_id])->queryOne();
        return $result;
    }

    protected function getallPageData($page_id){
        $result = \Yii::$app->db->createCommand("select lang_id, page_id, title, content from page_data where page_id=:page_id order by lang_id asc limit 4",[':page_id'=>$page_id])->queryAll();
        return $result;
    }

    protected static function insertPageData($page_id, $lang_id, $title, $content){
        $connection =  \Yii::$app->db;
        $connection->createCommand()->insert('page_data', [
            'page_id' => $page_id,
            'lang_id' => $lang_id,
            'title' => $title,
            'content' => $content
        ])->execute();
    }

    public static function updatePageData($page_id, $lang_id, $title, $content){
        $connection =  \Yii::$app->db;
        $connection->createCommand()->update('page_data', [
            'title' => $title,
            'content' => $content
        ], 
        'page_id = :page_id and lang_id=:lang_id', [':page_id' => $page_id, ':lang_id'=>$lang_id])->execute();
    }
}
