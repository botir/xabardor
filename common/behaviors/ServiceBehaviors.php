<?php
namespace common\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\components\NewsHelper;
use common\models\NewsArticle;


class ServiceBehaviors extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $owner;

    
    public function events()
    {
        $events = [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFindSingle',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
        return $events;
    }
    
    /**
     * @return void
     */
    public function afterFindSingle()
    {   
        if (!is_array($this->owner->options) || count($this->owner->options)<1){
            $options = $this->owner->more_option;
            foreach ($options as $data) {
                $this->owner->options[] = $data['title'];
            }
        }
    }

    public function beforeSave()
    {
        if ($this->owner->options){
            $more_option = [];
            foreach ($this->owner->options as $data) {
                $more_option[] = ['title' => $data];
            }
            $this->owner->more_option = $more_option;
        }
    }
}
