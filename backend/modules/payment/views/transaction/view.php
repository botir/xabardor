<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\payment\models\Transaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-view">
   
    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'price',
            'cancel_time',
            'payment_time',
            'perform_time',
            'pay_trans_id',
            'reason',
            'receivers:ntext',
            'service_id',
            'user_id',
            'created_at',
            'status',
        ],
    ]) ?>

</div>
