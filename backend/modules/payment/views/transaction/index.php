<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\payment\models\search\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'pay_trans_id',
            'price',
            'user_email',
            'service_title',
            'cancel_time',
            'payment_time',
            'perform_time',
            // 'pay_trans_id',
            // 'reason',
            // 'receivers:ntext',
            // 'service_id',
            // 'user_id',
            // 'created_at',
            // 'status',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}'
            ]
        ],
    ]); ?>

</div>
