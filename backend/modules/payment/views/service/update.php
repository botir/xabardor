<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\payment\models\Service */

$this->title = 'Update Service: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="service-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
