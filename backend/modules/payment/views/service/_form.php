<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\payment\models\Service */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-4">
            <?php echo $form->field($model, 'price')->textInput() ?>
        </div>
        <div class="col-sm-4">
           <?php echo $form->field($model, 'status')->dropDownList(\backend\modules\payment\models\Service::statusList(), ['prompt'=>'']) ?>
        </div>
        <div class="col-sm-4">
           <?php echo $form->field($model, 'front_active')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->field($model, 'uniq_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12" id="dynamic-container">
            <?php if (count($model->options) < 1):?>
            <div class="row field-container">
                <div class="col-sm-6">
                    <?php echo $form->field($model, 'options[]',[
                        'template' => '<div class="input-group input-group-sm">{input}<span class="input-group-btn"><button type="button" class="btn btn-success btn-flat">+</button></span></div>'])
                    ->textInput([
                        'placeholder'=> 'опция',
                        'maxlength' => true,
                        'class' => 'form-control',
                    ]); 
                    ?>
                </div>
            </div>
            <?php else:?>
                <?php foreach($model->options as $data):?>
                    <?php if ( $data === reset( $model->options )):?>
                        <div class="row field-container">
                            <div class="col-sm-6">
                                <?php echo $form->field($model, 'options[]',[
                                    'template' => '<div class="input-group input-group-sm">{input}<span class="input-group-btn"><button type="button" class="btn btn-success btn-flat">+</button></span></div>'])
                                ->textInput([
                                    'value'=> $data,
                                    'placeholder'=> 'опция',
                                    'maxlength' => true,
                                    'class' => 'form-control',
                                ]); 
                                ?>
                            </div>
                        </div>
                    <?php else:?>
                        <div class="row field-container">
                            <div class="col-sm-6">
                                <?php echo $form->field($model, 'options[]',[
                                    'template' => '<div class="input-group input-group-sm">{input}<span class="input-group-btn"><button type="button" class="btn btn-danger btn-flat">-</button></span></div>'])
                                ->textInput([
                                    'value'=> $data,
                                    'placeholder'=> 'опция',
                                    'maxlength' => true,
                                    'class' => 'form-control',
                                ]); 
                                ?>
                            </div>
                        </div>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton('Сохранить', ['class' =>'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
