<?php

namespace backend\modules\payment\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\payment\models\Transaction;

/**
 * TransactionSearch represents the model behind the search form about `backend\modules\payment\models\Transaction`.
 */
class TransactionSearch extends Transaction
{
    public $service_title;
    public $user_email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price', 'reason', 'order_id', 'user_id', 'status'], 'integer'],
            [['cancel_time', 'payment_time', 'perform_time', 'pay_trans_id', 'receivers', 'created_at'], 'safe'],
            [['service_title', 'user_email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find()
            ->select([
                'payment_transaction.id',
                'payment_transaction.status',
                'payment_transaction.price',
                'payment_transaction.cancel_time',
                'payment_transaction.payment_time',
                'payment_transaction.reason',
                'payment_transaction.order_id',
                'payment_transaction.user_id',
                'payment_transaction.created_at',
                'service_title'=>'order.service.title',
                'user_email'=>'user.email'
            ])
        ->joinWith('order',false, 'INNER JOIN')
        ->joinWith('user',false, 'INNER JOIN');
        //->joinWith('user.userProfile',false, 'INNER JOIN');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'cancel_time' => $this->cancel_time,
            'perform_time' => $this->perform_time,
            'reason' => $this->reason,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['ilike', 'payment_time', $this->payment_time])
            ->andFilterWhere(['ilike', 'pay_trans_id', $this->pay_trans_id])
            ->andFilterWhere(['ilike', 'receivers', $this->receivers]);

        return $dataProvider;
    }
}
