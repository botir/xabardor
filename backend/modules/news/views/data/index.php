<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News Datas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-data-index">


    <p>
        <?php echo Html::a('Create News Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'news_id',
            'lang_id',
            'title',
            'description',
            'content:ntext',
            // 'caption_image',
            // 'lead_info',
            // 'document_vector',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
