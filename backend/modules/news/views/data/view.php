<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsData */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-data-view">

    <p>
        <?php echo Html::a('Update', ['update', 'news_id' => $model->news_id, 'lang_id' => $model->lang_id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'news_id' => $model->news_id, 'lang_id' => $model->lang_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php if ($model->lang_id == 2):?>
     <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-info">
                <div class="box-header">
                   <?php 
                       
                            echo Html::a('Lotinchada', ['/news/data/view', 'news_id' => $model->news_id,'lang_id'=>1], ['class' => 'btn btn-danger']);   
                        
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php elseif($model->lang_id == 1):?>
        <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-info">
                <div class="box-header">
                   <?php 
                       
                            echo Html::a('Крилчада', ['/news/data/view', 'news_id' => $model->news_id,'lang_id'=>2], ['class' => 'btn btn-danger']);   
                        
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

<div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title">Название</h3>
                </div>
                <div class="box-body">
                    <?=$model->title; ?>
                </div>
            </div>
        </div>
    </div>

        <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title">Краткая информация</h3>
                </div>
                <div class="box-body">
                    
                    
                    <?=$model->description?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <div class="col-md-12">
        <div class="box box-solid box-success">
            <div class="box-header">
                <h3 class="box-title">Текст</h3>
            </div>
            <div class="box-body">
                <?=$model->content?>
            </div>
        </div>
    </div>
</div>

</div>
