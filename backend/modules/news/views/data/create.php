<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsData */

$this->title = 'Create News Data';
$this->params['breadcrumbs'][] = ['label' => 'News Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-data-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
