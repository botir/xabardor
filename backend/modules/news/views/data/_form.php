<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mihaildev\elfinder\ElFinder;
use backend\widgets\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\NewsData */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="news-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

<div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'description')->textarea(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'full',
                 'clientOptions' => ElFinder::ckeditorOptions('elfinder',[
                    'allowedContent'=>[
                        'script'=>true,
                        'div'=>true,
                        '$1'=>[
                            'elements: CKEDITOR.dtd',
                            'attributes'=>true,
                            'styles'=>true,
                            'classes'=>true,
                        ],
                    ]
                    ])
                ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'caption_image')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
