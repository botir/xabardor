<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsData */

$this->title = 'Update News Data: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'news_id' => $model->news_id, 'lang_id' => $model->lang_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-data-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
