<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsAudio */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'News Audios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$path = $model->photo_file['base_url'].$model->photo_file['path'];
$audio = $model->base_url.$model->audio_path;
?>
<div class="news-audio-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title">Информация</h3>
                </div>
                <div class="box-body">
                    <?=$model->title; ?>
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                          <div id="audio_news_s" class="jp-jplayer" file="<?=$audio?>" file_photo="<?=$path?>"></div>
                          <div class="row">
        <div class="medium-12 small-12 columns">
          <div id="single-song-player">
            <img amplitude-song-info="cover_art_url" amplitude-main-song-info="true"/>
            <div class="bottom-container">
              <progress class="amplitude-song-played-progress" amplitude-main-song-played-progress="true" id="song-played-progress"></progress>

              <div class="time-container">
                <span class="current-time">
                  <span class="amplitude-current-minutes" amplitude-main-current-minutes="true"></span>:<span class="amplitude-current-seconds" amplitude-main-current-seconds="true"></span>
                </span>
                <span class="duration">
                  <span class="amplitude-duration-minutes" amplitude-main-duration-minutes="true"></span>:<span class="amplitude-duration-seconds" amplitude-main-duration-seconds="true"></span>
                </span>
              </div>

              <div class="control-container">
                <div class="amplitude-play-pause" amplitude-main-play-pause="true" id="play-pause"></div>
                <div class="meta-container">
                  <span amplitude-song-info="name" amplitude-main-song-info="true" class="song-name"></span>
                  <span amplitude-song-info="artist" amplitude-main-song-info="true"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


      <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'published_at',
            'created_at'
        ],
    ]) ?>

</div>
