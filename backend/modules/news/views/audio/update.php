<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsAudio */

$this->title = 'Update News Audio: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'News Audios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-audio-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
