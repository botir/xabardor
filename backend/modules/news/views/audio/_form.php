<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use trntv\yii\datetime\DateTimeWidget;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\NewsAudio */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="news-audio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-sm-3">
            <?php echo $form->field($model, 'lang_id')->dropDownList([1=>'uz', 3=>'ru', 4=>'en'], ['prompt'=>'']) ?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->field($model, 'status')->dropDownList(\common\models\NewsArticle::statusList(), ['prompt'=>'']) ?>
        </div>
       
        <div class="col-sm-3">
            <?php
              echo $form->field($model, 'author_id')->widget(Select2::classname(), [
                  'data' =>\common\models\UserAuthor::getListData(),
                  'language' => 'ru',
                  'options' => ['placeholder' => 'Автор'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->field($model, 'published_at')->widget(
                DateTimeWidget::className(),
                [
                    'phpDatetimeFormat' => 'dd.MM.yyyy, HH:mm:ss',
                    'options'=>[
                        'value'=>$model->published_at
                    ]
                ]
            ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6"><?php echo $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-6"><?php echo $form->field($model, 'title_kr')->textInput(['maxlength' => true]) ?></div>
            </div>
            <div class="row">
                <div class="col-sm-6"><?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-6"><?php echo $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'audio_file')->widget(
              Upload::className(),
              [
                  'url' => ['/file/storage/file-upload'],
                  'maxFileSize' => 50000000, // 50 MiB
              ]);
          ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->field($model, 'photo_file')->widget(
              Upload::className(),
              [
                  'url' => ['/file/storage/upload'],
                  'maxFileSize' => 5000000, // 5 MiB
              ]);
          ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранит', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
