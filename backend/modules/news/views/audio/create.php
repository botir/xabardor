<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsAudio */

$this->title = 'Create News Audio';
$this->params['breadcrumbs'][] = ['label' => 'News Audios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-audio-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
