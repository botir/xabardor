<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\NewsAudio;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\news\models\AudioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Аудио новости';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="news-audio-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute'=>'lang_id',
                'value'=>function ($model) {
                    return $model->getLanguageСode();
                },

            ],
            [
                'attribute'=>'status',
                'value'=>function ($model) {
                    return $model->currentStatus();
                },
                'filter'=>NewsAudio::allStatusList()

            ],
            [
                'attribute'=>'published_at',
                'value'=>function ($model) {
                    return $model->getPublishDate();
                },

            ],
            'listen_count',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
