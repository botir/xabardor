<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model common\models\NewsCategory */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="news-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <div class="col-sm-3">
            <?php echo $form->field($model, 'lang_id')->dropDownList([1=>'uz', 3=>'ru', 4=>'en'], ['prompt'=>'']) ?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->field($model, 'status')->dropDownList($model->statusList(), ['prompt'=>'']) ?>
        </div>
        <div class="col-sm-3">
            <?php  echo $form->field($model, 'sort')->widget(TouchSpin::classname(), [
                'pluginOptions' => [
                    'step' => 1,
                    'max' => 5000,
                    'buttonup_class' => 'btn btn-primary',
                    'buttondown_class' => 'btn btn-info',
                    'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                    'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>'

                ]]);
            ?>
        </div>
         <div class="col-sm-3">
                    <?php echo $form->field($model, 'is_menu')->checkbox() ?>
                    <?php echo $form->field($model, 'is_main')->checkbox() ?>
                </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->field($model, 'title_oz')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <?php echo $form->field($model, 'slug')
            ->hint(Yii::t('backend', 'If you leave this field empty, the slug will be generated automatically'))
            ->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранит', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
