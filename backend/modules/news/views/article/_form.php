<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mihaildev\elfinder\ElFinder;
use kartik\select2\Select2;
use backend\widgets\ckeditor\CKEditor;
use trntv\yii\datetime\DateTimeWidget;
use trntv\filekit\widget\Upload;
/* @var $this yii\web\View */
/* @var $model common\models\NewsArticle */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="news-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-4">
                    <?php
                      echo $form->field($model, 'hash_tags')->widget(Select2::classname(), [
                          'data' =>\common\models\NewsTag::getListData(),
                          'language' => 'ru',
                          'options' => ['placeholder' => 'Хештеги', 'multiple' => true],
                          'pluginOptions' => ['allowClear' => true],
                      ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?php
                      echo $form->field($model, 'category_id')->widget(Select2::classname(), [
                          'data' =>\common\models\NewsCategory::getListData(),
                          'language' => 'ru',
                          'options' => ['placeholder' => 'Категория', 'multiple' => false],
                          'pluginOptions' => ['allowClear' => true],
                      ]);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?php echo $form->field($model, 'published_at')->widget(
                        DateTimeWidget::className(),
                        [
                            'phpDatetimeFormat' => 'dd.MM.yyyy, HH:mm:ss',
                            'options'=>[
                                'value'=>$model->published_at
                            ]
                        ]
                    ) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'description')->textarea(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'full',
                 'clientOptions' => ElFinder::ckeditorOptions('elfinder',[
                    'allowedContent'=>[
                        'script'=>true,
                        'div'=>true,
                        '$1'=>[
                            'elements: CKEDITOR.dtd',
                            'attributes'=>true,
                            'styles'=>true,
                            'classes'=>true,
                        ],
                    ]
                    ])
                ]) ?>
                </div>
            </div>
            
        </div>
        <div class="col-sm-3">
            <?php echo $form->field($model, 'content_type')->dropDownList(\common\models\NewsArticle::getTypeList(), ['prompt'=>'']) ?>
            <?php echo $form->field($model, 'status')->dropDownList(\common\models\NewsArticle::statusList(), ['prompt'=>'']) ?>
            <?php
              echo $form->field($model, 'author_id')->widget(Select2::classname(), [
                  'data' =>\common\models\UserAuthor::getListData(),
                  'language' => 'ru',
                  'options' => ['placeholder' => 'Автор'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);
            ?>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'is_selected')->checkbox() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'photo')->widget(
                        Upload::class,
                        [
                            'url' => ['/file/storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB
                        ]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'caption_image')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid box-success">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php echo $form->field($model, 'show_image')->checkbox() ?>
                                </div>
                                <div class="col-sm-12">
                                    <?php echo $form->field($model, 'list_image')->checkbox() ?>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-sm-12">
                                    <?php echo $form->field($model, 'is_allowcoment')->checkbox() ?>
                                </div>
                                <div class="col-sm-12">
                                    <?php echo $form->field($model, 'show_desc')->checkbox() ?>
                                </div>
                                <div class="col-sm-12">
                                    <?php echo $form->field($model, 'day_photo')->checkbox() ?>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $form->field($model, 'slug')
                    ->hint(Yii::t('backend', 'If you leave this field empty, the slug will be generated automatically'))
                    ->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        
    </div>




    


    


    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранит', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
