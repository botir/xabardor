<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsArticle */

$this->title = 'Update News Article: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'News Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-article-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
