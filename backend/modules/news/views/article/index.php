<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\NewsArticle;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\news\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-article-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create News Article', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'tab_number',
            'title',
            'category_title',

            [
                'attribute'=>'published_at',
                'filter'=>false

            ],
            [
                'attribute'=>'views_count',
                'filter'=>false
            ],
            [
                'attribute'=>'status',
                'value'=>function ($model) {
                    return $model->currentStatus();
                },
                'filter'=>NewsArticle::allStatusList()

            ],
            // 'category_id',
            // 'tab_number',
            // 'image_path',
            // 'image_base_url:url',
            // 'content_type',
            // 'is_actual:boolean',
            // 'is_selected:boolean',
            // 'is_ads:boolean',
            // 'is_premium:boolean',
            // 'views_count',
            // 'status',
            // 'published_at',
            // 'created_at',
            // 'updated_at',
            // 'more_option',
            // 'slug',
            // 'author_id',
            // 'user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
