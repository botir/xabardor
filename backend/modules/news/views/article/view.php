<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsArticle */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'News Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="news-article-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php if ($data):?>
     <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-info">
                <div class="box-header">
                   <?php 
                       
                            echo Html::a('Крилчада', ['/news/data/update', 'news_id' => $model->id,'lang_id'=>2], ['class' => 'btn btn-danger']);   
                        
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title">Краткая информация</h3>
                </div>
                <div class="box-body">
                    <?=$model->title; ?>
                    <br />
                        ➥ Batafsil: <?=Html::a($model->getShortUrl(), $model->getShortUrl(), ['class'=>'kv-author-link'])?>
                    <br/>
                    <?=$model->description?>
                </div>
            </div>
        </div>
    </div>


    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'tab_number',
            'content_type',
            'is_actual',
            'is_selected',
            'is_ads',
            'is_premium',
            'views_count',
            'status',
            'published_at',
            'created_at',
            'updated_at',
            'author_id'
        ],
    ]) ?>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid box-success">
            <div class="box-header">
                <h3 class="box-title">ФОТО</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <?php if ($model->photo): ?>
                            <?= Html::img($model->photo['base_url'].$model->photo['path']);?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6" style="display: none;">
                        <?php if ($model->photo): $ph = explode('.', $model->photo['path']); $path = $model->photo['base_url'].'/thumbnails'.$ph[0].'_medium.'.$ph[1];  ?>
                            <?= Html::img($path);?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid box-success">
            <div class="box-header">
                <h3 class="box-title">Текст</h3>
            </div>
            <div class="box-body">
                <?=$model->content?>
            </div>
        </div>
    </div>
</div>