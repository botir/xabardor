<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsArticle */

$this->title = 'Create News Article';
$this->params['breadcrumbs'][] = ['label' => 'News Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-article-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
