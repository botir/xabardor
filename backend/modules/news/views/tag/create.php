<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsTag */

$this->title = 'добавить хэштег';
$this->params['breadcrumbs'][] = ['label' => 'News Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-tag-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
