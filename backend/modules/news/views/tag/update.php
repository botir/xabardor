<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsTag */

$this->title = 'изменить хэштег ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-tag-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
