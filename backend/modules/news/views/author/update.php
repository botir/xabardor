<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserAuthor */

$this->title = 'Update User Author: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Authors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-author-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
