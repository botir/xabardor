<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAuthor */

$this->title = 'Create User Author';
$this->params['breadcrumbs'][] = ['label' => 'User Authors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-author-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
