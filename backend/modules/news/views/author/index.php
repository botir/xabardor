<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\news\models\search\AuthorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Authors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-author-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create User Author', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nickname',
            'email:email',
            'firstname',
            'middlename',
            // 'lastname',
            // 'description',
            // 'avatar_path',
            // 'avatar_base_url:url',
            // 'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
