<?php

namespace backend\modules\news\controllers;

use Yii;
use common\models\NewsArticle;
use common\models\NewsData;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DataController implements the CRUD actions for NewsData model.
 */
class DataController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

   

    /**
     * Displays a single NewsData model.
     * @param integer $news_id
     * @param integer $lang_id
     * @return mixed
     */
    public function actionView($news_id, $lang_id)
    {
        $model = $this->findModel($news_id, $lang_id);
        $news = NewsArticle::findOne($news_id);
        return $this->render('view', [
            'model' => $model,
            'news' => $model,
        ]);
    }

   

    /**
     * Updates an existing NewsData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $news_id
     * @param integer $lang_id
     * @return mixed
     */
    public function actionUpdate($news_id, $lang_id)
    {
        $model = $this->findModel($news_id, $lang_id);
        $news = NewsArticle::findOne($news_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'news_id' => $model->news_id, 'lang_id' => $model->lang_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the NewsData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $news_id
     * @param integer $lang_id
     * @return NewsData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($news_id, $lang_id)
    {
        if (($model = NewsData::findOne(['news_id' => $news_id, 'lang_id' => $lang_id])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
