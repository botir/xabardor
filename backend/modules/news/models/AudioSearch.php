<?php

namespace backend\modules\news\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NewsAudio;

/**
 * AudioSearch represents the model behind the search form about `common\models\NewsAudio`.
 */
class AudioSearch extends NewsAudio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lang_id', 'code', 'status', 'user_id', 'author_id', 'listen_count'], 'integer'],
            [['content', 'audio_path', 'base_url', 'photo_path', 'photo_base_url', 'duration', 'published_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsAudio::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lang_id' => $this->lang_id,
            'code' => $this->code,
            'status' => $this->status,
            'user_id' => $this->user_id,
            'author_id' => $this->author_id,
            'listen_count' => $this->listen_count,
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'content', $this->content])
            ->andFilterWhere(['ilike', 'audio_path', $this->audio_path])
            ->andFilterWhere(['ilike', 'base_url', $this->base_url])
            ->andFilterWhere(['ilike', 'photo_path', $this->photo_path])
            ->andFilterWhere(['ilike', 'photo_base_url', $this->photo_base_url])
            ->andFilterWhere(['ilike', 'duration', $this->duration]);

        return $dataProvider;
    }
}
