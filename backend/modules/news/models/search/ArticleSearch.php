<?php

namespace backend\modules\news\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NewsArticle;

/**
 * ArticleSearch represents the model behind the search form about `common\models\NewsArticle`.
 */
class ArticleSearch extends NewsArticle
{
    
    public $category_title;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'tab_number', 'content_type', 'views_count', 'status', 'author_id', 'user_id'], 'integer'],
            [['title', 'description', 'content', 'open_content', 'image_path', 'image_base_url', 'published_at', 'created_at', 'updated_at', 'more_option', 'slug'], 'safe'],
            [['is_actual', 'is_selected', 'is_ads', 'is_premium'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = NewsArticle::find()
            ->select([
            'news_article.id',
            'tab_number',
            'news_article.title',
            'published_at',
            'news_article.status',
            'views_count',
            'news_category.title as category_title'
          ])->joinWith('category',false, 'Left JOIN');

        //$query->orderBy(['created_at'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'tab_number' => $this->tab_number,
            'content_type' => $this->content_type,
            'is_actual' => $this->is_actual,
            'is_selected' => $this->is_selected,
            'is_ads' => $this->is_ads,
            'is_premium' => $this->is_premium,
            'views_count' => $this->views_count,
            'status' => $this->status,
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author_id' => $this->author_id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'content', $this->content])
            ->andFilterWhere(['ilike', 'open_content', $this->open_content])
            ->andFilterWhere(['ilike', 'image_path', $this->image_path])
            ->andFilterWhere(['ilike', 'image_base_url', $this->image_base_url])
            ->andFilterWhere(['ilike', 'more_option', $this->more_option])
            ->andFilterWhere(['ilike', 'slug', $this->slug]);

        return $dataProvider;
    }
}
