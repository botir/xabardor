$(function() {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

    
    if($('#audio_news_s').length){

      Amplitude.init({
            "bindings": {
              37: 'prev',
              39: 'next',
              32: 'play_pause'
            },
            "songs": [
              {
                //"name": "Risin' High (feat Raashan Ahmad)",
                //"artist": "Ancient Astronauts",
                "album": "Kun.uz",
                "url": $('#audio_news_s').attr('file'),
                "cover_art_url": $('#audio_news_s').attr('file_photo'),
              }
            ]
          });

          window.onkeydown = function(e) {
              return !(e.keyCode == 32);
          };

          /*
            Handles a click on the song played progress bar.
          */
          document.getElementById('song-played-progress').addEventListener('click', function( e ){
            var offset = this.getBoundingClientRect();
            var x = e.pageX - offset.left;

            Amplitude.setSongPlayedPercentage( ( parseFloat( x ) / parseFloat( this.offsetWidth) ) * 100 );
          });

  }

    if( $('#dynamic-container').length ){
        $("#dynamic-container .btn-success").click(function(e){
            var $clone_container = $(this).closest('.field-container').clone();

            $clone_container.find('input').val('');
            $clone_container.find('.btn-success').text('-');
            $clone_container.find('button').removeClass('btn-success').addClass('btn-danger');


            $('#dynamic-container').append($clone_container);
            $('.btn-danger', $clone_container).on("click", function() {
                $(this).closest('.field-container').remove();
            });
        });

        $('#dynamic-container .btn-danger').on('click', function (e) {
            $(this).closest('.field-container').remove();
        });

    }



})