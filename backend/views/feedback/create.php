<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FeedbackMessage */

$this->title = 'Create Feedback Message';
$this->params['breadcrumbs'][] = ['label' => 'Feedback Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-message-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
