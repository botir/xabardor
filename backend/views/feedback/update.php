<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FeedbackMessage */

$this->title = 'Update Feedback Message: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Feedback Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="feedback-message-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
