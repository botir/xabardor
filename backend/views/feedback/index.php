<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feedback Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-message-index">


    <p>
        <?php echo Html::a('Create Feedback Message', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'subject',
            'message_txt:ntext',
            'phone',
            'email:email',
            // 'full_name',
            // 'user_id',
            // 'is_mobile:boolean',
            // 'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
