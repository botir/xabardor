<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PageStatic */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Page Statics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-static-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'content_type',
            'views_count',
            'is_active:boolean',
            'is_menu:boolean',
            'slug',
            'view_file',
            'user_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>


    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title">uz</h3>
                </div>
                <div class="box-body">
                    <?=$model->title_uz; ?>
                    <br />
                       
                    <?=$model->content_uz?>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid box-success">
            <div class="box-header">
                <h3 class="box-title">kr</h3>
            </div>
            <div class="box-body">
                <?=$model->title_kr; ?>
                <br />
                   
                <?=$model->content_kr?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid box-success">
            <div class="box-header">
                <h3 class="box-title">ru</h3>
            </div>
            <div class="box-body">
                <?=$model->title_ru; ?>
                <br />
                   
                <?=$model->content_ru?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid box-success">
            <div class="box-header">
                <h3 class="box-title">en</h3>
            </div>
            <div class="box-body">
                <?=$model->title_en; ?>
                <br />
                   
                <?=$model->content_en?>
            </div>
        </div>
    </div>
</div>