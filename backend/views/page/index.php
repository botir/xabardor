<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Static pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-static-index">


    <p>
        <?php echo Html::a('Создать страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title_uz',
            [
                'attribute'=>'content_type',
                'value'=>function ($model) {
                    return $model->getContentType();
                }
            ],
            'is_active:boolean',
            'slug',
            [
            'class' => 'yii\grid\ActionColumn',
            'template'=>'{view}{update}'
            ]
        ],
    ]); ?>

</div>
