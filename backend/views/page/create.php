<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PageStatic */

$this->title = 'Create Page Static';
$this->params['breadcrumbs'][] = ['label' => 'Page Statics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-static-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
