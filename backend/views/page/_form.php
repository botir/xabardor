<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mihaildev\elfinder\ElFinder;
use backend\widgets\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\PageStatic */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="page-static-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-3">
            <?php echo $form->field($model, 'content_type')->dropDownList(\common\models\PageStatic::getTypeList(), ['prompt'=>'']) ?>
        </div>
        <div class="col-sm-3">
            <?php echo $form->field($model, 'is_active')->checkbox() ?>
        </div>
         <div class="col-sm-3">
            <?php echo $form->field($model, 'view_file')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
         'clientOptions' => ElFinder::ckeditorOptions('elfinder',[
            'allowedContent'=>[
                'script'=>true,
                'div'=>true,
                '$1'=>[
                    'elements: CKEDITOR.dtd',
                    'attributes'=>true,
                    'styles'=>true,
                    'classes'=>true,
                ],
            ]
            ])
        ]) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'content_uz')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
         'clientOptions' => ElFinder::ckeditorOptions('elfinder',[
            'allowedContent'=>[
                'script'=>true,
                'div'=>true,
                '$1'=>[
                    'elements: CKEDITOR.dtd',
                    'attributes'=>true,
                    'styles'=>true,
                    'classes'=>true,
                ],
            ]
            ])
        ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'title_kr')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'content_kr')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
         'clientOptions' => ElFinder::ckeditorOptions('elfinder',[
            'allowedContent'=>[
                'script'=>true,
                'div'=>true,
                '$1'=>[
                    'elements: CKEDITOR.dtd',
                    'attributes'=>true,
                    'styles'=>true,
                    'classes'=>true,
                ],
            ]
            ])
        ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'content_ru')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
         'clientOptions' => ElFinder::ckeditorOptions('elfinder',[
            'allowedContent'=>[
                'script'=>true,
                'div'=>true,
                '$1'=>[
                    'elements: CKEDITOR.dtd',
                    'attributes'=>true,
                    'styles'=>true,
                    'classes'=>true,
                ],
            ]
            ])
        ]) ?>
        </div>
    </div>

     <div class="row">
        <div class="col-sm-6">
            <?php echo $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'content_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
         'clientOptions' => ElFinder::ckeditorOptions('elfinder',[
            'allowedContent'=>[
                'script'=>true,
                'div'=>true,
                '$1'=>[
                    'elements: CKEDITOR.dtd',
                    'attributes'=>true,
                    'styles'=>true,
                    'classes'=>true,
                ],
            ]
            ])
        ]) ?>
        </div>
    </div>


    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранит', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
