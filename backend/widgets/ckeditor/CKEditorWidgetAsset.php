<?php
namespace backend\widgets\ckeditor;

use yii\web\AssetBundle;

class CKEditorWidgetAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/js/ckeditor';

    public $depends = [
        'backend\widgets\ckeditor\CKEditorAsset'
    ];

    public $js = [
        'dosamigos-ckeditor.widget.js'
    ];
}
