<?php
use yii\helpers\Html;
$static_url = Yii::getAlias('@apiUrl').'/static';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8"/>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Apple devices fullscreen -->
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" />
    <title><?php echo Html::encode($this->title) ?></title>
    <style type="text/css">
@font-face {
    font-family: ProximaNova-Regular;
    font-weight: 400;
    font-style: normal;
    src: url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Regular/ProximaNova-Regular.eot);
    src: url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Regular/ProximaNova-Regular.eot?#iefix) format("embedded-opentype"), url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Regular/ProximaNova-Regular.woff) format("woff"), url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Regular/ProximaNova-Regular.ttf) format("truetype")
}

@font-face {
    font-family: ProximaNova-Bold;
    font-weight: 700;
    font-style: normal;
    src: url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Bold/ProximaNova-Bold.eot);
    src: url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Bold/ProximaNova-Bold.eot?#iefix) format("embedded-opentype"), url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Bold/ProximaNova-Bold.woff) format("woff"), url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Bold/ProximaNova-Bold.ttf) format("truetype")
}

@font-face {
    font-family: ProximaNova-Semibold;
    font-weight: 600;
    font-style: normal;
    src: url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Semibold/ProximaNova-Semibold.eot);
    src: url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Semibold/ProximaNova-Semibold.eot?#iefix) format("embedded-opentype"), url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Semibold/ProximaNova-Semibold.woff) format("woff"), url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Semibold/ProximaNova-Semibold.ttf) format("truetype")
}

@font-face {
    font-family: ProximaNova-Light;
    font-weight: 300;
    font-style: normal;
    src: url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Light/ProximaNova-Light.eot);
    src: url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Light/ProximaNova-Light.eot?#iefix) format("embedded-opentype"), url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Light/ProximaNova-Light.woff) format("woff"), url(<?=$static_url?>/fonts/ProximaNova/ProximaNova-Light/ProximaNova-Light.ttf) format("truetype")
}

@font-face {
    font-family: Montserrat-Light;
    font-weight: 300;
    font-style: normal;
    src: url(<?=$static_url?>/fonts/Montserrat/Montserrat-Light/Montserrat-Light.eot);
    src: url(<?=$static_url?>/fonts/Montserrat/Montserrat-Light/Montserrat-Light.eot?#iefix) format("embedded-opentype"), url(<?=$static_url?>/fonts/Montserrat/Montserrat-Light/Montserrat-Light.woff) format("woff"), url(<?=$static_url?>/fonts/Montserrat/Montserrat-Light/Montserrat-Light.ttf) format("truetype")
}

@font-face {
    font-family: Montserrat-ExtraBold;
    font-weight: 700;
    font-style: normal;
    src: url(<?=$static_url?>/fonts/Montserrat/Montserrat-ExtraBold/Montserrat-ExtraBold.eot);
    src: url(<?=$static_url?>/fonts/Montserrat/Montserrat-ExtraBold/Montserrat-ExtraBold.eot?#iefix) format("embedded-opentype"), url(<?=$static_url?>/fonts/Montserrat/Montserrat-ExtraBold/Montserrat-ExtraBold.woff) format("woff"), url(<?=$static_url?>/fonts/Montserrat/Montserrat-ExtraBold/Montserrat-ExtraBold.ttf) format("truetype")
}

@font-face {
    font-family: PTSerif-Italic;
    font-weight: 400;
    font-style: normal;
    src: url(<?=$static_url?>/fonts/PTSerif/PTSerif-Italic/PTSerif-Italic.eot);
    src: url(<?=$static_url?>/fonts/PTSerif/PTSerif-Italic/PTSerif-Italic.eot?#iefix) format("embedded-opentype"), url(<?=$static_url?>/fonts/PTSerif/PTSerif-Italic/PTSerif-Italic.woff) format("woff"), url(<?=$static_url?>/fonts/PTSerif/PTSerif-Italic/PTSerif-Italic.ttf) format("truetype")
}

body {
    font-size: 14px;
    line-height: 1.25;
    font-family: ProximaNova-Regular, sans-serif;
    color: #212121;
    background-color: #fcfaf9;
    /*font-family: Arial, Helvetica, sans-serif;color: #2c2c2c;*/
}
.post-img img {
    width:100%;
    float: left;
}
.post-title {
    margin-bottom: 10px;
    float: left;
    width: 100%;
}
.post p {
    width: 100%;
    color: #212121;
    margin-top: 0;
}

.post-details {margin-bottom: 0;}
.post-details .post-title {margin: 0 0 16px 0;}
.post-details .post-img {margin-bottom: 20px;}
.post-details .data {margin-bottom: 16px;}
.post-details .data a {margin-right: 16px;}
.post-details p {font-size: 15px;line-height: 1.6em; margin-bottom: 1rem;}
.post-details a.source-link {color: #ee4d4d;}
.post-details a.source-link:hover {text-decoration: underline;}
.day-mode{background-color: #ffffff;}
.night-mode{background-color: #2b2b2b;}
.day-mode p {color: #000000 !important;}
.night-mode p {color: #ffffff !important;}

.post-body{
    background-color: #fcfaf9;
}

.post-body img{
    display: block;
    max-width: 100%;
    height: auto;
}
.post-body a{
    text-decoration: none;
}
.embed-wrapper {
    position: relative;
    display: block;
    width: 100%;
    padding: 0;
    overflow: hidden
}
.embed-wrapper:before {
    display: block;
    content: "";
    padding-top: 56.25%
}

.embed-wrapper embed,
.embed-wrapper iframe,
.embed-wrapper object,
.embed-wrapper video {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: 0
}

iframe {
    display: block;
    max-width: 100%;
}

.post-details iframe {width: 100%;max-width: 100%;}
figure{
    margin: 0;
}
figure img{
    display: block;
    height: auto;
    width: 100%;
    margin: auto;
    max-width: 100%;
}
figcaption{
    background: #f4f4f4 none repeat scroll 0 0;
    color: #9c9c9c;
    font-size: 11px;
    font-style: normal;
    padding: 3px;
    text-align: center;
}
div{
    display: block;
    clear: both;
}
ul {
    list-style: none;
    padding: 0;
    margin: 0;
    padding-left: 1em;
    display: block;
    clear: both;
}
ul li {
    margin-bottom: 15px;
    position: relative;
    padding-left: 25px;
}
ul li:before {
    content: '';
    position: absolute;
    top: 7px;
    left: 0;
    width: 10px;
    height: 10px;
    background-color: #184FA8;
    border-radius: 50%;
}
    </style>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?php echo $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
