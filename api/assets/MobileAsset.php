<?php
namespace api\assets;

use yii\web\AssetBundle;

/**
 * Frontend application asset
 */
class MobileAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@api/web/static';
}
