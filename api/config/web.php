<?php
$config = [
    'homeUrl' => Yii::getAlias('@apiUrl'),
    'controllerNamespace' => 'api\controllers',
    'defaultRoute' => 'site/index',    
    'modules' => [
        'v1' => \api\modules\v1\Module::class
    ],
    'components' => [
        'errorHandler' => [
            'errorAction' => 'site/error'
        ],
        'response' => [
            'class' => yii\web\Response::class,
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
            'on beforeSend' => function ($event) {
                if ($event->sender->format=='json'){
                    $response = $event->sender;
                    if ($response->data !== null) {
                        $response->data = [
                            'data' => $response->data,
                        ];
                    }
                }
            },
        ],
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'class'=>'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableSession' => false,
        ],
    ]
];

return $config;
