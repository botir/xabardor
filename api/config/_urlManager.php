<?php
return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
		'GET v1/tags/list'						=> 	'v1/tag/index',
		'GET v1/articles/list'					=> 	'v1/article/index',
		'GET v1/articles/search'				=> 	'v1/search/index',
		'GET v1/articles/<short_code>/info' 	=>	'v1/article/view',
		'GET v1/articles/<short_code>/content'	=>	'v1/content/view',
		'GET v1/author/<nickname>/info'			=>	'v1/author/index',
		'GET v1/author/<nickname>/articles'		=>	'v1/author/articles',
		'GET v1/pages/<slug>'					=>	'v1/page/view',
		'GET v1/ad/<place>'						=>	'v1/ad/view',
    ]
];
