## maqolalar kontenti [article content]

*path:* http://apps.xabardor.uz/v1/articles/<shortCode>/content
*type:* GET

misol:
*path:* http://apps.xabardor.uz/v1/articles/788141/content
*type:* GET
agar berilgan zapros mofaqiyyatli yakkunlansa status=200 bo'ladi va sizga ma'lumotlar qaytadi.
qaytgan javob HTML formatda boladi.