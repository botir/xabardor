## maqolalar malumotlari [article info]

*path:* http://apps.xabardor.uz/v1/articles/<shortCode>/info
*type:* GET

misol:
*path:* http://apps.xabardor.uz/v1/articles/702033/info
*type:* GET
agar berilgan zapros mofaqiyyatli yakkunlansa status=200 bo'ladi va sizga ma'lumotlar qaytadi.
qaytgan javob JSON formatda boladi,
maqolalarni olishda "info" object ni ichida bo'ladi

qaytadigan javobning ko'rinishi:

Property | Description
---|---
`title` | maqola nomi
`description` | maqola xaqida qisqacha
`image` | maqola uchun rasm object
`image[orginal]` | maqolaning orginal rasmi
`image[thumb]` | maqolaning kichraytirilgan rasmi
`image[caption]` | maqolaning istochniki
`image[show]` | maqolani rasmini xabar ichida ko'rsatish
`image[showList]` | maqolani rasmini listda ko'rsatish
`published` | maqola chiqqan vat
`impressions` | maqola o'qilganlar soni
`isAds` | maqola reklama materialligi
`commentInfo` | maqola izohlar uchun
`commentInfo[show]` | maqola izohni ko'rsatish
`commentInfo[count]` | izohlar soni
`commentInfo[allow]` | izohga ruxsat
`likeCount` | maqola likelar soni
`shortCode` | maqola kodi
`shareLink` | maqola share qilish uchun link
`contentLink` | maqola to'liq matni uchun link
`tags` | maqolaga tegishli teglar
`author` | maqola muallifi
`related` | o'xshash maqolalar 

```json
{
    "data": {
        "info": [
            {
                "title": "Millioner bo‘la olishingizning 5 alomati ",
                "description": "Agar siz qachonlardir millioner bo‘lishni xohlasangiz (kim ham xohlamaydi deysiz?), hozir ayni vaqti.",
                "image": {
                    "orginal": "http://media.xabardor.uz/source/images/products/list_view1554718619032623.jpg",
                    "thumb": "http://media.xabardor.uz/source/thumbnails/_medium/images/products/list_view1554718619032623_medium.jpg",
                    "caption": null,
                    "show": false,
                    "showList": true
                },
                "published": "2019-04-08T15:16:59+05:00",
                "impressions": 6057,
                "isAds": false,
                "commentInfo": {
                    "show": false,
                    "count": 0,
                    "allow": false
                },
                "likeCount": 0,
                "shortCode": 702033,
                "shareLink": "http://xabardor.uz/702033",
                "contentLink": "http://apps.xabardor.uz/v1/articles/702033/content"
            }
        ],
        "author": {
            "fullName": "Xabardor Blogger",
            "nickname": "xabardor_uz",
            "description": "Xabardor.uz!",
            "avatar": "http://media.xabardor.uz/source/1/ShzjEJqqA2icmmFh09nENaZz6Ft08gsx.png",
            "hidden": true
        },
        "tags": [
            {
                "id": 78,
                "slug": "maslahat",
                "title": "Maslahat"
            }
        ],
        "related": [
            {
                "title": "Har qanday ish joyini tashkillashtirish uchun 7 maslahat",
                "description": "Ayrim tadqiqot natijalari ish joyidagi tartib ijodiy fikrlash va unumdorlikka bevosita ta'sir qilishini ko‘rsatgan. ",
                "image": {
                    "orginal": "http://media.xabardor.uz/source/1/ss26JBBMKGUx_qpIxagrBj65HaQ5oqHm.jpeg",
                    "thumb": "http://media.xabardor.uz/source/thumbnails/_medium/1/ss26JBBMKGUx_qpIxagrBj65HaQ5oqHm_medium.jpeg",
                    "caption": "",
                    "show": false,
                    "showList": true
                },
                "published": "2019-07-03T20:12:49+05:00",
                "impressions": 363,
                "isAds": false,
                "isSelected": false,
                "showDesc": true,
                "commentInfo": {
                    "show": false,
                    "count": 0,
                    "allow": false
                },
                "tags": [
                    {
                        "id": 78,
                        "slug": "foydali",
                        "title": "Foydali"
                    }
                ],
                "shortCode": 784926
            },
            {
                "title": "Kelajakda farzandlaringizga badavlat bo‘lishda yordam beradigan tarbiyaning 6 siri",
                "description": "G‘amxo‘r ota-onalar farzandlari muvaffaqiyatli, baxtli va badavlat bo‘lib ulg‘ayishlarini orzu qiladilar. Buning uchun bolaga tongdan kechga qadar o‘qish, dam olish kunlari esa uni shaharning barcha to‘garaklariga olib borish shart emas. ",
                "image": {
                    "orginal": "http://media.xabardor.uz/source/1/DJTu1yrh2YWBriknGXZzYKtg3k_tfB5g.jpg",
                    "thumb": "http://media.xabardor.uz/source/thumbnails/_medium/1/DJTu1yrh2YWBriknGXZzYKtg3k_tfB5g_medium.jpg",
                    "caption": "",
                    "show": false,
                    "showList": true
                },
                "published": "2019-07-03T20:06:31+05:00",
                "impressions": 803,
                "isAds": false,
                "isSelected": false,
                "showDesc": true,
                "commentInfo": {
                    "show": false,
                    "count": 0,
                    "allow": false
                },
                "tags": [
                    {
                        "id": 78,
                        "slug": "foydali",
                        "title": "Foydali"
                    }
                ],
                "shortCode": 463322
            }
        ]
    }
}
```
