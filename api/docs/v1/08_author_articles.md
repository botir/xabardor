## muallif maqolalari [author articles]

*path:* http://apps.xabardor.uz/v1/author/<nickname>/articles
*type:* GET

misol:
*path:* http://apps.xabardor.uz/v1/author/xabardor_uz/articles
*type:* GET

agar teg va muallif bilan filter qilish kerak bo'lsa:
*path:* http://apps.xabardor.uz/v1/author/xabardor_uz/articles?f=tag&v=kompaniya
*type:* GET

agar berilgan zapros mofaqiyyatli yakkunlansa status=200 bo'ladi va sizga ma'lumotlar qaytadi.
qaytgan javob JSON formatda boladi.

qaytadigan javobning ko'rinishi:

Property | Description
---|---
`title` | maqola nomi
`description` | maqola xaqida qisqacha
`image` | maqola uchun rasm object
`image[orginal]` | maqolaning orginal rasmi
`image[thumb]` | maqolaning kichraytirilgan rasmi
`image[caption]` | maqolaning istochniki
`image[show]` | maqolani rasmini xabar ichida ko'rsatish
`image[showList]` | maqolani rasmini listda ko'rsatish
`published` | maqola chiqqan vat
`impressions` | maqola o'qilganlar soni
`isAds` | maqola reklama materialligi
`isSelected` | tanlangan maqola
`showDesc` | maqola description ni ko'rsatish 
`commentInfo` | maqola izohlar uchun
`commentInfo[show]` | maqola izohni ko'rsatish
`commentInfo[count]` | izohlar soni
`commentInfo[allow]` | izohga ruxsat
`tags` | maqolaga tegishli teglar
`tags[id]` | teg id
`tags[slug]` | teg slug
`tags[title]` | teg nomi
`shortCode` | maqola kodi

```json
{
    "data": {
        "articles": [
           {
                "title": "Dunyodagi eng baland daraxtlar. Chinor nechanchi o‘rinda?",
                "description": "Daraxtlar tabiatning ajralmas bo‘lagi, zaminning nafas tizimidir. O‘rtacha bir daraxt o‘rtacha bir oilaning kislorodga bo‘lgan ehtiyojini qoplaydi. Demak bitta daraxtni qulatish bir oilani toza havodan mahrum qilmoq demakdir.\r\n\r\n",
                "image": {
                    "orginal": "http://media.xabardor.uz/source/1/fFLql-O3ii54U697fJj2fTK19RTDw5go.jpg",
                    "thumb": "http://media.xabardor.uz/source/thumbnails/_medium/1/fFLql-O3ii54U697fJj2fTK19RTDw5go_medium.jpg",
                    "caption": "",
                    "show": false,
                    "showList": true
                },
                "published": "2019-07-03T23:56:36+05:00",
                "impressions": 735,
                "isAds": false,
                "isSelected": false,
                "showDesc": true,
                "commentInfo": {
                    "show": false,
                    "count": 0,
                    "allow": false
                },
                "tags": [
                    {
                        "id": 121,
                        "slug": "qiziqarli",
                        "title": "Qiziqarli"
                    }
                ],
                "shortCode": 669763
            }
        ],
        "pageInfo": {
            "totalCount": 3,
            "nextPage": null
        }
    }
}
```

