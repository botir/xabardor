## Access Control

API larga zapros berish uchun filterdan o'tishi kerak , buning uchun header qismida , CURRENT TIME VA GENERATED TOKEN kerak boladi

bu qanday yasaladi:
 key = 'KXab@UzG*LoBall*Acc@ss^&sv5'  					[ bu konstatnta]
 time =  1539795309								            [xozirgi vaqting millisecundagi korinishi]
 token = md5(key.time)                              [key va time ni birbiriga qoshib md5 qilinadi]

 xar safar token key yangi yaratilib yuborilishi kerak. xozirgi vaqtni olgan xolda
shunday ko'rinishda keyin yuboriladi 
```
 headers = {
    'token': "09t1b01ff3fd1b880360efbabf33f7e2",
    'time': "1439799309"
}
```
agar parametrlar xato yuborilsa yoki yuborilmasa:

STATUS = 403 Forbidden
```
{
    "data": {
        "name": "Forbidden",
        "message": "You are not allowed to perform this action.",
        "code": 0,
        "status": 403
    }
}
```