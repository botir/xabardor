## Teglar ro'yxati [tag list]

*path:* http://apps.xabardor.uz/v1/tags/list?f=menu
*type:* GET

agar berilgan zapros mofaqiyyatli yakkunlansa status=200 bo'ladi va sizga ma'lumotlar qaytadi.
qaytgan javob JSON formatda boladi,
Yanglikni olishda "tags" object ni ichida bo'ladi

"items" - 20 tadan qaytib keladi....
boshqa pagelarga o'tishda nextPage dagi qiymat orqali "next" filter berib o'tish mumkin. agar nextPage *null* bo'lsa keyingi page yo'q degani

qaytadigan javobning ko'rinishi:

Property | Description
---|---
`id` | teg id nomeri
`title` | teg nomi
`slug` | teg slug


```json
{
    "data": {
        "tags": [
            {
                "id": 78,
                "title": "Foydali",
                "slug": "foydali"
            },
            {
                "id": 121,
                "title": "Qiziqarli",
                "slug": "qiziqarli"
            },
            {
                "id": 108,
                "title": "Xorij",
                "slug": "xorij"
            }
        ],
        "pageInfo": {
            "totalCount": 3,
            "next_page": null
        }
    }
}
```
