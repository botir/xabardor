## muallif xaqida [author info]

*path:* http://apps.xabardor.uz/v1/author/<nickname>/info
*type:* GET

misol:
*path:* http://apps.xabardor.uz/v1/author/xabardor_uz/info
*type:* GET
agar berilgan zapros mofaqiyyatli yakkunlansa status=200 bo'ladi va sizga ma'lumotlar qaytadi.
qaytgan javob JSON formatda boladi.

qaytadigan javobning ko'rinishi:

Property | Description
---|---
`author` | object
`author[fullName]` | Muallif ism, familiyasi.
`author[nickname]` | muallifi nick nomi
`author[description]` | status
`author[avatar]` | avatar
`author[hidden]` | maqola ichida muallifni ko'rsatish yoki ko'rsatmaslik
`tags` | muallif ishlatgan teglar. Ko'p bo'lishi xam mumkin

```json
{
    "data": {
        "author": {
            "fullName": "Xabardor Blogger",
            "nickname": "xabardor_uz",
            "description": "Xabardor.uz!",
            "avatar": "http://media.xabardor.uz/source/1/ShzjEJqqA2icmmFh09nENaZz6Ft08gsx.png",
            "hidden": true
        },
        "tags": [
            {
                "id": 81,
                "title": "Startap",
                "slug": "startap"
            },
            {
                "id": 121,
                "title": "Qiziqarli",
                "slug": "qiziqarli"
            }
        ],
        "countArticles": 5963
    }
}
```
