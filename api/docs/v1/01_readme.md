## Немного введения 
API сервиса представляет собой классическое [restful api](https://ru.wikipedia.org/wiki/REST). 
Далее будут представлены эндпоинты, для каждого будет указан `path`; данный `path` нужно сконкатенировать с доменом сервиса, например:
  
  http://apps.xabardor.uz
  
Если случается ошибка, то в ответ возвращается `http-response` со статусом , в теле которого находится `json`, в котором: например:
```
{
  "data": {
    "name": "Not Found",
    "message": "Саҳифа топилмади.",
    "code": 0,
    "status": 404    
  }
}
```
```
{
  "data": {
    "name": "Forbidden",
    "message": "Вам не разрешено производить данное действие.",
    "code": 0,
    "status": 403
  }
}
```

каждого ответ внутри есть "дата". 

линк для получение АЙПИ:
http://apps.xabardor.uz
