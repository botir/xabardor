## maqolalar qidiruvi [article search]

*path:* http://apps.xabardor.uz/v1/articles/search
*type:* GET

qidiruvda *q* paramteri qoshiladi/
misol:
*path:* http://apps.xabardor.uz/v1/articles/search?q=Millionerlardan maslahat
*type:* GET
agar berilgan zapros mofaqiyyatli yakkunlansa status=200 bo'ladi va sizga ma'lumotlar qaytadi.
qaytgan javob JSON formatda boladi,
maqolalarni olishda "articles" object ni ichida bo'ladi

"items" - 12 tadan qaytib keladi....
boshqa pagelarga o'tishda nextPage dagi qiymat orqali "next" filter berib o'tish mumkin. agar nextPage *null* bo'lsa keyingi page yo'q degani

qaytadigan javobning ko'rinishi:

Property | Description
---|---
`title` | maqola nomi
`description` | maqola xaqida qisqacha
`image` | maqola uchun rasm object
`image[orginal]` | maqolaning orginal rasmi
`image[thumb]` | maqolaning kichraytirilgan rasmi
`image[caption]` | maqolaning istochniki
`image[show]` | maqolani rasmini xabar ichida ko'rsatish
`image[showList]` | maqolani rasmini listda ko'rsatish
`published` | maqola chiqqan vat
`impressions` | maqola o'qilganlar soni
`isAds` | maqola reklama materialligi
`isSelected` | tanlangan maqola
`showDesc` | maqola description ni ko'rsatish 
`commentInfo` | maqola izohlar uchun
`commentInfo[show]` | maqola izohni ko'rsatish
`commentInfo[count]` | izohlar soni
`commentInfo[allow]` | izohga ruxsat
`tags` | maqolaga tegishli teglar
`tags[id]` | teg id
`tags[slug]` | teg slug
`tags[title]` | teg nomi
`shortCode` | maqola kodi

```json
{
    "data": {
        "articles": [
           {
                "title": "Millioner bo‘la olishingizning 5 alomati ",
                "description": "Agar siz qachonlardir millioner bo‘lishni xohlasangiz (kim ham xohlamaydi deysiz?), hozir ayni vaqti.",
                "image": {
                    "orginal": "http://media.xabardor.uz/source/images/products/list_view1554718619032623.jpg",
                    "thumb": "http://media.xabardor.uz/source/thumbnails/_medium/images/products/list_view1554718619032623_medium.jpg",
                    "caption": null,
                    "show": false,
                    "showList": true
                },
                "published": "2019-04-08T15:16:59+05:00",
                "impressions": 6057,
                "isAds": false,
                "isSelected": false,
                "showDesc": true,
                "commentInfo": {
                    "show": false,
                    "count": 0,
                    "allow": false
                },
                "tags": [
                    {
                        "id": 78,
                        "slug": "maslahat",
                        "title": "Maslahat"
                    }
                ],
                "shortCode": 702033
            },
        ],
        "pageInfo": {
            "totalCount": 3,
            "nextPage": null
        }
    }
}
```
