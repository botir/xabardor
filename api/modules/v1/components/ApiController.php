<?php
namespace api\modules\v1\components;

use Yii;
use yii\web\Response;
use yii\rest\Controller;
use yii\helpers\Url;
use yii\helpers\Json;


class ApiController extends Controller
{
	const LIST_LIMIT = 20;
	const ARTICLE_LIMIT = 12;
	public $next;

	public function behaviors()
  	{
  		$behaviors = parent::behaviors();
      	$behaviors['contentNegotiator'] = [
        	'class' => 'yii\filters\ContentNegotiator',
        	'formats' => [
            	'application/json' => Response::FORMAT_JSON,
            	'charset' => 'UTF-8',
          	]
      	];
      	$behaviors['access'] = [
        	'class' => AccessControl::className(),
      	];

    	return $behaviors;
  	}

  	public function beforeAction($action)
  	{
		if (isset($_GET['next'])){
      		try {
        		$this->next = date('Y-m-d H:i:s', $_GET['next']);
      		} 
      		catch (\Exception $e) {
        		$this->next = '2030-12-31';
      		}
    	}
    	else
      		$this->next = '2030-12-31';
    	return parent::beforeAction($action);
  	}


  	public function getFormatNews($news){
    	$result = [];
      	foreach ($news as $item) {
      		$option = $this->getOptionArticle($item['more_option']);
      		if ($item['content_type'] == 1 && $item['list_image']){
      			$list_image = true;
      		}elseif($item['list_image']){
      			$list_image = true;
      		}else{
      			$list_image = false;
      		}
       		$result[] = [
          		'title' => $item['title'],
          		'description' => $item['description'],
          		'image' => [
            		'orginal' => $item['image_base_url'].$item['image_path'],
            		'thumb' => $this->getListImage($item),
            		'caption' => $option['caption_image'],
            		'show' => $option['show_image'],
            		'showList' => $list_image,
          		],
          		'published'=>  date("c", $item['unix_timestamp']),
          		'impressions'=> $item['views_count'],
          		'isAds'=> $item['is_ads'],
          		'isSelected'=> $item['is_selected'],
          		'showDesc'=> $item['show_desc'],
          		'commentInfo' => [
          			'show' => false,
          			'count' => 0,
          			'allow' => false,
          		],
          		'tags' => $this->getTags($item['more_option']),
              'shortCode' => $item['tab_number']
        	];
      }
      return $result;
  	}

    public function getInfoNews($data){
      $option = $this->getOptionArticle($data['more_option']);      
      $result = [
        'title' => $data['title'],
        'description' => $data['description'],
        'image' => [
          'orginal' => $data['image_base_url'].$data['image_path'],
          'thumb' => $this->getListImage($data),
          'caption' => $option['caption_image'],
          'show' => $option['show_image'],
          'showList' => true
        ],
        'published'=>  date("c", $data['unix_timestamp']),
        'impressions'=> $data['views_count'],
        'isAds'=> $data['is_ads'],
        'commentInfo' => [
          'show' => false,
          'count' => 0,
          'allow' => false
        ],
        'likeCount' => $option['like_count'],
        'shortCode' => $data['tab_number'],
        'shareLink' => Yii::getAlias('@frontendUrl').'/x/'.$data['tab_number'],
        'contentLink' => Url::toRoute(['/v1/content/view', 'short_code'=>$data['tab_number']], true)
      ];
      return $result;
    }

  	protected function getTags($json_data){
        $array_data = Json::decode($json_data);
        if (isset($array_data['tags']) && count($array_data['tags']) > 0){
            return $array_data['tags'];
        }else{
            return false;
        }
    }

    protected function getListImage($data){
        $mode = '_medium';
        return $data['image_base_url'].'/thumbnails/'.$mode.$data['image_name'].$mode.'.'.$data['image_ext'];
    }

    public function getOptionArticle($json_data){
        $array_data = Json::decode($json_data);
        return $array_data;
    }

  	protected function getLoadMore($obj)
    {
    	$count = count($obj);
    	if ($count > 0){
      		$dt1 = \DateTime::createFromFormat('Y-m-d H:i:s', $obj[$count-1]['published_at']);
      		$time = strtotime($dt1->format('Y-m-d H:i:s'));
      		return ['next'=>$time, 'pub'=>$obj[$count-1]['published_at'], 'count'=>$count];
    	}
    	else
      		return ['next'=>null,'pub'=>null,'count'=>0];
    }
}