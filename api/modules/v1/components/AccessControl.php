<?php
/**
 * Class AccessControl
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */

namespace api\modules\v1\components;

use Yii;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;


class AccessControl extends ActionFilter
{
    public $ruleConfig = ['class' => 'yii\filters\AccessRule'];

    public $rules = [];

    private $key='Xab@UzG*LoBall*Acc@ss^&sv5';

    public function beforeAction($action)
    {   

        $request = Yii::$app->getRequest();
        $time = $request->getHeaders()->get('time');
        $token = $request->getHeaders()->get('token');
        $key  = $this->key;
        if ($time !== null&&$token!==null){
            $k = md5($key.$time);
            if ($k==$token){
                return true;
            }
            else {
                throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
            }
        }

        throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));

    }
}