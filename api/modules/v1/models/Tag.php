<?php
namespace api\modules\v1\models;

use Yii;
use yii\db\Query;
use common\models\NewsTag;

class Tag extends NewsTag
{
	public static function filterList($limit=10, $type='all')
  	{
  		$query = (new Query())
        	->select('id, title, slug')
        	->from('news_tag');
        if ($type == 'all'){
        	$query = $query
        		->where('status=2')
        		->orderBy(['c_weight'=>SORT_DESC])
        		->limit($limit);
        }else{
        	$query = $query
        		->where('status=2 and is_menu is true')
        		->orderBy(['c_weight'=>SORT_DESC])
        		->limit($limit);	
        }
        
    	return $query->all();
  	}

    public static function getOne($slug)
    {
        $query = (new Query())
            ->select('title, slug, id')
            ->from('news_tag')
            ->where('status=2 and slug=:slug')
            ->orderBy(['c_weight'=>SORT_DESC])
            ->params([':slug' =>$slug]);
        return $query->one();
    }
}
