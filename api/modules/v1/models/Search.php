<?php
namespace api\modules\v1\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;

class Search extends Model
{
    public $q = '';

    public function attributeLabels()
    {
        return [
            'q' => \Yii::t('app', 'Do Search') . '...'
        ];
    }

    public function rules()
    {
        return [
            ['q', 'string', 'min' => 3, 'skipOnEmpty' => false],
        ];
    }

    public function searchNews($next, $limit)
    {
        $arrs = explode(" ",mb_strtolower($this->q, 'UTF-8'));
        $query_text = "news_article.status = 2 and published_at<'".$next."'";
        $iq = 0;

        foreach ($arrs as $value) {
            $one_str = str_replace("'", "%''%", $value);

            if ($one_str && $one_str!=''){
                if ($iq > 0 ){
                    $query_text.= " & ".$one_str.":*";
                }
                else{
                    $query_text .= " and (document_vector @@ tsquery('".$one_str.":*";
                }
                $iq++;
            }
        }
        $query_text.="'))";

        $count_data = $this->counterSearchNews($query_text);
        if ($count_data > 0){
        $data = (new Query())
            ->select('tab_number, title, description, slug, "published_at"::timestamp, more_option, ("more_option"->>\'list_image\')::boolean AS "list_image", ("more_option"->>\'show_desc\')::boolean AS "show_desc", content_type, is_ads, is_selected, views_count, comment_count, image_base_url, image_path,  LEFT(image_path, POSITION(\'.\' IN image_path)-1) as image_name, right(image_path, POSITION(\'.\' IN REVERSE(image_path)) - 1) as image_ext, extract(epoch from news_article.published_at) as unix_timestamp')
            ->from('news_article')
            ->innerJoin('news_content','news_article.id=news_content.article_id')
            ->where($query_text)
            ->orderBy(['published_at'=>SORT_DESC])
            ->limit($limit)
            ->all();
        }else{
            $data = [];
        }

        $result = [
            'data' => $data,
            'counter' => $this->counterSearchNews($query_text)
        ];
       return $result;
    }

    public function counterSearchNews($query_text){

        $result = (new Query())
            ->select('count(1) as cnt')
            ->from('news_article')
            ->innerJoin('news_content','news_article.id=news_content.article_id')
            ->where($query_text)
            ->one();

        if ($result){
            return intval($result['cnt']);
        }else{
            return 0;
        }
    }

}
