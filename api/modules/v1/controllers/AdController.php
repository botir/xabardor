<?php
namespace api\modules\v1\controllers;

use yii\web\Response;
use yii\web\NotFoundHttpException;
use api\modules\v1\components\ApiController;

/**
 * Class AdController
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class AdController extends ApiController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'text/html' => Response::FORMAT_HTML,
                'charset' => 'UTF-8',
            ]
        ];
      return $behaviors;
    }

    public function actionView($place){
        $this->layout = "//no_style";
        if ($place == 'top')
            return $this->render('ad_top');
        elseif($place == 'center')
            return $this->render('ad_center');
        else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
