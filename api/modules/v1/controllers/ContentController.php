<?php
namespace api\modules\v1\controllers;

use yii\web\Response;
use yii\web\NotFoundHttpException;
use api\modules\v1\components\ApiController;
use api\modules\v1\models\Article;

/**
 * Class ContentController
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class ContentController extends ApiController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => Response::FORMAT_HTML,
                'charset' => 'UTF-8',
            ]
        ];
      return $behaviors;
    }

    public function actionView($short_code){
        $article = $this->findArticleContent($short_code);

        $result = [
            'content' => $article['content'],
            'title' => $article['title'],
        ];

        return $this->render('article', $result);
    }

    protected function findArticleContent($tab_number)
    {  
        $article = Article::getOneArticleContent($tab_number);
        if ($article !== null && $article) {
            return $article;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
