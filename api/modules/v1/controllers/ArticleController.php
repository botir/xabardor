<?php
namespace api\modules\v1\controllers;

use yii\helpers\Json;
use yii\web\NotFoundHttpException;

use api\modules\v1\components\ApiController;
use api\modules\v1\models\Article;
use api\modules\v1\models\Tag;

/**
 * Class ArticleController
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class ArticleController extends ApiController
{
    /**
     * @return array|null|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function actionIndex()
    {
        if (isset($_GET['f']))
            $type = $_GET['f'];
        else{
            $type = 'last';
        }
        $next_page = $this->next;
        if ($type == 'last'){
            $result = $this->getLatest($next_page);
        }elseif ($type == 'main'){
            $result = $this->getMain();
        }elseif($type == 'selected'){
            $result = $this->getSelected();
        }elseif($type == 'top'){
            $result = $this->getTopNews();
        }elseif($type == 'tag' && isset($_GET['v'])){
            $result = $this->getTagNews($_GET['v'], $next_page);
        }else{
            $result=[];            
        }

        return $result;
    }


    public function actionView($short_code){
        $article = $this->findArticleTabNumber($short_code);
        Article::updateViews($article['id']);
        $array_data = Json::decode($article['more_option']);
        if (isset($array_data['tags']) && count($array_data['tags']) > 0){
            $tags = $array_data['tags'];
        }else{
            $tags  = [];
        }

        $author_info = [
            'fullName' => $article['full_name'],
            'nickname' => $article['nickname'],
            'description' => $article['user_status'],
            'avatar' => $article['avatar_base_url'].$article['avatar_path'],
            'hidden' => true,
        ];

        $related = Article::getRelatedArticles($article['id'], $tags);

        $result = [
            'info' => $this->getInfoNews($article),
            'author' => $author_info,
            'tags' => $tags,
            'related' => $this->getFormatNews($related),
        ];

        return $result;
    }

    protected function getLatest($next_page){
        $data = Article::listArticles(1, $next_page, self::ARTICLE_LIMIT);
        $counters = Article::countersArticles(1, $next_page);
        $last_item = $this->getLoadMore($data);
        $result = [
            'articles' => $this->getFormatNews($data),
            'pageInfo' => [
                'totalCount'=> $counters,
                'nextPage'=> $last_item['next']
            ]
        ];
        return $result;
    }

    protected function getMain(){
        $data = Article::getMainNews(2, 4);
        $last_item = $this->getLoadMore($data);
        $result = [
            'articles' => $this->getFormatNews($data),
            'pageInfo' => [
                'totalCount'=> $last_item['count'],
                'nextPage'=> null
            ]
        ];
        return $result;
    }

    protected function getSelected(){
        $data = Article::getMainNews(3, 3);
        $last_item = $this->getLoadMore($data);
        $result = [
            'articles' => $this->getFormatNews($data),
            'pageInfo' => [
                'totalCount'=> $last_item['count'],
                'nextPage'=> null
            ]
        ];
        return $result;
    }

    protected function getTopNews(){
        $data = Article::listTopArticles(4);
        $last_item = $this->getLoadMore($data);
        $result = [
            'articles' => $this->getFormatNews($data),
            'pageInfo' => [
                'totalCount'=> $last_item['count'],
                'nextPage'=> null
            ]
        ];
        return $result;
    }

    protected function getTagNews($slug, $next_page){
        $tag = $this->findTag($slug);
        $data = Article::listTagArticles($tag['id'], $this->next, self::ARTICLE_LIMIT);
        $last_item = $this->getLoadMore($data);
        $counters = Article::countersTagArticle($tag['id'], $next_page);
         
        $result = [
            'articles' => $this->getFormatNews($data),
            'pageInfo' => [
                'totalCount'=> $counters,
                'nextPage'=> $last_item['next']
            ]
        ];
        return $result;
    }

    protected function findTag($slug)
    {  
        $tag = Tag::getOne($slug);
        if ($tag !== null && $tag) {
            return $tag;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findArticleTabNumber($tab_number)
    {  
        $article = Article::getOneArticlewithTab($tab_number);
        if ($article !== null && $article) {
            return $article;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
