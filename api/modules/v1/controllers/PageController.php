<?php
namespace api\modules\v1\controllers;

use yii\web\Response;
use yii\web\NotFoundHttpException;
use api\modules\v1\components\ApiController;
use common\models\PageStatic;

/**
 * Class PageController
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class PageController extends ApiController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => Response::FORMAT_HTML,
                'charset' => 'UTF-8',
            ]
        ];
        return $behaviors;
    }

    public function actionView($slug){
        if ($slug == 'contact'){
            return $this->render('contact');
        }

        $model = $this->findModel($slug);
        $result = [
            'content' => $model['content'],
            'title' => $model['title'],
        ];

        return $this->render('view', $result);
    }

    protected function findModel($slug)
    {
        if (($model = PageStatic::find()->where(['slug'=>$slug])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
