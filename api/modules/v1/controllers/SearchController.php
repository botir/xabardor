<?php
namespace api\modules\v1\controllers;

use yii\helpers\Json;
use yii\web\NotFoundHttpException;

use api\modules\v1\components\ApiController;
use api\modules\v1\models\Search;

/**
 * Class SearchController
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class SearchController extends ApiController
{

    /**
     * @return array|null|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function actionIndex()
    {
        $model = new Search();
        $model->load(\Yii::$app->request->get());
        $search_text = "";

        if (isset($_GET['q'])) 
            $model->q = $_GET['q'];
        if ($model->q && $model->q != ""){
            $search_text = strip_tags(stripslashes ( $model->q ));
            $model->q = $search_text;
            $data = $model->searchNews($this->next, self::ARTICLE_LIMIT);

            $last_item = $this->getLoadMore($data['data']);

            $result = [
                'articles' => $this->getFormatNews($data['data']),
                'pageInfo' => [
                    'totalCount'=> $data['counter'],
                    'nextPage'=> $last_item['next']
                ]
            ];
            return $result;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
