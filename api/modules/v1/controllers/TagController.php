<?php

namespace api\modules\v1\controllers;

use api\modules\v1\components\ApiController;
use api\modules\v1\models\Tag;

/**
 * Class TagController
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class TagController extends ApiController
{
    /**
     * @return array|null|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function actionIndex()
    {
        if (isset($_GET['f']))
            $type = $_GET['f'];
        else{
            $type = 'menu';
        }
        $data = Tag::filterList(self::LIST_LIMIT, $type);
        return [
            'tags' => $data,
            'pageInfo' => [
                'totalCount'=>count($data),
                'nextPage'=>null
            ]
        ];
    }
}
