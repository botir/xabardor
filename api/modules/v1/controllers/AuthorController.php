<?php
namespace api\modules\v1\controllers;

use yii\helpers\Json;
use yii\web\NotFoundHttpException;

use common\models\UserAuthor;
use api\modules\v1\components\ApiController;
use api\modules\v1\models\Article;
use api\modules\v1\models\Tag;

/**
 * Class AuthorController
 * @author Botir Ziyatov <botirziyatov@gmail.com>
 */
class AuthorController extends ApiController
{
    /**
     * @return array|null|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function actionIndex($nickname)
    {
        $author = $this->findAuthor($nickname);
        $author_article_count = Article::getCountAuthorArticles($author['id']);
        $tags = Article::listkolumnistTags($author['id']);

         $author_info = [
            'fullName' => $author['full_name'],
            'nickname' => $author['nickname'],
            'description' => $author['description'],
            'avatar' => $author['avatar_base_url'].$author['avatar_path'],
            'hidden' => true,
        ];

        $result = [           
            'author' => $author_info,
            'tags' => $tags,
            'countArticles' => $author_article_count,
        ];

        return $result;
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function actionArticles($nickname)
    {
        $author = $this->findAuthor($nickname);
        if (isset($_GET['f']))
            $type = $_GET['f'];
        else{
            $type = 'last';
        }
        $next_page = $this->next;
        if ($type == 'last'){
            $result = $this->getLatest($author['id'], $next_page);
        }elseif($type == 'tag' && isset($_GET['v'])){
            $result = $this->getByTag($author['id'], $_GET['v'], $next_page);
        }else{
            $result=[];            
        }
        return $result;
    }

    protected function getLatest($author_id, $next_page){
        $data = Article::listArticles(1, $next_page, self::ARTICLE_LIMIT, $author_id);
        $counters = Article::getCountlistedArticles(1, $next_page, $author_id);
        $last_item = $this->getLoadMore($data);
        $result = [
            'articles' => $this->getFormatNews($data),
            'pageInfo' => [
                'totalCount'=> $counters,
                'nextPage'=> $last_item['next']
            ]
        ];
        return $result;
    }

    protected function getByTag($author_id, $hashtag, $next_page){
        $tag = $this->findTag($hashtag);
        $data = Article::listTagArticles($tag['id'], $next_page, self::ARTICLE_LIMIT, $author_id);
        $counters = Article::countersTagArticle($tag['id'], $next_page, $author_id);
        $last_item = $this->getLoadMore($data);
        $result = [
            'articles' => $this->getFormatNews($data),
            'pageInfo' => [
                'totalCount'=> $counters,
                'nextPage'=> $last_item['next']
            ]
        ];
        return $result;
    }

    protected function findAuthor($nickname)
    {  
        $author = UserAuthor::nickOneAuthor($nickname);
        if ($author !== null && $author) {
            return $author;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findTag($slug)
    {  
        $tag = Tag::getOne($slug);
        if ($tag !== null && $tag) {
            return $tag;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
