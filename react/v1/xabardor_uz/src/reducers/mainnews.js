import * as types from 'constants/ActionTypes';

const initialState = {
  isFetching: false,
  items: [],
  nextUrl: null,
};

function mainnews(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_MAINNEWS_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case types.FETCH_MAINNEWS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: [...new Set([...state.items, ...action.items])],
        nextUrl: action.nextUrl,
      };
    default:
      return state;
  }
}

export default mainnews;