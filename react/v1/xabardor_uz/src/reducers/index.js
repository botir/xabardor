import { combineReducers } from 'redux';
import mainnews from 'reducers/mainnews';

const rootReducer = combineReducers({
  mainnews
});

export default rootReducer;
