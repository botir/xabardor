import { normalize } from 'normalizr';
import axios from 'axios';
import * as types from 'constants/ActionTypes';
import { mainnewsSchema } from 'constants/Schemas';

import { getMainnews } from 'selectors/CommonSelectors';

export const fetchmainNewsRequest = mainnews => ({
  type: types.FETCH_MAINNEWS_REQUEST,
  mainnews,
});

export const fetchmainNewsSuccess = (mainnews, items, entities, nextUrl) => ({
  type: types.FETCH_MAINNEWS_SUCCESS,
  entities,
  items,
  mainnews,
  nextUrl,
});

export const fetchNews = (newslist, url) => async (dispatch) => {
  dispatch(fetchmainNewsRequest(newslist));

  const json = axios({
    method: 'get',
    url: url,
    responseType: 'json'
  });
  console.log(json);

  //const { json } = await callApi(url);

  const news = json.collection || json;
  const nextUrl = json.nextHref || null;
  const { result, entities } = normalize(news, [mainnewsSchema]);

  dispatch(fetchmainNewsSuccess(newslist, result, entities, nextUrl));
};

export const fetchNewsIfNeeded = (newslist, newslistUrl) => (dispatch, getState) => {
  const state = getState();
  const newlists = getMainnews(state);
  const newslistExists = newslist in newlists;
  const newslistIsFetching = newslistExists ? newlists[newslist].isFetching : false;
  const newslistHasItems = newslistExists ? Boolean(newlists[newslist].items.length) : false;
  const shouldFetchNews = newslistUrl
     && (!newslistExists || (!newslistHasItems && !newslistIsFetching));

  if (shouldFetchNews) {
    dispatch(fetchNews(newslist, newslistUrl));
  }
};
