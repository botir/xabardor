import React from "react";
import MobileNav from "./MobileNav.jsx";

class TopMenu extends React.Component {

  render() {
    return (
      <React.Fragment>
      <header className="header wrapper__header">
        <div className="container">
          <div className="row">
            <div className="col-lg-2 col-md-2 col-sm-12">
              <div className="mobnav-container mobile-only">
                <button className="menu-toggle">
                  <i className="icon-menu-icon"></i>
                </button>
              </div>            
              <div className="logo">
                <h2 className="logo__text">
                  <a href="#" title="Xabardor">Xabardor</a>
                </h2>
              </div>
              <ul className="unstyled toplinks mobile-only">
                <li className="toplinks__item">
                  <a href="#signModal" className="toplinks__link">
                    <i className="toplinks__icon icon-user-icon"></i>
                    <span>Kirish</span>
                  </a>                
                  <a className="toplinks__link d-none" href="profile.html">
                    <img className="toplinks__img" src="images/user.jpg" alt="" />
                    <span>Odil Abdullaev</span>
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-lg-7 col-md-7 col-sm-12 desktop-only">
              <div className="xb_topcenter">
                <ul className="unstyled topmenu">
                  <li className="topmenu__item">
                    <a className="topmenu__link" href="#" title="">Lenta</a>
                    <span className="topmenu__badge">5</span>
                  </li>
                  <li className="topmenu__item">
                    <a className="topmenu__link" href="#" title="">Kolumnist</a>
                  </li>
                  <li className="topmenu__item">
                    <a className="topmenu__link" href="#" title="">Turizm</a>
                  </li>
                  <li className="topmenu__item">
                    <a className="topmenu__link" href="#" title="">Startup</a>
                  </li>
                  <li className="topmenu__item">
                    <a className="topmenu__link" href="#" title="">Busines</a>
                  </li>
                </ul>
                <form className="navbar-form navbar-form-search" role="search">
                  <div className="search-form-container hdn search-input-container">
                    <div className="search-input-group">
                      <div className="form-group mb-0">
                        <input type="text" className="form-control" name="search" placeholder="Qidirish" autoFocus />
                      </div>
                      <button type="button" className="btnClose hide-search-input-container">
                        <i className="icon-close-icon"></i>
                      </button>
                    </div>
                  </div>
                  <button type="submit" className="navbar-form__btn search-button">
                    <i className="navbar-form__icon icon-loupe-icon"></i>
                  </button>
                </form>
              </div>              
            </div>
            <div className="col-lg-3 col-md-3 col-sm-12 desktop-only">
              <ul className="unstyled toplinks">
                <li className="toplinks__item">
                  <a href="#signModal" className="toplinks__link">
                    <i className="toplinks__icon icon-user-icon"></i>
                    <span>Kirish</span>
                  </a>                
                  <a className="toplinks__link d-none" href="profile.html">
                    <img className="toplinks__img" src="images/user.jpg" />
                    <span>Odil Abdullaev</span>
                  </a>
                </li>
                <li className="toplinks__item">
                  <a className="toplinks__link upgrade" href="upgrade.html">
                    <span>Upgrade</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
      <MobileNav />
      </React.Fragment>
    );
  }
}

export default TopMenu;
