import React from "react";

class MobileNav extends React.Component {

  render() {
    return (
    <div className="mobnav-container mobile-only">
      <div className="mobile-menu">
        <div className="main-menu">
          <div className="container">
            <div className="">
              <form className="navbar-form navbar-form-search" role="search">
                <div className="search-form-container hdn search-input-container">
                  <div className="search-input-group">
                    <div className="form-group">
                      <input type="text" className="form-control" name="search" placeholder="Qidirish" autoFocus />
                    </div>
                    <button type="button" className="btnClose hide-search-input-container">
                      <i className="icon-close-icon"></i>
                    </button>
                  </div>
                </div>
                <button type="submit" className="navbar-form__btn search-button">
                  <i className="navbar-form__icon icon-loupe-icon"></i>
                </button>
              </form>
              <ul className="unstyled topmenu">
                <li className="topmenu__item topmenu__item--active">
                  <a className="topmenu__link" href="#" title="">Lenta</a>
                  <span className="topmenu__badge">5</span>
                </li>
                <li className="topmenu__item">
                  <a className="topmenu__link" href="#" title="">Kolumnist</a>
                </li>
                <li className="topmenu__item">
                  <a className="topmenu__link" href="#" title="">Turizm</a>
                </li>
                <li className="topmenu__item">
                  <a className="topmenu__link" href="#" title="">Startup</a>
                </li>
                <li className="topmenu__item">
                  <a className="topmenu__link" href="#" title="">Busines</a>
                </li>
              </ul> 
              <ul className="unstyled topmenu extramenu">            
                <li className="topmenu__item">
                  <a className="topmenu__link subsrb" href="#" title="">Obuna</a>
                  <a className="toplinks__link upgrade" href="upgrade.html">
                    <span>Upgrade</span>
                  </a>
                </li>
                <li className="topmenu__item">
                  <a className="topmenu__link" href="#" title="">Ko’p o’qilgan</a>
                </li>
                <li className="topmenu__item">
                  <a className="topmenu__link" href="#" title="">Ko’p kommentlar</a>
                </li>
                <li className="topmenu__item">
                  <a className="topmenu__link" href="#" title="">Muxarrir tanlovi</a>
                </li>
              </ul> 
            </div>    
          </div>
        </div>
        <div className="secondary-menu">
          <div className="container">
            <ul className="unstyled infoblock">
              <li className="infoblock__item infoblock__item--active">
                <a href="#" className="infoblock__link" title="">Biz haqimizda</a>
              </li>
              <li className="infoblock__item">
                <a href="#" className="infoblock__link" title="">RSS</a>
              </li>
              <li className="infoblock__item">
                <a href="#" className="infoblock__link" title="">Reklama</a>
              </li>
              <li className="infoblock__item">
                <a href="#" className="infoblock__link" title="">Biz bilan aloqa</a>
              </li>
              <li className="infoblock__item">
                <a href="#" className="infoblock__link" title="">Savol-javob</a>
              </li>
            </ul> 
          </div>
        </div>
      </div>
    </div>
    );
  }
}

export default MobileNav;
