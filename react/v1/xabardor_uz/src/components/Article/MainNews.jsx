import React from "react";

class MainNews extends React.Component {

  componentWillMount() {
    const { fetchNewsIfNeeded} = this.props;
    fetchNewsIfNeeded('dd', 'dddd');
  }

  // componentWillReceiveProps(nextProps) {
  //   const { fetchSongsIfNeeded, playlist } = this.props;
  //   if (playlist !== nextProps.playlist) {
  //     fetchSongsIfNeeded(nextProps.playlist, nextProps.playlistUrl);
  //   }
  // }

  render() {
    return (
          <div className="top-news">
        <div className="container">
          <div className="white-block">
            <div className="row">
              <div className="col-md-5 col-sm-12 col-12">
                <div className="bignews">
                  <a href="#" className="bignews__link">
                    <img className="bignews__img img-fluid" src="images/img01.jpg" alt="" />
                  </a>
                  <div className="bignews__body news-body">
                    <h5 className="news-body__title news-title">
                      <a href="#" title="">O‘zbekiston birjasida yevro 10 000 so‘mlik ko‘rsatkichga qaytdi</a>
                    </h5>
                    <p className="news-body__content news-content">
                      O‘zbekiston valyuta birjasida yevro 2 oktabr kuni 10 ming so‘mlik ko‘rsatkichda sotildi.
                    </p>
                    <div className="dashed-divider"></div>
                    <ul className="unstyled taglist">
                      <li className="taglist__item">
                        <a href="#" title="" className="taglist__link">&#35 <span>busines</span></a>
                      </li>
                      <li className="taglist__item">
                        <a href="#" title="" className="taglist__link">&#35 <span>startup</span></a>
                      </li>
                      <li className="taglist__item">
                        <a href="#" title="" className="taglist__link">&#35 <span>toshkent</span></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-md-7 col-sm-12 col-12">
                <ul className="unstyled news-list">
                  <li className="news-list__item">
                    <a className="news-list__link" href="#" title="">
                      <img src="images/img05.jpg" alt="" />
                    </a>
                    <div className="news-list__content">
                      <h6 className="news-list__title news-title">
                        <a href="#" title="">Chet elliklar O‘zbekistondagi investitsiyalardan qancha pul ishlab olgani ma'lum bo‘ldi</a>
                      </h6>
                      <div className="dashed-divider"></div>
                      <ul className="unstyled taglist">
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>busines</span></a>
                        </li>
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>startup</span></a>
                        </li>
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>toshkent</span></a>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li className="news-list__item">
                    <a className="news-list__link" href="#" title="">
                      <img src="images/img06.jpg" alt="" />
                    </a>
                    <div className="news-list__content">
                      <h6 className="news-list__title news-title">
                        <a href="#" title="">Dunyodagi eng yirik onlayn-bank egasi Oleg Tinkovdan yetti maslahat</a>
                      </h6>
                      <div className="dashed-divider"></div>
                      <ul className="unstyled taglist">
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>busines</span></a>
                        </li>
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>startup</span></a>
                        </li>
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>toshkent</span></a>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li className="news-list__item">
                    <a className="news-list__link" href="#" title="">
                      <img src="images/img04.png" alt="" />
                    </a>
                    <div className="news-list__content">
                      <h6 className="news-list__title news-title">
                        <a href="#" title="">Chery avtomobilsozlik kompaniyasi 176 mingta avtomobilni ortga qaytarib olmoqda   </a>
                      </h6>
                      <div className="dashed-divider"></div>
                      <ul className="unstyled taglist">
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>busines</span></a>
                        </li>
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>startup</span></a>
                        </li>
                        <li className="taglist__item">
                          <a href="#" title="" className="taglist__link">&#35 <span>toshkent</span></a>
                        </li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>        
        </div>
      </div>
    );
  }
}

export default MainNews;
