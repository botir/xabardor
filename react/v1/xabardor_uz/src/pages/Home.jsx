import React from "react";
import MainNewsContainer from "containers/MainNewsContainer.jsx";


class Home extends React.Component {
  render() {
    return (
		<MainNewsContainer />
    );
  }
}

export default Home;
