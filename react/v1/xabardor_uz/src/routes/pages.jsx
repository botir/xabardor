import Home from "pages/Home.jsx";
import Contact from "pages/Contact.jsx";

var pageRoutes = [
  {
    path: "/contact",
    name: "contact",
    component: Contact
  },
  {
    path: "/",
    name: "home",
    component: Home
  }
  
];
export default pageRoutes;
