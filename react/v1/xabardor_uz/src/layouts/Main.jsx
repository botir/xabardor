import React from "react";

import { Route, Switch, Redirect } from "react-router-dom";

import TopMenu from "components/Header/TopMenu.jsx";
import pageRoutes from "routes/pages.jsx";

class Main extends React.Component {
  render() {
    return (
      <div className="wrapper">
        <TopMenu />
        <Switch>
        {pageRoutes.map((prop, key) => {
          if (prop.pro) {
            return null;
          }
          if (prop.redirect) {
            return <Redirect from={prop.path} to={prop.pathTo} key={key} />;
          }
          return (
            <Route path={prop.path} component={prop.component} key={key} />
          );
          })}
        </Switch>



      </div>
    );
  }
}

export default Main;
