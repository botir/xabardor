import React from 'react';
import { connect } from 'react-redux';
import {fetchNewsIfNeeded} from 'actions/MainnewsActions';
import MainNews from 'components/Article/MainNews';

const MainNewsContainer = props => <MainNews {...props} />;

const mapStateToProps = (state) => {
  const { mainnews } = state;

  return {
    mainnews
  
  };
};

export default connect(mapStateToProps, {
  fetchNewsIfNeeded
})(MainNewsContainer);
